/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ForeignFailureMessageDto implements Serializable {

    @NotNull
    @Size(max=256)
    @Pattern(regexp="[A-Za-z0-9\\s_/:.-]+")
    private String metaId;

    @Size(max=256)
    @Pattern(regexp="^$|[A-Za-z0-9\\s_/(),.;:-]+")
    private String description;

    @NotNull
    @Size(max=100)
    @Pattern(regexp="[A-Za-z0-9\\s_/(),.;:-]+")
    private String source;

    @NotNull
    private @Valid ForeignFailureDataDto payload;

}
