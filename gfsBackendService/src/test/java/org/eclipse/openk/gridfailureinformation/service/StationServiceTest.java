/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationPolygonDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class StationServiceTest {
    @Qualifier("myStationService")
    @Autowired
    private StationService stationService;

    @MockBean
    private FailureInformationService failureInformationService;

    @MockBean
    private StationRepository stationRepository;

    @Test
    public void shouldGetBranchesProperly() {
        List<TblStation> mockTblStationList = MockDataHelper.mockTblStationList();
        when(stationRepository.findAll()).thenReturn(mockTblStationList);
        List<StationDto> stationDtoList = stationService.getStations();

        assertEquals(stationDtoList.size(), mockTblStationList.size());
        assertEquals(2, stationDtoList.size());
        assertEquals(stationDtoList.get(1).getUuid(), mockTblStationList.get(1).getUuid());
    }

    @Test
    public void shouldFindASingleBranchByIdAndName() {
        TblStation mockTblStation = MockDataHelper.mockTblStation();
        when(stationRepository.findByStationId(any(String.class))).thenReturn(Optional.of(mockTblStation));
        StationDto stationDto = stationService.getStationsById("xyz");

        assertEquals(stationDto.getUuid(), mockTblStation.getUuid());
    }

    @Test
    public void shouldGetPolygonCoordinates() {
        List<UUID> uuidList =  MockDataHelper.mockUuidList();
        List<StationPolygonDto> stationPolygonDtoList = MockDataHelper.mockPolygonStationCoordinatesList();
        when(failureInformationService.getPolygonCoordinatesForStationUuids((anyList()))).thenReturn(stationPolygonDtoList);

        List<StationPolygonDto> polygonCoordinatesList =  stationService.getPolygonCoordinates(uuidList);

        assertEquals(stationPolygonDtoList.size(), polygonCoordinatesList.size());
    }

}
