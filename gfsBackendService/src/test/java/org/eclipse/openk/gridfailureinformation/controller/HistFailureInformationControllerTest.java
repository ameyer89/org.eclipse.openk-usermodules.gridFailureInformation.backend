/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class HistFailureInformationControllerTest {

    @MockBean
    private HistFailureInformationService histFailureInformationService;

    @MockBean
    private HistFailureInformationStationService histFailureInformationStationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFindHistFailureInfos() throws Exception {
        List<FailureInformationDto> failureInformationDtoList = MockDataHelper.mockHistGridFailureInformationDtoList();

        when(histFailureInformationService.getFailureInformationVersionsByUuid(any(UUID.class))).thenReturn(failureInformationDtoList);

        mockMvc.perform(get("/hist-grid-failure-informations/{uuid}/versions", UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldFindHistFailureInfoVersion() throws Exception {
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        when(histFailureInformationService.getFailureInformationVersion(
                any(UUID.class),
                any(Long.class)
        )).thenReturn(dto);

        String versionId = Long.toString(new Random().nextLong());

        mockMvc.perform(get("/hist-grid-failure-informations/{uuid}/versions/{versionNumber}" ,UUID.randomUUID(),new Random().nextLong() ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("responsibility", is(  dto.getResponsibility())));
    }


    @Test
    public void shouldFindStationsforHistFailureInfoVersion() throws Exception {
        List<StationDto> dto = MockDataHelper.mockStationDtoList();

        when(histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(
                any(UUID.class),
                any(Long.class)
        )).thenReturn(dto);

        String versionId = Long.toString(new Random().nextLong());

        mockMvc.perform(get("/hist-grid-failure-informations/{uuid}/versions/{versionNumber}/stations" ,UUID.randomUUID(),new Random().nextLong() ))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

}