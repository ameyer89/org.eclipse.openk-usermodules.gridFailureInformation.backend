/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.email.EmailConfig;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.email.GfiEmail;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.RabbitMqMessageDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

//@SpringBootTest(classes = GridFailureInformationApplication.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
@ExtendWith(OutputCaptureExtension.class)
public class EmailServiceTest {

    private static GreenMail mailServer;
    private static int emailTestPort;

    @Qualifier("myEmailService")
    @Autowired
    private EmailService emailService;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    GfiProcessEnvironment gfiProcessEnvironment;

    @Autowired
    private EmailConfig emailConfig;

    @BeforeAll
    public static void beforeAll() {
        ServerSetup serverSetup = new ServerSetup(3025 , "localhost", ServerSetup.PROTOCOL_SMTP);
        serverSetup.setServerStartupTimeout(3000);
        mailServer = new GreenMail(serverSetup);
    }

    @BeforeEach
    public void beforeTest() {
        mailServer.start();
    }

    @AfterEach
    public void afterTest() {
        mailServer.stop();
    }

    @Test
    public void testSenHtmldEmail() throws MessagingException {
        RabbitMqMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailService.sendMail(mailMessageDto);
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendEmail() throws MessagingException {
        RabbitMqMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailConfig.setHtmlEmail(false);
        emailService.sendMail(mailMessageDto);
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendTestEmail() throws MessagingException {
        RabbitMqMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        emailConfig.setHtmlEmail(false);
        emailConfig.setUseHtmlEmailTemplate(false);
        emailService.sendTestMail("tester@test.de");
        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
    }

    @Test
    public void testSendMail_nok() {
        mailServer.stop();
        emailConfig.setUseHtmlEmailTemplate(false);
        RabbitMqMessageDto mailMessageDto = MockDataHelper.mockMailMessageDto();
        assertThrows(MessagingException.class, () -> emailService.sendMail(mailMessageDto));
    }

    @Test
    public void testSendMail_invalidRecipient(CapturedOutput output) throws MessagingException {
        RabbitMqMessageDto mailMessageDto = MockDataHelper.mockMailMessageDtoWrongRecipientFormat();
        emailService.sendMail(mailMessageDto);
        String out = output.getOut();
        assertTrue(output.getOut().contains("Invalid email-addresse: " + mailMessageDto.getMailAddresses().get(0)));
    }

    @Test
    public void testSendMail_invalidSender() {
        EmailConfig emailConfig = new EmailConfig();
        emailConfig.setSmtpHost("localhost");
        emailConfig.setEmailPort("3025");
        emailConfig.setSender("testCaseSendertest.de");
        assertThrows(MessagingException.class, () -> {
            GfiEmail emailManager = new GfiEmail(emailConfig);
            emailManager.sendEmail();
        });
    }




}
