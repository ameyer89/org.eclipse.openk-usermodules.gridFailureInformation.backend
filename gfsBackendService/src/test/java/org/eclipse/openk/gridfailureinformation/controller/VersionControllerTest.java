/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.*;
import org.eclipse.openk.gridfailureinformation.service.VersionService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.VersionDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class VersionControllerTest {

    @MockBean
    private VersionService versionService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void shouldReturnVersion() throws Exception {
        VersionDto versionDto = MockDataHelper.mockVersionDto();
        when(versionService.getVersion()).thenReturn(versionDto);

        mockMvc.perform(get("/version"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("backendVersion", is(  "660")))
                .andExpect(jsonPath("dbVersion", is("550")));
    }

    @Test
    public void shouldRaiseBadRequest1() throws Exception {
        checkException(new BadRequestException(), HttpStatus.BAD_REQUEST.value());
    };

    @Test
    public void shouldRaiseBadRequest2() throws Exception {
        checkException(new BadRequestException("test"), HttpStatus.BAD_REQUEST.value());
    };

    @Test
    public void shouldRaiseConflict() throws Exception {
        checkException(new ConflictException("test"), HttpStatus.CONFLICT.value());
    };

    @Test
    public void shouldRaiseInternalServer() throws Exception {
        checkException(new InternalServerErrorException("test"), HttpStatus.INTERNAL_SERVER_ERROR.value());
    };

    @Test
    public void shouldRaiseNotFound() throws Exception {
        checkException(new NotFoundException("test"), HttpStatus.NOT_FOUND.value());
    };

    @Test
    public void shouldRaiseNotAcceptable() throws Exception {
        checkException(new OperationDeniedException(OperationType.CREATE, "test"), HttpStatus.NOT_ACCEPTABLE.value());
    };

    @Test
    public void shouldRaiseUnautorized() throws Exception {
        checkException(new UnauthorizedException("test"), HttpStatus.UNAUTHORIZED.value());
    };

    @Test
    private  void checkException( Exception ex, int codeToCheck ) throws Exception {
        when(versionService.getVersion()).thenThrow(ex);
        mockMvc.perform(get("/version")).andExpect(status().is(codeToCheck));
    }

}