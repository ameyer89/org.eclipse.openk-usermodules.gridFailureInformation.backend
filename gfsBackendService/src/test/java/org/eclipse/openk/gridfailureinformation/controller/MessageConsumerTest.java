/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.email.MessageConsumer;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@ActiveProfiles("test")
public class MessageConsumerTest {

    @InjectMocks
    private MessageConsumer messageConsumer;

    @Mock
    ObjectMapper objectMapper;

    ObjectMapper objectMapperForTest = new ObjectMapper();

    @Mock
    EmailService emailService;

    @Test
    public void shouldCallImport() throws Exception {

        String mockMailMessageDtoString = objectMapperForTest.writeValueAsString(MockDataHelper.mockMailMessageDto());
        MessageProperties properties = new MessageProperties();
        properties.setContentType("text");

        Message mockMessage = mock(Message.class);
        when(mockMessage.getBody()).thenReturn(mockMailMessageDtoString.getBytes());
        when(mockMessage.getMessageProperties()).thenReturn(properties);

        messageConsumer.listenMessage(mockMessage);

        verify(emailService, times(1)).sendMail(any());
    }
}
