/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.UIStoreFailureInformationTask;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
class UIStoreFailureInformationTaskTest {
    class UIStoreTaskTester extends UIStoreFailureInformationTask {
        boolean onStayInTaskCalled = false;
        boolean onLeaveStepCalled = false;

        public UIStoreTaskTester(String description, boolean stayIfStateUnchanged) {
            super(description, stayIfStateUnchanged);
        }

        @Override
        protected void onStayInTask(GfiProcessSubject subject) throws ProcessException {
            onStayInTaskCalled = true;
            super.onStayInTask(subject);
        }

        @Override
        protected void onLeaveStep(GfiProcessSubject subject) throws ProcessException {
            onLeaveStepCalled = true;
            super.onLeaveStep(subject);
        }

    }

    @Qualifier("myFailureInformationService")
    @Autowired
    @SpyBean
    private FailureInformationService failureInformationService;

    @Autowired
    @SpyBean
    private ProcessHelper processHelper;

    @Test
    void shouldStayInTask() throws ProcessException {

        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();

        doReturn(dtoToSave).when(failureInformationService).storeFailureInfo(nullable(FailureInformationDto.class),nullable(GfiProcessState.class));
        doReturn(null).when(processHelper).getProcessStateFromStatusUuid(any());

        ProcessSubject subject = GfiProcessSubject.of(dtoToSave, processHelper);
        UIStoreTaskTester task = new UIStoreTaskTester("Testme", true);
        task.leaveStep(subject);

        verify(processHelper, times(1))
                .storeFailureFromViewModel(any( FailureInformationDto.class ));
        assertTrue(task.onStayInTaskCalled);

    }

    @Test
    void shouldNotStayInTask() throws ProcessException {
        FailureInformationDto failureInformationDto = MockDataHelper.mockFailureInformationDto();
        doReturn(failureInformationDto).when(failureInformationService).storeFailureInfo(nullable(FailureInformationDto.class),nullable(GfiProcessState.class));
        ProcessSubject subject = GfiProcessSubject.of(failureInformationDto, processHelper);

        UIStoreTaskTester task = new UIStoreTaskTester("Testme", false);
        task.leaveStep(subject);

        verify(processHelper, times(1))
                .storeFailureFromViewModel(any( FailureInformationDto.class ));

        assertTrue(task.onLeaveStepCalled);
    }


}
