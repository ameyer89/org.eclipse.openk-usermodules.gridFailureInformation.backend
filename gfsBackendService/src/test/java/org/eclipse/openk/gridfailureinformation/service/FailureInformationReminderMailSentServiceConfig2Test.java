/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@TestPropertySource(properties = "reminder.status-change.enabled=false")
@ActiveProfiles("test")
class FailureInformationReminderMailSentServiceConfig2Test {

    @Autowired
    private FailureInformationReminderMailSentService failureInformationReminderMailSentService;

    @Test
    void shouldNotDisplayReminder() {
        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();
        assertEquals(false, reminderIsDisplayed);
    }

}
