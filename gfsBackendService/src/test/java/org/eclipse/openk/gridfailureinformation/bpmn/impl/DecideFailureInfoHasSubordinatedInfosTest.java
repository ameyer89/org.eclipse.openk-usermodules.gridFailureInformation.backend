/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoHasSubordinatedInfos;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DecideFailureInfoHasSubordinatedInfosTest {


    @Test
    void shouldDecideYes() throws ProcessException {
        ProcessHelper processHelper = mock(ProcessHelper.class);
        List<FailureInformationDto> children = MockDataHelper.mockGridFailureInformationDtos();
        when( processHelper.getSubordinatedChildren( any(FailureInformationDto.class)))
                .thenReturn(children);

        GfiProcessSubject sub = GfiProcessSubject.of(MockDataHelper.mockFailureInformationDto(), processHelper);

        DecisionTask<GfiProcessSubject> dTask = new DecideFailureInfoHasSubordinatedInfos();
        assertEquals( DecisionTask.OutputPort.YES, dTask.decide(sub));
    }


    @Test
    void shouldDecideNo() throws ProcessException {
        ProcessHelper processHelper = mock(ProcessHelper.class);
        List<FailureInformationDto> children = new ArrayList<>();
        when( processHelper.getSubordinatedChildren( any(FailureInformationDto.class)))
                .thenReturn(children);

        GfiProcessSubject sub = GfiProcessSubject.of(MockDataHelper.mockFailureInformationDto(), processHelper);

        DecisionTask<GfiProcessSubject> dTask = new DecideFailureInfoHasSubordinatedInfos();
        assertEquals( DecisionTask.OutputPort.NO, dTask.decide(sub));
    }
}
