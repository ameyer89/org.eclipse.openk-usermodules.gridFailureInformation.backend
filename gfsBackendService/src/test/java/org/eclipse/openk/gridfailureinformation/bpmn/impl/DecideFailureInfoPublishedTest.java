/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoPublished;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class DecideFailureInfoPublishedTest {


    @Test
    public void shouldDecideCorrectly() throws ProcessException {
        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        fiDto.setPublicationStatus(Constants.PUB_STATUS_VEROEFFENTLICHT);
        GfiProcessSubject sub = GfiProcessSubject.of(fiDto, null);
        DecisionTask dtask = new DecideFailureInfoPublished();
        assertEquals(DecisionTask.OutputPort.YES, dtask.decide(sub));

        sub.getFailureInformationDto().setPublicationStatus(Constants.PUB_STATUS_UNVEROEFFENTLICHT);
        assertEquals(DecisionTask.OutputPort.NO, dtask.decide(sub));
    }

}
