/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.util;

import org.assertj.core.util.Lists;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc()
@ActiveProfiles("test")
class GroupMemberPlzFilterTest {

    @Mock
    private FailureInformationStationRepository fiStationRepository;

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private GroupMemberPlzFilter groupMemberPlzFilter;

    @Test
    void shouldFilterCorrectly() {
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setPostcode("11111");

        List<DistributionGroupMemberDto> memberDtoList = MockDataHelper.mockDistributionGroupMemberDtoList();
        memberDtoList.add(MockDataHelper.mockDistributionGroupMemberDto2());
        memberDtoList.add(MockDataHelper.mockDistributionGroupMemberDto2());

        memberDtoList.get(0).setPostcodeList(Arrays.asList("11111", "22222", "88888"));
        memberDtoList.get(1).setPostcodeList(null);
        memberDtoList.get(2).setPostcodeList(Arrays.asList("77777", "88888"));
        memberDtoList.get(3).setPostcodeList(Lists.emptyList());

        List<TblFailinfoStation> fiStations = MockDataHelper.mockTblFailureInformationStationList();
        fiStations.get(0).setStationStationId("ST01");
        fiStations.get(1).setStationStationId("ST02");
        fiStations.get(2).setStationStationId("ST03");
        fiStations.get(3).setStationStationId("ST04");
        when( fiStationRepository.findByFailureInformationId(eq( tblFailureInformation.getId())))
                .thenReturn(fiStations);

        TblAddress[] addresses = new TblAddress[5];
        for( int i=0; i<5; i++ ) {
            addresses[i] = MockDataHelper.mockTblAddress();
            when(addressRepository.findByStationId(eq("ST0"+(i+1)))).thenReturn(Arrays.asList(addresses[i]));
        }
        addresses[0].setPostcode(null);
        addresses[1].setPostcode("11111");
        addresses[2].setPostcode("22222");
        addresses[3].setPostcode("33333");
        addresses[4].setPostcode("44444");

        List<DistributionGroupMemberDto> retList = groupMemberPlzFilter.filter(memberDtoList, tblFailureInformation);

        assertEquals( 3, retList.size());
        assertEquals( memberDtoList.get(0).getUuid(), retList.get(0).getUuid());
        assertEquals( memberDtoList.get(1).getUuid(), retList.get(1).getUuid());
        assertEquals( memberDtoList.get(3).getUuid(), retList.get(2).getUuid());


    }

}
