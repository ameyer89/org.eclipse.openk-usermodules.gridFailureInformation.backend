/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.*;
import static org.eclipse.openk.gridfailureinformation.constants.Constants.*;
import static org.junit.Assert.assertEquals;


public class ExternalStatusCalculatorTest {

    @Test
    public void shouldCalulateExternalStatusCorrectly() {
        LocalDateTime inPast = java.time.LocalDateTime.now().minusDays(1);
        LocalDateTime inFuture = java.time.LocalDateTime.now().plusDays(1);

        assertEquals( EXT_STATUS_FINISHED, ExternalStatusCalculator.calculate(QUALIFIED.getStatusValue(), null, null, inPast ));
        assertEquals( EXT_STATUS_FINISHED, ExternalStatusCalculator.calculate(UPDATED.getStatusValue(), PUB_STATUS_VEROEFFENTLICHT, null, inPast ));
        assertEquals( EXT_STATUS_FINISHED, ExternalStatusCalculator.calculate(NEW.getStatusValue(), PUB_STATUS_VEROEFFENTLICHT, null, inPast ));

        assertEquals( EXT_STATUS_PLANNED, ExternalStatusCalculator.calculate(QUALIFIED.getStatusValue(), null, inFuture, inFuture ));
        assertEquals( EXT_STATUS_PLANNED, ExternalStatusCalculator.calculate(UPDATED.getStatusValue(), "any", inFuture, null ));

        assertEquals( EXT_STATUS_IN_WORK, ExternalStatusCalculator.calculate(QUALIFIED.getStatusValue(), null, inPast, inFuture ));
        assertEquals( EXT_STATUS_IN_WORK, ExternalStatusCalculator.calculate(UPDATED.getStatusValue(), "any", inPast, inFuture ));

        assertEquals( EXT_STATUS_FINISHED, ExternalStatusCalculator.calculate(COMPLETED.getStatusValue(), null, inPast, inFuture ));
        assertEquals( EXT_STATUS_FINISHED, ExternalStatusCalculator.calculate(CANCELED.getStatusValue(), "any", inFuture, inPast ));


        assertEquals( EXT_STATUS_IN_WORK, ExternalStatusCalculator.calculate(UPDATED.getStatusValue(), "any", inPast, null ));

        assertEquals( "", ExternalStatusCalculator.calculate(NEW.getStatusValue(), "any", inPast, null ));

        assertEquals( "", ExternalStatusCalculator.calculate(QUALIFIED.getStatusValue(), "any", null, null ));

    }

}
