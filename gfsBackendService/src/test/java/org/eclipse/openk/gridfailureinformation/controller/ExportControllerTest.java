/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc()
@ActiveProfiles("test")
public class ExportControllerTest {

    @MockBean
    private ExportService exportService;
    @MockBean
    ImportGeoJsonService importGeoJsonService;
    @MockBean
    EmailService emailService;


    @Autowired
    private MockMvc mockMvc;


    @Test
    public void shouldUpdateFailureInformation() throws Exception {

        FailureInformationDto failureInfoDto = MockDataHelper.mockFailureInformationDto();
        String[] channels = {"[\"TEST\"]"};

        when( exportService.exportFailureInformation(any(UUID.class), any(String[].class), any(GfiProcessState.class))).thenReturn(true);

        mockMvc.perform(put("/export/{failureInfoUuid}", failureInfoDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(channels)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldExportFailureInformationsToDMZ() throws Exception {

        doNothing().when(exportService).exportFailureInformationsToDMZ();
        doNothing().when(exportService).exportFeSettingsToDMZ();

        mockMvc.perform(get("/export/exporttodmz"))
                .andExpect(status().is2xxSuccessful());

        verify(exportService, times(1)).exportFailureInformationsToDMZ();
        verify(exportService, times(1)).exportFeSettingsToDMZ();
    }

    @Test
    void shoulImportgeojson() throws Exception {

        doNothing().when(importGeoJsonService).importGeoJsonFile();

        mockMvc.perform(get("/export/importgeojson"))
                .andExpect(status().is2xxSuccessful());

        verify(importGeoJsonService, times(1)).importGeoJsonFile();
    }

    @Test
    @WithMockUser(value = "mockedUser")
    void shoulSendTestMail() throws Exception {

        doNothing().when(emailService).sendTestMail(any());

        mockMvc.perform(get("/export/testmail"))
                .andExpect(status().is2xxSuccessful());

        verify(emailService, times(1)).sendTestMail(any());
    }

}