/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationLastModDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationPublicationChannelDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class FailureInformationControllerTest {

    @MockBean
    private FailureInformationService failureInformationService;

    @MockBean
    private ExportService exportService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldFindFailureInfos() throws Exception {
        Page<FailureInformationDto> page = MockDataHelper.mockGridFailureInformationDtoPage();
        when(failureInformationService.findFailureInformations(any(Pageable.class))).thenReturn(page);

        mockMvc.perform(get("/grid-failure-informations"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    void shouldFindFailureInfo() throws Exception {
        FailureInformationDto dto = MockDataHelper.mockFailureInformationDto();
        when(failureInformationService.findFailureInformation(any(UUID.class))).thenReturn(dto);

        mockMvc.perform(get("/grid-failure-informations/"+UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("responsibility", is(  dto.getResponsibility())));
    }

    @Test
    void shouldExceptionIfNotFoundFailureInfo() throws Exception {
        when(failureInformationService.findFailureInformation(any(UUID.class))).thenThrow( new NotFoundException() );

        mockMvc.perform(get("/grid-failure-informations/"+UUID.randomUUID().toString()))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldUpdateFailureInformation() throws Exception {
        FailureInformationDto failureInfoDto = MockDataHelper.mockFailureInformationDto();

        doNothing().when(exportService).exportFailureInformationsToDMZ();
        when( failureInformationService.updateFailureInfo(any(FailureInformationDto.class))).thenReturn(failureInfoDto);

        mockMvc.perform(put("/grid-failure-informations/{failureInfoUuid}", failureInfoDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoDto)))
                .andExpect(status().is2xxSuccessful());

        verify(failureInformationService, times(1)).updateFailureInfo(failureInfoDto);
    }


    @Test
    void shouldUpdateFailureInformationForPublish() throws Exception {
        FailureInformationDto failureInfoDto = MockDataHelper.mockFailureInformationDto();
        doNothing().when(exportService).exportFailureInformationsToDMZ();

        mockMvc.perform(put("/grid-failure-informations/{failureInfoUuid}?saveForPublish=true", failureInfoDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoDto)))
                .andExpect(status().is2xxSuccessful());

        verify(failureInformationService, times(1)).updateAndPublish(failureInfoDto);
    }


    @Test
    void shouldNotUpdateFailureInformationDueToException() throws Exception {
        FailureInformationDto failureInfoDto = MockDataHelper.mockFailureInformationDto();

        when( failureInformationService.updateFailureInfo(any(FailureInformationDto.class))).thenReturn(failureInfoDto);

        // provide different UUID in url and object
        mockMvc.perform(put("/grid-failure-informations/{failureInfoUuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoDto)))
                .andExpect(status().isBadRequest());
    }


    @Test
    void shouldInsertFailureInformation() throws Exception {
        FailureInformationDto failureInfoDto = MockDataHelper.mockFailureInformationDto();

        doNothing().when(exportService).exportFailureInformationsToDMZ();
        when( failureInformationService.insertFailureInfo(any(FailureInformationDto.class), eq(GfiProcessState.NEW))).thenReturn(failureInfoDto);

        mockMvc.perform(post("/grid-failure-informations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoDto)))
                .andExpect(jsonPath("$.responsibility", is(failureInfoDto.getResponsibility())))
                .andExpect(jsonPath("$.voltageLevel", is(failureInfoDto.getVoltageLevel())))
                .andExpect(jsonPath("$.pressureLevel", is(failureInfoDto.getPressureLevel())))
                .andExpect(jsonPath("$.description", is(failureInfoDto.getDescription())))
                .andExpect(jsonPath("$.postcode", is(failureInfoDto.getPostcode())))
                .andExpect(jsonPath("$.city", is(failureInfoDto.getCity())))
                .andExpect(jsonPath("$.district", is(failureInfoDto.getDistrict())))
                .andExpect(jsonPath("$.street", is(failureInfoDto.getStreet())))
                .andExpect(jsonPath("$.housenumber", is(failureInfoDto.getHousenumber())))
                .andExpect(jsonPath("$.stationId", is(failureInfoDto.getStationId())))
                .andExpect(jsonPath("$.stationDescription", is(failureInfoDto.getStationDescription())))
                .andExpect(jsonPath("$.stationCoords", is(failureInfoDto.getStationCoords())))
                .andExpect(jsonPath("$.radiusId", is(failureInfoDto.getRadiusId().toString())))
                .andExpect(jsonPath("$.radius", is(failureInfoDto.getRadius().intValue())))
                .andExpect(jsonPath("$.longitude", is(failureInfoDto.getLongitude().doubleValue())))
                .andExpect(jsonPath("$.latitude", is(failureInfoDto.getLatitude().doubleValue())))
                .andExpect(jsonPath("$.publicationStatus", is(failureInfoDto.getPublicationStatus())))
                .andExpect(jsonPath("$.publicationFreetext", is(failureInfoDto.getPublicationFreetext())))
                .andExpect(jsonPath("$.expectedReasonId", is(failureInfoDto.getExpectedReasonId().toString())))
                .andExpect(jsonPath("$.expectedReasonText", is(failureInfoDto.getExpectedReasonText())))
                .andExpect(jsonPath("$.failureClassificationId", is(failureInfoDto.getFailureClassificationId().toString())))
                .andExpect(jsonPath("$.failureClassification", is(failureInfoDto.getFailureClassification())))
                .andExpect(jsonPath("$.failureTypeId", is(failureInfoDto.getFailureTypeId().toString())))
                .andExpect(jsonPath("$.statusInternId", is(failureInfoDto.getStatusInternId().toString())))
                .andExpect(jsonPath("$.branchId", is(failureInfoDto.getBranchId().toString())))
                .andExpect(jsonPath("$.branch", is(failureInfoDto.getBranch())))
                .andExpect(jsonPath("$.branchColorCode", is(failureInfoDto.getBranchColorCode())))

                .andExpect(status().is2xxSuccessful());
    }


    @Test
    void shouldInsertPublicationChannelForFailureInfo() throws Exception {

        FailureInformationPublicationChannelDto publicationChannel = MockDataHelper.mockFailureInformationPublicationChannelDto();

        when (failureInformationService.insertPublicationChannelForFailureInfo(any(UUID.class),anyString(), anyBoolean())).thenReturn(publicationChannel);

        mockMvc.perform(post("/grid-failure-informations/{uuid}/channels?publicationChannel=MOCKMAIL", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(publicationChannel)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldDeletePublicationChannelForFailureInfo() throws Exception {

        FailureInformationPublicationChannelDto publicationChannel = MockDataHelper.mockFailureInformationPublicationChannelDto();

        doNothing().when(failureInformationService).deletePublicationChannelForFailureInfo(any(UUID.class),anyString());

        mockMvc.perform(delete("/grid-failure-informations/{uuid}/channels?publicationChannel=MOCKMAIL", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(publicationChannel)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldCondenseFailureInformations() throws Exception {
        List<UUID> uuidList = new ArrayList<>();
        uuidList.add(UUID.randomUUID());
        uuidList.add(UUID.randomUUID());
        uuidList.add(UUID.randomUUID());

        FailureInformationDto failureInformationDto = MockDataHelper.mockFailureInformationDto();

        when (failureInformationService.condenseFailureInfos(any(List.class), any(Optional.class))).thenReturn(failureInformationDto);

        mockMvc.perform(post("/grid-failure-informations/condense")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(uuidList)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldFindSubordinateFailureInfosByCondensedUuid()  throws Exception{

        List<FailureInformationDto> fiList = MockDataHelper.mockGridFailureInformationDtos();
        when (failureInformationService.findFailureInformationsByCondensedUuid(any(UUID.class))).thenReturn(fiList);

        mockMvc.perform(get("/grid-failure-informations/condensed/{uuid}", UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldFindChannelsByFailureUuid() throws Exception{

        List<FailureInformationPublicationChannelDto> fipChannelList = MockDataHelper.mockFailureInformationPublicationChanneDtolList();
        when (failureInformationService.getPublicationChannelsForFailureInfo(any(UUID.class))).thenReturn(fipChannelList);

        mockMvc.perform(get("/grid-failure-informations/{uuid}/channels", UUID.randomUUID().toString()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldDeleteFailureInfo() throws Exception {

        FailureInformationDto failureInformationDto = MockDataHelper.mockFailureInformationDto();

        doNothing().when(failureInformationService).deleteFailureInfo(any(UUID.class));

        mockMvc.perform(delete("/grid-failure-informations/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInformationDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test void shouldGetLastModTimestampCorrectly() throws Exception {
        FailureInformationLastModDto dto = MockDataHelper.mockFailureInformationLastModDto();
        when( failureInformationService.getFailureInformationLastModTimeStamp() ).thenReturn(dto);

        mockMvc.perform(get("/grid-failure-informations/last-modification")
        .contentType(MediaType.APPLICATION_JSON))
           .andExpect(jsonPath("$.id", is(dto.getUuid().toString())));

    }

}