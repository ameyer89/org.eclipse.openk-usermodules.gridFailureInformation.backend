/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.api;

import org.eclipse.openk.gridfailureinformation.api.impl.CustomPageImpl;
import org.junit.Test;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CustomPageImplTest {

    @Test
    public void testWholeClass() {
        List<String> contentList = new LinkedList<>();
        contentList.add("item1");
        contentList.add("item2");

        CustomPageImpl<String> testPageEmpty = new CustomPageImpl<>();
        assertEquals( 0, testPageEmpty.getTotalElements());

        CustomPageImpl<String> testPageFull = new CustomPageImpl<String>(contentList, 0, 3, 2L, null, true, 1, null, true,2 );
        assertEquals(2, testPageFull.getNumberOfElements());

        CustomPageImpl<String> testPageThree = new CustomPageImpl<>(contentList, Pageable.unpaged(), 2L);
        assertEquals(2, testPageThree.getNumberOfElements());

        CustomPageImpl<String> testPageSingleList = new CustomPageImpl<>(contentList);
        assertEquals(2, testPageSingleList.getNumberOfElements());

    }
}
