/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.security;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecurityTest {

    private static final List<String> EXCLUDED_METHODS = Arrays.asList("org.eclipse.openk.gridfailureinformation.controller.VersionController.getVersion()");

    @DisplayName("Test - Security")
    @Test
    void testSecurity() {

        final String packageName = "org.eclipse.openk.gridfailureinformation";  // Package-Name from Main-Class (Root)

        Set<String> setMethodsMappings = new HashSet<>();        // Set for annotated Methods with @*Mapping
        Set<String> setMethodsSecured = new HashSet<>();         // Set for annotated Methods with @Secured


        Reflections reflMeth = new Reflections(packageName, new MethodAnnotationsScanner());

        Set<Method> methodsGetMapping = reflMeth.getMethodsAnnotatedWith(GetMapping.class);             // Methods annotated with @GetMapping
        methodsGetMapping.forEach(method -> setMethodsMappings.add(method.toString()));
        Set<Method> methodsRequestMapping = reflMeth.getMethodsAnnotatedWith(RequestMapping.class);     // Methods annotated with @RequestMapping
        methodsRequestMapping.forEach(method -> setMethodsMappings.add(method.toString()));
        Set<Method> methodsPostMapping = reflMeth.getMethodsAnnotatedWith(PostMapping.class);           // Methods annotated with @PostMapping
        methodsPostMapping.forEach(method -> setMethodsMappings.add(method.toString()));
        Set<Method> methodsPutMapping = reflMeth.getMethodsAnnotatedWith(PutMapping.class);             // Methods annotated with @PutMapping
        methodsPutMapping.forEach(method -> setMethodsMappings.add(method.toString()));
        Set<Method> methodsDeleteMapping = reflMeth.getMethodsAnnotatedWith(DeleteMapping.class);       // Methods annotated with @DeleteMapping
        methodsDeleteMapping.forEach(method -> setMethodsMappings.add(method.toString()));

        Set<Method> methodsSecured = reflMeth.getMethodsAnnotatedWith(Secured.class);                   // Methods annotated with @Secured
        methodsSecured.forEach(method -> setMethodsSecured.add(method.toString()));

        Set<String> setMethodsMappingsFinal = checkForRestClasses(packageName, setMethodsMappings); // Proceed only with Mapping-Method which are in a class annotated with @RestController

        setMethodsSecured.forEach(setMethodsMappingsFinal::remove); // Alle Secured-Methoden aus der Mappings-Set-Final entfernen

        assertEquals("Everything is @Secured", extractUnsecuredMethods(packageName, setMethodsMappingsFinal));
    }

    /*
    * Generates a String depending on the result of the Test
    * */
    private String extractUnsecuredMethods(String pPackageName, Set<String> pSetMappings) {

        String tempFailures = "Everything is @Secured";

        boolean insecureMethodFound = false;
        if (pSetMappings.size() > 0) {
            // Set is not empty -> there are failures
            // Naming (and trimming the modifier & returnType) the faulty methods
            StringBuilder sb = new StringBuilder();
            sb.append("\nThe following methods (" + pSetMappings.size() + ") are not @Secured:\n");
            for (String m : pSetMappings) {
                String x = m.substring(0, m.indexOf("(")); // Cut off list of parameters
                int bezeichnerIndex = x.lastIndexOf(pPackageName); // get index of identifier
                m = m.substring(bezeichnerIndex); // Cut off modifier & returnType
                if (EXCLUDED_METHODS.contains(m)) continue;
                insecureMethodFound = true;
                sb.append("\t- " + m + "\n");
            }
            if (insecureMethodFound) {
                tempFailures = sb.toString();
            }
        }
        return tempFailures;
    }

    /*
     * Checks a set of methods if the members are located in a RestController
     * */
    private Set<String> checkForRestClasses(String pPackageName, Set<String> pSetMappings){

        Set<String> setClassesRc = new HashSet<>();         // List for annotated Classes with @RestController
        Set<String> setMethodMappingFinal = new HashSet<>();         // List for all methods in @RestController-classes

        Reflections reflRstClss = new Reflections(pPackageName, new SubTypesScanner(false),
                new TypeAnnotationsScanner());
        Set<Class<?>> set_RestController = reflRstClss.getTypesAnnotatedWith(RestController.class);
        set_RestController.stream().forEach(classRc -> setClassesRc.add(classRc.getName().substring(classRc.getName().indexOf(pPackageName))));


        for (String m : pSetMappings) {
            String x = m.substring(0, m.lastIndexOf("(")); // Cut off list of parameters
            x = x.substring(x.lastIndexOf(pPackageName)); // Cut off modifier und returnType
            x = x.substring(0, x.lastIndexOf(".")); // Cut off name of method
            if (setClassesRc.contains(x)) {
                setMethodMappingFinal.add(m);
            }
        }

        return setMethodMappingFinal;
    }


}