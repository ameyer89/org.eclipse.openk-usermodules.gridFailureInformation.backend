/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessGrid;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessGridImpl;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessStateImpl;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ServiceTaskImpl;
import org.eclipse.openk.gridfailureinformation.bpmn.base.TestProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
class GfiGridTest {

    @Qualifier("myFailureInformationService")
    @Autowired
    @SpyBean
    private FailureInformationService failureInformationService;

    @SpyBean
    @Autowired
    private ProcessHelper processHelper;
    @MockBean
    private StatusRepository statusRepository;
    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @Test
    void testProcessGrid() throws ProcessException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        theGrid.recover(subject).start(() -> ProcessStateImpl.UITASK);
        ServiceTaskImpl serviceTask = (ServiceTaskImpl)theGrid.getService();
        assertTrue(serviceTask.leaveStepCalled);
    }

    @Test
    void testProcessGrid_unreachable() throws ProcessException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        assertThrows(ProcessException.class, () -> theGrid.recover(subject).start(()-> ProcessStateImpl.UNREACHABLE) );
    }

    // wegen intialer Testabdeckung
    @Test
    void testGfiGridConstr() throws ProcessException {
        ProcessGrid grid = new GfiGrid();
        assertNotNull(grid);
    }

    @Test
    void shouldEndInQualifyMessageWhenStoredWithCreatedFromDBStateNew() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.CREATED.getStatusValue());

        when( failureInformationRepository.findByUuid(dtoToSave.getUuid())).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(failureFromDB.getRefStatusIntern().getUuid())).thenReturn(Optional.of(refStatusFromDB));
        when( statusRepository.findByUuid(dtoToSave.getStatusInternId())).thenReturn(Optional.of(refStatusFromDB));
        doReturn(dtoToSave).when(failureInformationService).storeFailureInfo(nullable(FailureInformationDto.class),nullable(GfiProcessState.class));

        FailureInformationDto failureInformationDto = failureInformationService.updateFailureInfo(dtoToSave);

        verify(processHelper, times(1)).storeFailureFromViewModel(any(FailureInformationDto.class));

    }

    @Test
    void shouldEndInQualifyMessageWhenStoredWithCreatedFromDBStatePlanned() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.PLANNED.getStatusValue());
        doReturn(dtoToSave).when(failureInformationService).storeFailureInfo(nullable(FailureInformationDto.class),nullable(GfiProcessState.class));

        when( failureInformationRepository.findByUuid(dtoToSave.getUuid())).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(failureFromDB.getRefStatusIntern().getUuid())).thenReturn(Optional.of(refStatusFromDB));
        when( statusRepository.findByUuid(dtoToSave.getStatusInternId())).thenReturn(Optional.of(refStatusFromDB));

        FailureInformationDto failureInformationDto = failureInformationService.updateFailureInfo(dtoToSave);

        verify(processHelper, times(1))
                .storeFailureFromViewModel(any(FailureInformationDto.class));

    }

    @Test
    void shouldCallStoreOnlyOnceWhenSavingWithNewCreated() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.NEW.getStatusValue());
        doReturn(dtoToSave).when(failureInformationService).storeFailureInfo(nullable(FailureInformationDto.class),nullable(GfiProcessState.class));

        when( failureInformationRepository.findByUuid(dtoToSave.getUuid())).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(failureFromDB.getRefStatusIntern().getUuid())).thenReturn(Optional.of(refStatusFromDB));
        when( statusRepository.findByUuid(dtoToSave.getStatusInternId())).thenReturn(Optional.of(refStatusFromDB));

        FailureInformationDto failureInformationDto = failureInformationService.updateFailureInfo(dtoToSave);

        verify(processHelper, times(1)).storeFailureFromViewModel(any(FailureInformationDto.class));
    }
}
