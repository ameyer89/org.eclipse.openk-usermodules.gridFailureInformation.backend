/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.HtblFailureInformationStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.HistFailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.HistFailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
@Slf4j
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

class HistFailureInformationStationServiceTest {
    @Qualifier("myHistFailureInformationStationService")
    @Autowired
    private HistFailureInformationStationService histFailureInformationStationService;

    @Autowired
    private HistFailureInformationMapper histFailureInformationMapper;

    @Autowired
    private StationMapper stationMapper;

    @MockBean
    private HistFailureInformationStationRepository histFailureInformationStationRepository;

    @MockBean
    private FailureInformationStationRepository failureInformationStationRepository;

    @MockBean
    private HistFailureInformationRepository histFailureInformationRepository;

    @MockBean
    private StationRepository stationRepository;

    @Test
    void shouldThrowNotFoundException() {
        UUID failureInfoUuid = UUID.randomUUID();
        long versionNumber = new Random().nextLong();
        try {
            histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(failureInfoUuid, versionNumber);
            Assertions.fail("Expected an NotFoundException to be thrown");
        } catch (NotFoundException nfe) {
            log.error("TEST NotFoundException caught");
        }
    }

    @Test
    void shouldGetAllStationsForFailureInfoVersion() {
        List<HtblFailureInformation> htblFailureInformationList = MockDataHelper.mockHistTblFailureInformationList();
        List<HtblFailureInformationStation> htblFailureInformationStationList = MockDataHelper.mockHistTblFailureInformationStationList();

        TblStation tblStation = MockDataHelper.mockTblStation();
        List<StationDto> stationDtoList = MockDataHelper.mockTblStationList()
                .stream()
                .map( stationMapper::toStationDto )
                .collect(Collectors.toList());

        when(histFailureInformationRepository.findByUuidAndVersionNumber(any(UUID.class), any(Long.class))).thenReturn(htblFailureInformationList);
        when(histFailureInformationStationRepository.findByFkTblFailureInformationAndVersionNumber(any(Long.class), any(Long.class))).thenReturn(htblFailureInformationStationList);
        when(stationRepository.findByStationId(any(String.class))).thenReturn(Optional.of(tblStation));

        List<StationDto> testStationDtos = histFailureInformationStationService.findHistStationsByFailureInfoAndVersionNumber(UUID.randomUUID(), new Random().nextLong());

        assertEquals(stationDtoList.size(), testStationDtos.size());
        assertEquals(2, testStationDtos.size());
        assertEquals(1, stationDtoList.stream().filter(x->x.getG3efid().equals(tblStation.getG3efid())).count());
    }

    @Test
    void shouldInsertStationsForFailureInfoVersion() {
        HtblFailureInformationStation histFailureInfoStation = MockDataHelper.mockHistTblFailureInformationStation();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        List<TblFailinfoStation> fiStations = new LinkedList<>();
        fiStations.add(MockDataHelper.mockTblFailureInformationStation());
        fiStations.add(MockDataHelper.mockTblFailureInformationStation());

        when(histFailureInformationStationRepository.save(any(HtblFailureInformationStation.class))).thenReturn(histFailureInfoStation);
        when(failureInformationStationRepository.findByFailureInformationId(anyLong())).thenReturn(fiStations);

        List<HistFailureInformationStationDto> savedHistFailureInfoStationDtos = histFailureInformationStationService.insertHistFailureInfoStationsForGfi(failureInformation);

        assertEquals(fiStations.size(), savedHistFailureInfoStationDtos.size());
        assertEquals(savedHistFailureInfoStationDtos.get(0).getFailureInformationId(), histFailureInfoStation.getFkTblFailureInformation());
        assertEquals(savedHistFailureInfoStationDtos.get(0).getStationStationId(), histFailureInfoStation.getStationStationId());
        assertEquals(savedHistFailureInfoStationDtos.get(0).getVersionNumber(), histFailureInfoStation.getVersionNumber());
    }


    @Test
    void shouldNotInsertStationsForFailureInfoVersion_NoStations() {
        HtblFailureInformationStation histFailureInfoStation = MockDataHelper.mockHistTblFailureInformationStation();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation2();
        List<TblStation> tblStationList = new LinkedList<>();
        failureInformation.setStations(tblStationList);
        when(histFailureInformationStationRepository.save(any(HtblFailureInformationStation.class))).thenReturn(histFailureInfoStation);

        List<HistFailureInformationStationDto> savedHistFailureInfoStationDtos = histFailureInformationStationService.insertHistFailureInfoStationsForGfi(failureInformation);

        assertEquals(savedHistFailureInfoStationDtos.size(), failureInformation.getStations().size());
    }

}
