/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.apache.commons.io.FileUtils;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ImportGeoJsonServiceTest {

    @MockBean
    private StationRepository stationRepository;

    @Autowired
    ImportGeoJsonService importGeoJsonService;

    private final static String TEST_RES_FOLDER = "./src/test/resources/geoJsonImporterTest";
    private final static String TEST_RES_FOLDER_TMP = "/tmpFolder";
    private final File srcTestFolder = new File(TEST_RES_FOLDER, "/files");
    private final File targetTmpFolder = new File(TEST_RES_FOLDER, TEST_RES_FOLDER_TMP);

    @BeforeAll
    public void setup() throws IOException {
        targetTmpFolder.mkdir();
        FileUtils.copyDirectory(srcTestFolder, targetTmpFolder);
        importGeoJsonService.initImportGeoJsonService();
    }

    @Test
    void shouldImportCorrectlyAndSortOutInvalids() {

        when(stationRepository.findByStationId(any())).thenReturn(Optional.of(MockDataHelper.mockTblStation()));
        when(stationRepository.save(any(TblStation.class))).thenReturn(MockDataHelper.mockTblStation());
        importGeoJsonService.importGeoJsonFile();

        verify(stationRepository, times(1)).save(any( TblStation.class));
    }
}
