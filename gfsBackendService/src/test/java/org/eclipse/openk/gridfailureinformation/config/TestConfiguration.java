/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.api.AuthNAuthApi;
import org.eclipse.openk.gridfailureinformation.api.ContactApi;
import org.eclipse.openk.gridfailureinformation.api.SitCacheApi;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiGrid;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqProperties;
import org.eclipse.openk.gridfailureinformation.email.EmailConfig;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.mapper.AddressMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.BranchMapper;
import org.eclipse.openk.gridfailureinformation.mapper.BranchMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMemberMapper;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMemberMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.ExpectedReasonMapper;
import org.eclipse.openk.gridfailureinformation.mapper.ExpectedReasonMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureClassificationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureClassificationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationDistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationDistributionGroupMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationPublicationChannelMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationPublicationChannelMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationReminderMailSentMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationReminderMailSentMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationStationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.HistFailureInformationStationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.ImportDataMapper;
import org.eclipse.openk.gridfailureinformation.mapper.ImportDataMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.RadiusMapper;
import org.eclipse.openk.gridfailureinformation.mapper.RadiusMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.StatusMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StatusMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.VersionMapper;
import org.eclipse.openk.gridfailureinformation.mapper.VersionMapperImpl;
import org.eclipse.openk.gridfailureinformation.mapper.tools.VisibilityConfigurationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.tools.VisibilityConfigurationMapperImpl;
import org.eclipse.openk.gridfailureinformation.model.JwtToken;
import org.eclipse.openk.gridfailureinformation.service.AddressService;
import org.eclipse.openk.gridfailureinformation.service.AuthNAuthService;
import org.eclipse.openk.gridfailureinformation.service.BranchService;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupMemberService;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.DistributionTextPlaceholderService;
import org.eclipse.openk.gridfailureinformation.service.ExpectedReasonService;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.FailureClassificationService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationDistributionGroupService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationReminderMailSentService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationService;
import org.eclipse.openk.gridfailureinformation.service.HistFailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.eclipse.openk.gridfailureinformation.service.ImportService;
import org.eclipse.openk.gridfailureinformation.service.RadiusService;
import org.eclipse.openk.gridfailureinformation.service.SettingsService;
import org.eclipse.openk.gridfailureinformation.service.StationService;
import org.eclipse.openk.gridfailureinformation.service.StatusService;
import org.eclipse.openk.gridfailureinformation.service.VersionService;
import org.eclipse.openk.gridfailureinformation.util.GroupMemberPlzFilter;
import org.eclipse.openk.gridfailureinformation.util.ImportDataValidator;
import org.eclipse.openk.gridfailureinformation.util.VisibilityConfigurationRaw;
import org.mockito.Mockito;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@EnableJpaRepositories(basePackages = "org.eclipse.openk.gridfailureinformation")
@EntityScan(basePackageClasses = GridFailureInformationApplication.class)
@ContextConfiguration( initializers = {ConfigFileApplicationContextInitializer.class})
@TestPropertySource("spring.config.location=classpath:application.yml")
public class TestConfiguration {

    @Value("${jwt.staticJwt}")
    private String staticJwt;

    @Bean
    public EmailService myEmailService() {return new EmailService();}

    @Bean
    public EmailConfig myEmailConfig() {return new EmailConfig();}

    @Bean
    public MessageChannel mailExportChannel() {return Mockito.mock(MessageChannel.class);}

    @Bean
    public RestTemplate restTemplate(){return mock(RestTemplate.class);}

    @Bean
    SettingsService settingsService(){return new SettingsService();}

    @Bean
    FESettings fESettings(){return new FESettings();}

    @Bean
    @Qualifier("exchange")
    public DirectExchange exchange(){return mock(DirectExchange.class);}

    @Bean
    @Qualifier("rabbitAdmin")
    public RabbitAdmin rabbitAdmin(){return mock(RabbitAdmin.class);}

    @Bean
    @Qualifier("rabbitMqConfig")
    public RabbitMqConfig rabbitMqConfig() {
        RabbitMqConfig rabbitMqConfigMock = mock(RabbitMqConfig.class);
        doNothing().when(rabbitMqConfigMock).buildAllQueues();
        return rabbitMqConfigMock;
    }

    @Bean
    @Qualifier("rabbitTemplate")
    public RabbitTemplate rabbitTemplate(){

        RabbitTemplate rt =  mock(RabbitTemplate.class);
        ConnectionFactory cf = mock(ConnectionFactory.class);
        when(rt.getConnectionFactory()).thenReturn(cf);

        return rt;

    }

    @Bean
    @Qualifier("rabbitMqProperties")
    public RabbitMqProperties rabbitMqProperties() {
        return mock(RabbitMqProperties.class);
    }

    @Bean
    AuthNAuthService authNAuthService(){
        AuthNAuthService authNAuthServiceSpy = spy(AuthNAuthService.class);
        doNothing().when(authNAuthServiceSpy).logout();
        doNothing().when(authNAuthServiceSpy).logout(any());
        return authNAuthServiceSpy;}

    @Bean
    AuthNAuthApi authNAuthApi(){
        AuthNAuthApi authNAuthApiMock = mock(AuthNAuthApi.class);
        JwtToken jwtToken = new JwtToken();
        jwtToken.setAccessToken(staticJwt);
        when(authNAuthApiMock.login(any())).thenReturn(jwtToken);
        return authNAuthApiMock;
    }

    @Bean
    ResourceConfig resourceConfig(){return new ResourceConfig();}

    @Bean
    ExportService exportService(){return new ExportService();}

    @Bean
    ContactApi contactApi() { return mock(ContactApi.class); }

    @Bean
    SitCacheApi sitCacheApi() { return mock(SitCacheApi.class); }

    @Bean
    VersionMapper versionMapper() { return new VersionMapperImpl(); }

    @Bean
    FailureInformationMapper failureInformationMapper() { return new FailureInformationMapperImpl(); }

    @Bean
    HistFailureInformationMapper histFailureInformationMapper() { return new HistFailureInformationMapperImpl(); }

    @Bean
    BranchMapper branchMapper() { return new BranchMapperImpl(); }

    @Bean
    FailureClassificationMapper failureClassificationMapper() {
        return new FailureClassificationMapperImpl();
    }

    @Bean
    StatusMapper statusMapper() {
        return new StatusMapperImpl();
    }

    @Bean
    RadiusMapper radiusMapper() {
        return new RadiusMapperImpl();
    }

    @Bean
    ExpectedReasonMapper expectedReasonMapper() { return new ExpectedReasonMapperImpl(); }

    @Bean
    ImportDataMapper importDataMapper() { return new ImportDataMapperImpl(); }

    @Bean
    AddressMapper addressMapper() { return new AddressMapperImpl(); }

    @Bean
    ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    DistributionGroupMapper distributionGroupMapper() { return new DistributionGroupMapperImpl(); }

    @Bean
    DistributionGroupMemberMapper distributionGroupMemberMapper() { return new DistributionGroupMemberMapperImpl(); }

    @Bean
    FailureInformationReminderMailSentMapper failureInformationReminderMailSentMapper() {return new FailureInformationReminderMailSentMapperImpl(); }

    @Bean
    StationMapper stationMapper() { return new StationMapperImpl(); }

    @Bean
    FailureInformationStationMapper failureInformationStationMapper() { return new FailureInformationStationMapperImpl(); }

    @Bean
    FailureInformationDistributionGroupMapper failureInformationDistributionGroupMapper() { return new FailureInformationDistributionGroupMapperImpl(); }

    @Bean
    public FailureInformationPublicationChannelMapper myFailureInformationPublicationChannelMapper() {
        return new FailureInformationPublicationChannelMapperImpl();
    }

    @Bean
    HistFailureInformationStationMapper histFailureInformationStationMapper() { return new HistFailureInformationStationMapperImpl(); }

    @Bean
    VisibilityConfigurationMapper visibilityConfigurationMapper() { return new VisibilityConfigurationMapperImpl(); }

    @Bean
    public VersionService myVersionService() {
        return new VersionService();
    }

    @Bean
    public ImportDataValidator importDataValidator() { return new ImportDataValidator(); }

    @Bean
    public FailureInformationService myFailureInformationService() {
        return new FailureInformationService();
    }

    @Bean
    public GfiGrid gfiGrid() {
        try {
            return new GfiGrid();
        } catch (ProcessException e) {
            return null;
        }
    }

    @Bean
    public ProcessHelper processHelper() {
        return new ProcessHelper();
    }

    @Bean
    public GfiProcessEnvironment gfiProcessEnvironment() {
        return new GfiProcessEnvironment();
    }
    @Bean
    public ImportGeoJsonService myImportGeoJsonService() {
        return new ImportGeoJsonService();
    }
    @Bean
    public HistFailureInformationService myHistFailureInformationService() {
        return new HistFailureInformationService();
    }

    @Bean
    public BranchService myBranchService() {
        return new BranchService();
    }

    @Bean
    public FailureClassificationService myFailureClassificationService() {
        return new FailureClassificationService();
    }

    @Bean
    public StatusService myStatusService() {
        return new StatusService();
    }

    @Bean
    public RadiusService myRadiusService() {
        return new RadiusService();
    }

    @Bean
    public ExpectedReasonService myExpectedReasonService() {
        return new ExpectedReasonService();
    }

    @Bean
    public AddressService myAddressService() { return new AddressService(); }

    @Bean
    public ImportService myJobManagerService() { return new ImportService(); }

    @Bean
    public DistributionGroupService myDistributionGroupService() {return  new DistributionGroupService();}

    @Bean
    public DistributionGroupMemberService myDistributionGroupMemberService() {return  new DistributionGroupMemberService();}

    @Bean
    public DistributionTextPlaceholderService myDistributionTextPlaceholderService() {return  new DistributionTextPlaceholderService();}

    @Bean
    public FailureInformationReminderMailSentService myFailureInformationReminderMailSentService() {
        return new FailureInformationReminderMailSentService();
    }
    @Bean
    public StationService myStationService() { return new StationService(); }

    @Bean
    public FailureInformationStationService myFailureInformationStationService() {
        return new FailureInformationStationService();
    }
    @Bean
    public FailureInformationDistributionGroupService myFailureInformationDistributionGroupService() {return  new FailureInformationDistributionGroupService();}

    @Bean
    public HistFailureInformationStationService myHistFailureInformationStationService() {return  new HistFailureInformationStationService();}

    @Bean
    public GroupMemberPlzFilter groupMemberPlzFilter() { return new GroupMemberPlzFilter(); }

    @Bean
    public VisibilityConfigurationRaw visibilityConfigurationRaw() { return new VisibilityConfigurationRaw(); }

    @Bean
    public VisibilityConfigurationRaw.FieldVisibilityRaw detailFieldVisibilityRaw() { return new VisibilityConfigurationRaw.FieldVisibilityRaw(); }

    @Bean
    public VisibilityConfigurationRaw.TableInternColumnVisibility tableInternColumnVisibility() { return new VisibilityConfigurationRaw.TableInternColumnVisibility(); }

    @Bean
    public VisibilityConfigurationRaw.TableExternColumnVisibility tableExternColumnVisibility() { return new VisibilityConfigurationRaw.TableExternColumnVisibility(); }

    @Bean
    public VisibilityConfigurationRaw.MapExternTooltipVisibility mapExternTooltipVisibility() { return new VisibilityConfigurationRaw.MapExternTooltipVisibility(); }


}

