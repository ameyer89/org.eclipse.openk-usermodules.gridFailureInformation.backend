/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDistributionGroupDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class FailureInformationDistributionGroupServiceTest {
    @Qualifier("myFailureInformationDistributionGroupService")
    @Autowired
    private FailureInformationDistributionGroupService failureInformationDistributionGroupService;

    @Autowired
    private DistributionGroupMapper distributionGroupMapper;

    @MockBean
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @MockBean
    private DistributionGroupRepository distributionGroupRepository;

    @Test
    public void shouldGetAllDistributionGroupsForFailureInfo() {
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        List<DistributionGroupDto> distributionGroupDtoList = MockDataHelper.mockDistributionGroupList().stream()
                .map( distributionGroupMapper::toDistributionGroupDto )
                .collect(Collectors.toList());
        List<TblFailureInformationDistributionGroup> tblDistGroupList = MockDataHelper.mockFailureInformationDistributionGroupList();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(tblDistGroupList);


        List<DistributionGroupDto> distributionGroups = failureInformationDistributionGroupService.findDistributionGroupsByFailureInfo(UUID.randomUUID());

        assertEquals(distributionGroupDtoList.size(), distributionGroups.size());
        assertEquals(2, distributionGroupDtoList.size());
        assertEquals(distributionGroupDtoList.get(1).getName(), distributionGroups.get(1).getName());
    }

    @Test
    public void shouldInsertADistributionGroupForFailureInfo() {
        TblFailureInformationDistributionGroup failureInfoGroup = MockDataHelper.mockTblFailureInformationDistributionGroup();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup distributionGroup = MockDataHelper.mockTblDistributionGroup();
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(distributionGroup));
        when(failureInformationDistributionGroupRepository.save(any(TblFailureInformationDistributionGroup.class))).thenReturn(failureInfoGroup);

        FailureInformationDistributionGroupDto savedDto = failureInformationDistributionGroupService.insertFailureInfoDistributionGroup(UUID.randomUUID(), groupDto);

        assertNotNull(savedDto.getDistributionGroupId());
        assertEquals(savedDto.getDistributionGroupId(), failureInfoGroup.getDistributionGroup().getId());
        assertEquals(savedDto.getFailureInformationId(), failureInfoGroup.getFailureInformation().getId());
    }


    @Test
    public void shouldInsertADistributionGroup2() {
        TblFailureInformationDistributionGroup failureInfoGroup = MockDataHelper.mockTblFailureInformationDistributionGroup();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup distributionGroup = MockDataHelper.mockTblDistributionGroup();
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        List<TblFailureInformationDistributionGroup> dgList = MockDataHelper.mockTblFailureInformationDistributionGroupList();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(distributionGroup));
        when(failureInformationDistributionGroupRepository.save(any(TblFailureInformationDistributionGroup.class))).thenReturn(failureInfoGroup);
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(dgList);
        FailureInformationDistributionGroupDto savedDto = failureInformationDistributionGroupService.insertFailureInfoDistributionGroup(UUID.randomUUID(), groupDto);

        assertNotNull(savedDto.getDistributionGroupId());
        assertEquals(savedDto.getDistributionGroupId(), failureInfoGroup.getDistributionGroup().getId());
        assertEquals(savedDto.getFailureInformationId(), failureInfoGroup.getFailureInformation().getId());
    }

    @Test
    public void shouldNotInsertADistributionGroupCauseOfException() {
        TblFailureInformationDistributionGroup failureInfoGroup = MockDataHelper.mockTblFailureInformationDistributionGroup();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup distributionGroup = MockDataHelper.mockTblDistributionGroup();
        DistributionGroupDto groupDto = MockDataHelper.mockDistributionGroupDto();
        List<TblFailureInformationDistributionGroup> dgList = MockDataHelper.mockTblFailureInformationDistributionGroupList();
        dgList.get(0).getDistributionGroup().setUuid(groupDto.getUuid());

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(distributionGroup));
        when(failureInformationDistributionGroupRepository.save(any(TblFailureInformationDistributionGroup.class))).thenReturn(failureInfoGroup);
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(dgList);
        assertThrows(OperationDeniedException.class, () -> failureInformationDistributionGroupService.insertFailureInfoDistributionGroup(UUID.randomUUID(), groupDto));
    }

    @Test
    public void shouldRemoveDistributionGroupFromFailureInfo() {
        TblFailureInformationDistributionGroup failureInfoGroup = MockDataHelper.mockTblFailureInformationDistributionGroup();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup distributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(distributionGroup));
        when(failureInformationDistributionGroupRepository.findByFailureInformationIdAndDistributionGroupId(anyLong(), anyLong())).thenReturn(Optional.of(failureInfoGroup));
        Mockito.doNothing().when(failureInformationDistributionGroupRepository).delete( isA( TblFailureInformationDistributionGroup.class ));

        failureInformationDistributionGroupService.deleteFailureInfoDistributionGroup(UUID.randomUUID(), UUID.randomUUID());

        Mockito.verify(failureInformationDistributionGroupRepository, times(1)).delete( failureInfoGroup );
    }

}
