/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqProperties;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
class ExportServiceTest {

    @MockBean
    RabbitMqConfig rabbitMqConfig;

    @MockBean
    private FailureInformationService failureInformationService;

    @MockBean
    private DistributionGroupMemberService distributionGroupMemberService;

    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @MockBean
    private FailureInformationPublicationChannelRepository publicationChannelRepository;

    @MockBean
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @MockBean
    private DistributionGroupRepository distributionGroupRepository;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private MessageChannel mailExportChannel;

    /*@MockBean
    private FailureInformationMapper failureInformationMapper;*/

    @MockBean
    RabbitTemplate rabbitTemplate;

    @MockBean
    private RabbitMqProperties rabbitMqProperties;

    @Autowired
    private ExportService exportService;

    @MockBean
    private ProcessHelper processHelper;

    @MockBean
    RestTemplate restTemplate;

    @Test
    void shouldNotExportFailureBecauseAlreadyPublished() {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setResponsibility(null);
        tblFailureInformation.setVoltageLevel(null);
        tblFailureInformation.setPressureLevel(null);
        tblFailureInformation.setFailureBegin(null);
        tblFailureInformation.setFailureEndPlanned(null);
        tblFailureInformation.setFailureEndResupplied(null);
        tblFailureInformation.setStreet(null);
        tblFailureInformation.setDistrict(null);
        tblFailureInformation.setCity(null);
        tblFailureInformation.setRefExpectedReason(null);

        List<TblFailureInformationPublicationChannel> pubChannelList = MockDataHelper.mockTblFailureInformationPublicationChannelList();
        pubChannelList.get(0).setPublished(true);
        pubChannelList.get(0).setPublicationChannel("Mail (lang)");
        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (lang)"));
        when(publicationChannelRepository.findByTblFailureInformation(tblFailureInformation)).thenReturn(pubChannelList);
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(0)).convertAndSend(any(), any(), any(Object.class));
        assertFalse(isMailPushed);

    }

    @Test
    void shouldNotExportFailureBecauseAlreadyPublished2() {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setResponsibility(null);
        tblFailureInformation.setVoltageLevel(null);
        tblFailureInformation.setPressureLevel(null);
        tblFailureInformation.setFailureBegin(null);
        tblFailureInformation.setFailureEndPlanned(null);
        tblFailureInformation.setFailureEndResupplied(null);
        tblFailureInformation.setStreet(null);
        tblFailureInformation.setDistrict(null);
        tblFailureInformation.setCity(null);
        tblFailureInformation.setRefExpectedReason(null);

        when(publicationChannelRepository.findByTblFailureInformation(eq(tblFailureInformation))).thenReturn(null);
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(0)).convertAndSend(any(), any(), any(Object.class));
        assertFalse(isMailPushed);

    }


    @Test
    void shouldExportFailureInfoAsMail() throws Exception {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        List<TblFailureInformationDistributionGroup> listTblDistributionGroups= MockDataHelper.mockTblFailureInformationDistributionGroupList();

        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        RefStatus refStatus = MockDataHelper.mockRefStatusQUALIFIED();
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(listTblDistributionGroups);
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(3)).convertAndSend(any(), any(), any(Object.class));
        assertTrue(isMailPushed);

    }

    @Test
    void shouldExportFailureInfoAsMail_emptyFieldsForContentReplace() throws Exception {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setResponsibility(null);
        tblFailureInformation.setVoltageLevel(null);
        tblFailureInformation.setPressureLevel(null);
        tblFailureInformation.setFailureBegin(null);
        tblFailureInformation.setFailureEndPlanned(null);
        tblFailureInformation.setFailureEndResupplied(null);
        tblFailureInformation.setStreet(null);
        tblFailureInformation.setDistrict(null);
        tblFailureInformation.setCity(null);
        tblFailureInformation.setRefExpectedReason(null);

        List<TblFailureInformationDistributionGroup> listTblDistributionGroups= MockDataHelper.mockTblFailureInformationDistributionGroupList();
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(listTblDistributionGroups);
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();
        RefStatus refStatus = MockDataHelper.mockRefStatusQUALIFIED();
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(3)).convertAndSend(any(), any(), any(Object.class));
        assertTrue(isMailPushed);

    }


    @Test
    void shouldExportFailureInfoAsMail_noChannelToSave() throws Exception {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        tblFailureInformation.setResponsibility(null);
        tblFailureInformation.setVoltageLevel(null);
        tblFailureInformation.setPressureLevel(null);
        tblFailureInformation.setFailureBegin(null);
        tblFailureInformation.setFailureEndPlanned(null);
        tblFailureInformation.setFailureEndResupplied(null);
        tblFailureInformation.setStreet(null);
        tblFailureInformation.setDistrict(null);
        tblFailureInformation.setCity(null);
        tblFailureInformation.setRefExpectedReason(null);
        tblFailureInformation.setInternalRemark("intRem");
        tblFailureInformation.setDescription("Descr");
        tblFailureInformation.setPostcode("112233");
        tblFailureInformation.setHousenumber("10-1");

        RefStatus refStatus = MockDataHelper.mockRefStatusQUALIFIED();
        List<TblFailureInformationDistributionGroup> listTblDistributionGroups= MockDataHelper.mockTblFailureInformationDistributionGroupList();
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(failureInformationDistributionGroupRepository.findByFailureInformationId(anyLong())).thenReturn(listTblDistributionGroups);
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);

        List<TblFailureInformationPublicationChannel> pubCList = MockDataHelper.mockTblFailureInformationPublicationChannelList();
        pubCList.get(0).setPublicationChannel("Mail");
        pubCList.get(0).setPublished(false);
        when(publicationChannelRepository.findByTblFailureInformation(any(TblFailureInformation.class)))
                .thenReturn(pubCList);
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(3)).convertAndSend(any(), any(), any(Object.class));
        assertTrue(isMailPushed);

    }


    @Test
    void shouldExportFailureInfoToStoerungsauskunft() throws Exception {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelStoerungsauskunft();
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        List<TblDistributionGroup> listTblDistributionGroups= MockDataHelper.mockDistributionGroupList();
        //tblFailureInformation.setDistributionGroups(listTblDistributionGroups);
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        when(failureInformationService.enrichFailureInfo(any())).thenReturn(new FailureInformationDto());
        when(processHelper.isFailureInfoPlanned(any(FailureInformationDto.class ))).thenReturn(false);
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());

        String[] channels = {"Störungsauskunft.de"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        Mockito.verify(rabbitTemplate, times(1)).convertAndSend(any(), any(), any(Object.class));
        assertTrue(isMailPushed);

    }

    @Test
    void shouldNotExportFailureInfoAsMails_noDistributionGroups() throws Exception {

        ExportService exportServiceLocal = spy(exportService);

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        //tblFailureInformation.setDistributionGroups(null);

        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        assertFalse(isMailPushed);

    }

    @Test
    void shouldNotExportFailureInfoAsMails_noMailAddresses() throws Exception {

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        List<TblDistributionGroup> listTblDistributionGroups= MockDataHelper.mockDistributionGroupList();
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        //tblFailureInformation.setDistributionGroups(listTblDistributionGroups);
        listTblDistributionGroupMemberDtos.forEach(x->x.setEmail(null));

        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());

        String[] channels = {"Mail (lang)"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        assertFalse(isMailPushed);

    }

    @Test
    void shouldNotExportFailureInfoAsMails_incorrectChannels() throws Exception {

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        List<TblDistributionGroup> listTblDistributionGroups= MockDataHelper.mockDistributionGroupList();
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        listTblDistributionGroupMemberDtos.forEach(x->x.setEmail(null));

        when(failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());

        String[] channels = {"TEST", "TEST2"};

        boolean isMailPushed = exportServiceLocal.exportFailureInformation(UUID.randomUUID(), channels, null);

        assertFalse(isMailPushed);

    }

    @Test
    void shouldExportReminderSMS() throws Exception{

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailShort();

        RefStatus refStatus = MockDataHelper.mockRefStatusQUALIFIED();
        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (kurz)"));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupRepository.findByName(any(String.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);

        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(MockDataHelper.mockRabbitMqchannelTypeToNameMap()).when(rabbitMqConfig).getChannelTypeToNameMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        when(mailExportChannel.send(any(Message.class))).thenReturn(true);

        boolean isMailPushed = exportServiceLocal.exportFailureInformationReminderMail(UUID.randomUUID());

        Mockito.verify(restTemplate, times(1)).postForObject(anyString(), any(), any());
        assertTrue(isMailPushed);

    }

    @Test
    void shouldExportReminderMail() throws Exception{

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();

        RefStatus refStatus = MockDataHelper.mockRefStatusQUALIFIED();
        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (lang)"));
        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupRepository.findByName(any(String.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);

        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(MockDataHelper.mockRabbitMqchannelTypeToNameMap()).when(rabbitMqConfig).getChannelTypeToNameMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        when(mailExportChannel.send(any(Message.class))).thenReturn(true);

        boolean isMailPushed = exportServiceLocal.exportFailureInformationReminderMail(UUID.randomUUID());

        Mockito.verify(rabbitTemplate, times(1)).convertAndSend(any(), any(), any(Object.class));
        assertTrue(isMailPushed);

    }

    @Test
    void shouldNotExportReminderMail_noChannels() throws Exception{

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<DistributionGroupMemberDto> listTblDistributionGroupMemberDtos =  MockDataHelper.mockDistributionGroupMemberDtoList();

        RabbitMqChannel rabbitMqChannel =  null;

        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (lang)"));
        when (failureInformationRepository.findByUuid( any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(distributionGroupRepository.findByName(any(String.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberService.getMembersByGroupId(any(UUID.class))).thenReturn(listTblDistributionGroupMemberDtos);

        when(publicationChannelRepository.save(any(TblFailureInformationPublicationChannel.class))).thenReturn(new TblFailureInformationPublicationChannel());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        when(mailExportChannel.send(any(Message.class))).thenReturn(true);

        assertThrows(NotFoundException.class, () -> exportServiceLocal.exportFailureInformationReminderMail(UUID.randomUUID()));
    }

    @Test
    void shouldNotExportReminderMail_noDistributionGroup() throws Exception{

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (lang)"));
        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        when(distributionGroupRepository.findByName(any(String.class))).thenReturn(Optional.empty());

        UUID rand = UUID.randomUUID();
        assertThrows(NotFoundException.class,
                () -> exportServiceLocal.exportFailureInformationReminderMail(rand));

    }


    @Test
    void shouldNotExportReminderMail_noDistributionGroup2() throws Exception{

        ExportService exportServiceLocal = spy(exportService);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        when(processHelper.getChannelsToPublishList(any(), anyBoolean())).thenReturn(Arrays.asList("Mail (lang)"));
        RabbitMqChannel rabbitMqChannel =  MockDataHelper.mockRabbitMqChannelMailLong();
        doReturn(MockDataHelper.mockRabbitMqchannelNameToTypeMap()).when(rabbitMqConfig).getChannelNameToTypeMap();
        doReturn(rabbitMqChannel).when(exportServiceLocal).getAvailableRabbitMqChannel(anyString());
        doNothing().when(rabbitMqConfig).checkExchangeAndQueueOnRabbitMq(anyString(), anyString());
        when(distributionGroupRepository.findByName(any(String.class))).thenReturn(Optional.empty());
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));

        UUID rand = UUID.randomUUID();
        assertThrows(NotFoundException.class,
                () -> exportServiceLocal.exportFailureInformationReminderMail(rand));

    }

}
