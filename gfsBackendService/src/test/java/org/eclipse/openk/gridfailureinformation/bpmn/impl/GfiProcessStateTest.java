/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GfiProcessStateTest {
    @Test
    public void testAll() {
        assertEquals(GfiProcessState.NEW , GfiProcessState.fromValue(1) );
        assertEquals(GfiProcessState.PLANNED, GfiProcessState.fromValue(2) );
        assertEquals(GfiProcessState.CREATED, GfiProcessState.fromValue(3) );
        assertEquals(GfiProcessState.CANCELED, GfiProcessState.fromValue(4) );
        assertEquals(GfiProcessState.QUALIFIED, GfiProcessState.fromValue(5) );
        assertEquals(GfiProcessState.UPDATED, GfiProcessState.fromValue(6) );
        assertEquals(GfiProcessState.COMPLETED, GfiProcessState.fromValue(7) );
        assertEquals(GfiProcessState.UNDEFINED_, GfiProcessState.fromValue(-1) ); // NOSONAR
    }

}
