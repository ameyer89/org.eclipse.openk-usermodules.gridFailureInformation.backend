/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupMemberRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class DistributionGroupServiceTest {
    @Qualifier("myDistributionGroupService")
    @Autowired
    private DistributionGroupService distributionGroupService;

    @MockBean
    private DistributionGroupRepository distributionGroupRepository;

    @MockBean
    private DistributionGroupMemberRepository distributionGroupMemberRepository;

    @Test
    public void shouldGetAllDistributionGroups() {
        List<TblDistributionGroup> distributionGroups = MockDataHelper.mockDistributionGroupList();
        when(distributionGroupRepository.findAll()).thenReturn(distributionGroups);
        List<DistributionGroupDto> distributionGroupDtoList = distributionGroupService.getDistributionGroups();

        assertEquals(distributionGroupDtoList.size(), distributionGroups.size());
        assertEquals(2, distributionGroupDtoList.size());
        assertEquals(distributionGroupDtoList.get(1).getUuid(), distributionGroups.get(1).getUuid());
    }

    @Test
    public void shouldFindASingleDistributionGroup() {
        TblDistributionGroup distributionGroup = MockDataHelper.mockTblDistributionGroup();
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(distributionGroup));
        DistributionGroupDto dto = distributionGroupService.getDistributionGroupByUuid(UUID.randomUUID());

        assertEquals(distributionGroup.getUuid(), dto.getUuid());
    }

    @Test
    public void shouldThrowExceptionWhenGroupNotFound() {
        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class,
                () -> distributionGroupService.getDistributionGroupByUuid(UUID.randomUUID()));
    }

    @Test
    public void shouldInsertADistributionGroup() {
        DistributionGroupDto dgDto = MockDataHelper.mockDistributionGroupDto();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.save(any(TblDistributionGroup.class))).thenReturn(tblDistributionGroup);
        DistributionGroupDto savedDto = distributionGroupService.insertDistributionGroup(dgDto);

        assertNotNull(savedDto.getUuid());
        assertEquals(savedDto.getUuid(), tblDistributionGroup.getUuid());
    }


    @Test
    public void shouldDeleteEmptyDistributionGroup() {
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        Mockito.doNothing().when(distributionGroupRepository).delete( isA( TblDistributionGroup.class ));

        distributionGroupService.deleteDistributionGroup(tblDistributionGroup.getUuid());

        Mockito.verify(distributionGroupRepository, times(1)).delete( tblDistributionGroup );
    }


    @Test
    public void shouldDeleteDistributionGroupWithMembers() {
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();
        List<TblDistributionGroupMember> tblDistributionGroupMemberList = MockDataHelper.mockDistributionGroupMemberList();

        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupMemberRepository.findByTblDistributionGroupUuid(any(UUID.class))).thenReturn(tblDistributionGroupMemberList);

        Mockito.doNothing().when(distributionGroupRepository).delete( isA( TblDistributionGroup.class ));

        distributionGroupService.deleteDistributionGroup(tblDistributionGroup.getUuid());

        Mockito.verify(distributionGroupRepository, times(1)).delete( tblDistributionGroup );
        Mockito.verify(distributionGroupMemberRepository, times(1)).delete( tblDistributionGroupMemberList.get(0) );
        Mockito.verify(distributionGroupMemberRepository, times(1)).delete( tblDistributionGroupMemberList.get(1) );
    }

    @Test
    public void shouldUpdateDistributionGroup() {
        DistributionGroupDto distributionGroupDto = MockDataHelper.mockDistributionGroupDto();
        TblDistributionGroup tblDistributionGroup = MockDataHelper.mockTblDistributionGroup();

        when(distributionGroupRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblDistributionGroup));
        when(distributionGroupRepository.save(any(TblDistributionGroup.class))).thenReturn(tblDistributionGroup);
        DistributionGroupDto savedDistributionGroupDto = distributionGroupService.updateGroup(distributionGroupDto);

        assertEquals(tblDistributionGroup.getUuid(), savedDistributionGroupDto.getUuid());
    }
}
