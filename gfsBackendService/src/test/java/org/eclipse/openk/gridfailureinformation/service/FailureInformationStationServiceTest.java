/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

class FailureInformationStationServiceTest {
    @Qualifier("myFailureInformationStationService")
    @Autowired
    private FailureInformationStationService failureInformationStationService;

    @Autowired
    private StationMapper stationMapper;

    @MockBean
    private FailureInformationStationRepository failureInformationStationRepository;

    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @MockBean
    private StationRepository stationRepository;

    @Test
    void shouldGetAllStationsForFailureInfo() {
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        List<StationDto> stationDtoList = MockDataHelper.mockTblStationList().stream()
                .map( stationMapper::toStationDto )
                .collect(Collectors.toList());

        //when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        List<UUID> fiStationList = new LinkedList<>();
        fiStationList.add(UUID.randomUUID());
        fiStationList.add(UUID.randomUUID());
        when(failureInformationStationRepository.findUuidByFkTblFailureInformation(any(UUID.class)))
                    .thenReturn(fiStationList);
        //List<StationDto> stations = failureInformationStationService.findStationsByFailureInfo(UUID.randomUUID());
        when(stationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(MockDataHelper.mockTblStation()));
        assertEquals(stationDtoList.size(), fiStationList.size());
        assertEquals(2, stationDtoList.size());
     }

    @Test
    void shouldInsertAStationForFailureInfo() {
        TblFailinfoStation failureInfoGroup = MockDataHelper.mockTblFailureInformationStation();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblStation station = MockDataHelper.mockTblStation();
        StationDto stationDto = MockDataHelper.mockStationDto();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(stationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(station));
        when(failureInformationStationRepository.save(any(TblFailinfoStation.class))).thenReturn(failureInfoGroup);

        FailureInformationStationDto savedDto = failureInformationStationService.insertFailureInfoStation(UUID.randomUUID(), stationDto);

        assertNotNull(savedDto.getStationStationId());
        assertEquals(savedDto.getStationStationId(), failureInfoGroup.getStationStationId());
        assertEquals(savedDto.getFailureInformationId(), failureInfoGroup.getFailureInformation().getId());
    }

    @Test
    void shouldRemoveStationFromFailureInfo() {
        TblFailinfoStation failureInfoStation = MockDataHelper.mockTblFailureInformationStation();
        TblFailureInformation failureInformation = MockDataHelper.mockTblFailureInformation();
        TblStation station = MockDataHelper.mockTblStation();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(failureInformation));
        when(stationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(station));
        when(failureInformationStationRepository.findByFailureInformationIdAndStationStationId(anyLong(), anyString())).thenReturn(Optional.of(failureInfoStation));
        Mockito.doNothing().when(failureInformationStationRepository).delete( isA( TblFailinfoStation.class ));

        failureInformationStationService.deleteFailureInfoStation(UUID.randomUUID(), UUID.randomUUID());

        Mockito.verify(failureInformationStationRepository, times(1)).delete( failureInfoStation );
    }

}
