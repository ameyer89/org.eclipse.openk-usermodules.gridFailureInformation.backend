/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationReminderMailSentRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class FailureInformationReminderMailSentServiceTest {
    @Qualifier("myFailureInformationReminderMailSentService")
    @Autowired
    private FailureInformationReminderMailSentService failureInformationReminderMailSentService;

    @MockBean
    private FailureInformationService failureInformationService;

    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository;

    @MockBean
    private ExportService exportService;

    @Test
    void shouldDisplayReminder() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCreated();

        Date reminderDate = Date.from( Instant.now().plus(Duration.ofHours(2)));
        mockfailurePage.getContent().stream().findFirst().get().setFailureEndPlanned(reminderDate);
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();

        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.empty());


        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        assertEquals(true, reminderIsDisplayed);
    }

    @Test
    void shouldNotDisplayReminder_FailureInfoCompleted() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCOMPLETED();

        Date reminderDate = Date.from( Instant.now().plus(Duration.ofHours(2)));
        mockfailurePage.getContent().forEach(f -> {
            f.setFailureEndPlanned(reminderDate);
            f.setRefStatusIntern(mockRefStatus);
        });
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.empty());

        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        assertEquals(false, reminderIsDisplayed);
    }

    @Test
    void shouldNotDisplayReminder_FailureInfoCancelled() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCANCELED();

        Date reminderDate = Date.from( Instant.now().plus(Duration.ofHours(2)));
        mockfailurePage.getContent().forEach(f -> {
            f.setFailureEndPlanned(reminderDate);
            f.setRefStatusIntern(mockRefStatus);
        });
        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.empty());

        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        assertEquals(false, reminderIsDisplayed);
    }


    @Test
    void shouldNotDisplayReminder_NoExpectedStatusChangeFound() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        Date reminderDate = Date.from( Instant.now().plus(Duration.ofHours(96)));
        mockfailurePage.getContent().forEach(f -> f.setFailureEndPlanned(reminderDate));
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCreated();

        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        assertEquals(false, reminderIsDisplayed);
    }

    @Test
    void shouldNotDisplayReminder_NoFailureEndPlanned() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        mockfailurePage.getContent().stream().forEach(f -> f.setFailureEndPlanned(null));
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCreated();

        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        assertEquals(false, reminderIsDisplayed);
    }

    @Test
    void shouldSendReminderMails() {

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        List<FailureInformationDto> failureInformationDtoList = MockDataHelper.mockGridFailureInformationDtos();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.empty());
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        failureInformationReminderMailSentService.sendReminderMails(failureInformationDtoList);
        Mockito.verify(failureInformationReminderMailSentRepository, times(2)).save(any(TblFailureInformationReminderMailSent.class));

    }

    @Test
    void shouldNotSendNoReminderMails_MailsAlreadySent() {

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblFailureInformationReminderMailSent failureInfoReminderMailSent = MockDataHelper.mockTblFailureInformationReminderMailSent();
        List<FailureInformationDto> failureInformationDtoList = MockDataHelper.mockGridFailureInformationDtos();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.of(failureInfoReminderMailSent));
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        failureInformationReminderMailSentService.sendReminderMails(failureInformationDtoList);

        verify(exportService, never()).exportFailureInformationReminderMail(any(UUID.class));
    }

    @Test
    void shouldSendNoReminderMails_MailNotSentYet() {

        TblFailureInformation tblFailureInformation = MockDataHelper.mockTblFailureInformation();
        TblFailureInformationReminderMailSent failureInfoReminderMailSent = MockDataHelper.mockTblFailureInformationReminderMailSentFuture();
        List<FailureInformationDto> failureInformationDtoList = MockDataHelper.mockGridFailureInformationDtos();

        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(tblFailureInformation));
        when(failureInformationReminderMailSentRepository.findByTblFailureInformation(any(TblFailureInformation.class))).thenReturn(Optional.of(failureInfoReminderMailSent));
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        failureInformationReminderMailSentService.sendReminderMails(failureInformationDtoList);

        verify(failureInformationReminderMailSentRepository, times(2)).save(any(TblFailureInformationReminderMailSent.class));
        verify(exportService, times(2)).exportFailureInformationReminderMail(any(UUID.class));
    }

    @Test
    void shouldNotSendReminderMails_FailureInfoCompleted() {
        Page<TblFailureInformation> mockfailurePage = MockDataHelper.mockTblFailureInformationPage();
        RefStatus mockRefStatus = MockDataHelper.mockRefStatusCOMPLETED();
        mockfailurePage.getContent().forEach(f -> {
            f.setRefStatusIntern(null);
            f.setRefStatusIntern(mockRefStatus);
        });

        when(failureInformationService.findFailureInformationsForDisplay(any(Pageable.class))).thenReturn(mockfailurePage);
        when(statusRepository.findById(any(Long.class))).thenReturn(Optional.of(mockRefStatus));
        when(exportService.exportFailureInformationReminderMail(any(UUID.class))).thenReturn(true);

        Boolean reminderIsDisplayed = failureInformationReminderMailSentService.displayStatusChangeReminderAndSendMails();

        verify(exportService, never()).exportFailureInformationReminderMail(any(UUID.class));
        assertEquals(false, reminderIsDisplayed);

    }
}
