/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config;

import lombok.Getter;
import lombok.Setter;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.util.VisibilityConfigurationRaw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.rabbitmq", ignoreUnknownFields = true)
public class FESettings {

    private List<RabbitMqChannel> channels = new ArrayList<>();

    @Autowired
    VisibilityConfigurationRaw visibilityConfigurationRaw;

    @Value("${spring.settings.overviewMapInitialZoom}")
    public String overviewMapInitialZoom;
    @Value("${spring.settings.detailMapInitialZoom}")
    public String detailMapInitialZoom;
    @Value("${spring.settings.overviewMapInitialLatitude}")
    public String overviewMapInitialLatitude;
    @Value("${spring.settings.overviewMapInitialLongitude}")
    public String overviewMapInitialLongitude;

    @Value("${spring.settings.mapTileAttribution:}")
    public String mapTileAttribution;
    @Value("${spring.settings.mapTileLayerUrl:}")
    public String mapTileLayerUrl;
    @Value("${spring.settings.mapWmsLayer:}")
    public String mapWmsLayer;
    @Value("${spring.settings.iconUrl:}")
    public String iconUrl;

    @Value("${spring.settings.dataExternInitialVisibility}")
    public String dataExternInitialVisibility;

    @Value("${spring.settings.emailSubjectCompleteInit:}")
    private String emailSubjectCompleteInit;
    @Value("${spring.settings.emailContentCompleteInit:}")
    private String emailContentCompleteInit;
    @Value("${spring.settings.emailSubjectUpdateInit:}")
    private String emailSubjectUpdateInit;
    @Value("${spring.settings.emailContentUpdateInit:}")
    private String emailContentUpdateInit;
    @Value("${spring.settings.emailSubjectPublishInit:}")
    private String emailSubjectPublishInit;
    @Value("${spring.settings.emailContentPublishInit:}")
    private String emailContentPublishInit;

    @Value("${spring.settings.emailSubjectCompleteShortInit:}")
    private String emailSubjectCompleteShortInit;
    @Value("${spring.settings.emailContentCompleteShortInit:}")
    private String emailContentCompleteShortInit;
    @Value("${spring.settings.emailSubjectUpdateShortInit:}")
    private String emailSubjectUpdateShortInit;
    @Value("${spring.settings.emailContentUpdateShortInit:}")
    private String emailContentUpdateShortInit;
    @Value("${spring.settings.emailSubjectPublishShortInit:}")
    private String emailSubjectPublishShortInit;
    @Value("${spring.settings.emailContentPublishShortInit:}")
    private String emailContentPublishShortInit;

    @Value("${spring.settings.plzLookupEnabled:false}")
    private boolean plzLookupEnabled;
    @Value("${spring.settings.nominatimURL:}")
    private String nominatimURL;


}

