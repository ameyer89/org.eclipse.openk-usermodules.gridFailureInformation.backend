/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationPolygonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class StationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private StationMapper stationMapper;

    @Autowired
    private FailureInformationService failureInformationService;

    public List<StationDto> getStations() {
        return stationRepository.findAll().stream()
                .map(stationMapper::toStationDto)
                .sorted( (x, y) -> (x.getStationName()+"@"+ x.getStationId())
                        .compareTo(y.getStationName()+"@"+y.getStationId()))
                .collect(Collectors.toList());
    }


    public StationDto getStationsById(String stationId) {
        Optional<TblStation> tblStation = stationRepository.findByStationId(stationId);
        if( tblStation.isPresent()) {
            return stationMapper.toStationDto(tblStation.get());
        }
        return null;
    }

    public List<StationPolygonDto> getPolygonCoordinates(List<UUID> stationUuids) {
        return failureInformationService.getPolygonCoordinatesForStationUuids(stationUuids);
    }

    public long getCountAddresses(List<UUID> stationUuids) {

        long count = 0;

        for (UUID station : stationUuids) {

            TblStation tblStation = stationRepository
                    .findByUuid(station)
                    .orElse(null);
            if (tblStation != null) {
                count += addressRepository.findByStationId(tblStation.getStationId() + "").size();
            }
        }

        return count;
    }
}
