/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.config.auth;

import org.eclipse.openk.gridfailureinformation.exceptions.UnauthorizedException;
import org.keycloak.RSATokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Value("${jwt.useStaticJwt}")
    private boolean useStaticJwt;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.staticJwt}")
    private String staticJwt;

    @Value("${jwt.requiredRole:}")
    private String requiredRole;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authenticationHeader = useStaticJwt ? staticJwt : request.getHeader(this.tokenHeader);

        if (response.getStatus() == HttpServletResponse.SC_UNAUTHORIZED) return;

        try {
            SecurityContext context = SecurityContextHolder.getContext();

            if (authenticationHeader != null) {

                final String bearerTkn = authenticationHeader.replace("Bearer ", "");

                createToken(context, bearerTkn);

            }
            chain.doFilter(request, response);
        } catch (AuthenticationException | UnauthorizedException ex) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private void createToken(SecurityContext context, String bearerTkn) throws AuthenticationException,
            ServletException {
        try {

            List<String> allRoles = new ArrayList<>();
            AccessToken token = RSATokenVerifier.create(bearerTkn).getToken();
            //Clientroles
            token.getResourceAccess().forEach((client, access) -> allRoles.addAll(access.getRoles()));

            //Realmroles
            if (token.getRealmAccess() != null) {
                allRoles.addAll(token.getRealmAccess().getRoles());
            }

            List<GrantedAuthority> authorities= new ArrayList<>();
            allRoles.forEach( x -> authorities.add(new SimpleGrantedAuthority("ROLE_"+x.toUpperCase())));

            UsernamePasswordAuthenticationToken auth =
                    new UsernamePasswordAuthenticationToken(token.getPreferredUsername(), null, authorities);
            auth.setDetails(bearerTkn);

            // require an additional Role
            if (!requiredRole.isEmpty() && !allRoles.contains(requiredRole)) {
                throw new UnauthorizedException("Has not the required role " + requiredRole);
            }

            context.setAuthentication(auth);

        } catch (AuthenticationException | UnauthorizedException | VerificationException ua) {
            throw new UnauthorizedException("Invalid Token");
        } catch (Exception e) {
            throw new ServletException("Internal Server Error");
        }
    }
}
