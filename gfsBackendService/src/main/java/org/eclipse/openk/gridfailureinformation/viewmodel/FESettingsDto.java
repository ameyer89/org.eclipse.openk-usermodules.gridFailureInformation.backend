/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FESettingsDto implements Serializable {
    @Data
    public static class FieldVisibility implements Serializable  {
        private String failureClassification;
        private String responsibility;
        private String description;
        private String internalRemark;
    }
    @Data
    public static class TableInternColumnVisibility implements Serializable  {
        private String failureClassification;
        private String responsibility;
        private String description;
        private String statusIntern;
        private String statusExtern;
        private String publicationStatus;
        private String branch;
        private String voltageLevel;
        private String pressureLevel;
        private String failureBegin;
        private String failureEndPlanned;
        private String failureEndResupplied;
        private String expectedReasonText;
        private String internalRemark;
        private String postcode;
        private String city;
        private String district;
        private String street;
        private String housenumber;
        private String radius;
        private String stationIds;
    }
    @Data
    public static class TableExternColumnVisibility implements Serializable  {
        private String branch;
        private String failureBegin;
        private String failureEndPlanned;
        private String expectedReasonText;
        private String description;
        private String postcode;
        private String city;
        private String district;
        private String street;
        private String failureClassification;
    }
    @Data
    public static class MapExternTooltipVisibility implements Serializable  {
        private String failureBegin;
        private String failureEndPlanned;
        private String expectedReasonText;
        private String branch;
        private String postcode;
        private String city;
        private String district;
    }
    @Data
    public static class VisibilityConfiguration implements Serializable {
        private FieldVisibility fieldVisibility;
        private TableInternColumnVisibility tableInternColumnVisibility;
        private TableExternColumnVisibility tableExternColumnVisibility;
        private MapExternTooltipVisibility mapExternTooltipVisibility;
    }

    private List<String> exportChannels;
    private String overviewMapInitialZoom;
    private String detailMapInitialZoom;

    private String overviewMapInitialLatitude;
    private String overviewMapInitialLongitude;

    private String mapTileAttribution;
    private String mapTileLayerUrl;
    private String mapWmsLayer;
    private String iconUrl;

    private String dataExternInitialVisibility;

    private VisibilityConfiguration visibilityConfiguration;

    private boolean plzLookupEnabled;
    private String nominatimURL;
}
