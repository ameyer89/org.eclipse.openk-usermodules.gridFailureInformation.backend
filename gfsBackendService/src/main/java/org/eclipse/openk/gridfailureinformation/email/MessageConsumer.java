/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.email;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.viewmodel.RabbitMqMessageDto;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Log4j2
@Component
public class MessageConsumer {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmailService emailService;

    @RabbitListener(queues = "${spring.rabbitmq.mail_export_queue}")
    public void listenMessage(Message message) {
        try {
            getMessage(message);
        } catch (Exception e) {
            log.error("Error while receiving a message from rabbitmq", e);
        }
    }

    public void getMessage(Message message) throws JsonProcessingException, MessagingException {

        SimpleMessageConverter converter = new SimpleMessageConverter();
        String messagePayload = (String) converter.fromMessage(message);
        RabbitMqMessageDto mailMessageDto = objectMapper.readValue(messagePayload, RabbitMqMessageDto.class);
        emailService.sendMail(mailMessageDto);

    }
}