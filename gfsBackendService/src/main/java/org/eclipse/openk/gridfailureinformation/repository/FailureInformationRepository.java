/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */

package org.eclipse.openk.gridfailureinformation.repository;

import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface FailureInformationRepository extends PagingAndSortingRepository<TblFailureInformation, Long > {

    @Query("SELECT a from TblFailureInformation a WHERE a.tblFailureInformationCondensed IS NULL " +
            "AND ( a.refStatusIntern.id NOT IN (:statusClosedId, :statusCancelledId ) OR " +
            "(a.refStatusIntern.id = :statusClosedId AND a.failureEndResupplied IS NOT NULL AND a.failureEndResupplied >= :dateNowFourWeeksAgo) OR" +
            "(a.refStatusIntern.id = :statusClosedId AND a.failureEndResupplied IS NULL AND a.modDate >= :dateNowFourWeeksAgo) OR" +
            "(a.refStatusIntern.id = :statusCancelledId AND a.modDate >= :dateNowFourWeeksAgo) ) ORDER BY a.failureBegin DESC")
    Page<TblFailureInformation> findByTblFailureInformationForDisplay(long statusClosedId, long statusCancelledId,
                                                                      Date dateNowFourWeeksAgo, Pageable pageable); // NOSONAR

    Optional<TblFailureInformation> findByUuid(UUID uuid);

    List<TblFailureInformation> findByUuidIn(List<UUID> uuidList);

    List<TblFailureInformation> findByPublicationStatus(String publicationStatus);

    Optional<TblFailureInformation> findByObjectReferenceExternalSystem(String extRef );

    @Query("select fi from TblFailureInformation fi where fi.tblFailureInformationCondensed.uuid = :uuid")
    List<TblFailureInformation> findByFailureInformationCondensedUuid(@Param("uuid") UUID uuid);

    List<TblFailureInformation> findByRefStatusInternNotAndRefStatusInternNot(RefStatus abgeschlossen, RefStatus storniert);
}
