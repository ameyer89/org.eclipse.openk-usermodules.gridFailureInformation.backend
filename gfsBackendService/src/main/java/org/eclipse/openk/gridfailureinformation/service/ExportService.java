/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.api.SitCacheApi;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.ResourceConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqProperties;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.model.RefExpectedReason;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.RefRadius;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationDistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.util.ExternalStatusCalculator;
import org.eclipse.openk.gridfailureinformation.util.GroupMemberPlzFilter;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionTextPlaceholderDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExportGridFailures;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExportSettings;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.SMSDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Log4j2
@Validated
@EnableConfigurationProperties
@PropertySource(value = "classpath:config-${spring.profiles.active}.yml", ignoreResourceNotFound = true)
public class ExportService {

    @Lazy
    @Autowired
    private RabbitMqConfig rabbitMqConfig;

    @Autowired
    private ProcessHelper processHelper;

    @Autowired
    private RabbitMqProperties rabbitMqProperties;

    @Autowired
    private org.springframework.amqp.rabbit.core.RabbitTemplate rabbitTemplate;

    @Autowired
    private FailureInformationService failureInformationService;

    @Autowired
    private FailureInformationStationRepository failureInformationStationRepository;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private DistributionGroupMemberService distributionGroupMemberService;

    @Autowired
    private FailureInformationMapper failureInformationMapper;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private FailureInformationPublicationChannelRepository publicationChannelRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    private FailureInformationDistributionGroupRepository failureInformationDistributionGroupRepository;

    @Autowired
    private ResourceConfig resourceConfig;

    @Autowired
    private AuthNAuthService authNAuthService;

    @Autowired
    private GroupMemberPlzFilter groupMemberPlzFilter;

    @Autowired
    private SitCacheApi sitCacheApi;

    @Autowired
    private SettingsService settingsService;

    private static final String SUBJECT_DATE_PATTERN_APPLIED = "EEEE', den' dd.MM.yy 'um' HH:mm:ss";
    protected Map<String,String> contentReplaceMap;
    protected int countExportedMessages;

    @Value("${distribution-group-publisher.name}")
    private String publisherDistributionGroup;

    @Value("${spring.settings.isUseHtmlEmailBtnTemplate:true}")
    private boolean isUseHtmlEmailBtnTemplate;

    @Value("${export-to-dmz.source:}")
    private String source;

    @Value("${export-to-dmz.password:}")
    private String password;

    @Value("${smsUrl}")
    private String smsUrl;

    @Value("${smsApiKey}")
    private String smsAPIKey;

    @Autowired
    RestTemplate restTemplate;

    public boolean exportFailureInformation(UUID uuid, String[] targetChannels, GfiProcessState processState){
        log.info("Start exporting failureInformation with uuid: " + uuid);
        countExportedMessages = 0;
        for(String targetChannel: targetChannels){

            RabbitMqChannel rChannel = getAvailableRabbitMqChannel(targetChannel);
            if(getAvailableRabbitMqChannel(targetChannel) == null){
                log.warn(Constants.CHANNEL_NOT_EXISTING);
                continue;
            }

            rabbitMqConfig.checkExchangeAndQueueOnRabbitMq(rChannel.getExportQueue(), rChannel.getExportKey());

            //Holen der Störinfo
            TblFailureInformation existingTblFailureInfo = failureInformationRepository.findByUuid( uuid )
                    .orElseThrow(NotFoundException::new);

            //Wenn State mitgeliefert wird dann sende mail (zb. bei Update oder Complete State)
            if( !isChannelAlreadyPublished( targetChannel, existingTblFailureInfo) || processState != null) {

                //Nullsafe check
                if (Constants.CHANNEL_TPYE_LONG_MAIL.equals(rabbitMqConfig.getChannelNameToTypeMap().get(rChannel.getName())) ||
                        Constants.CHANNEL_TPYE_SHORT_MAIL.equals(rabbitMqConfig.getChannelNameToTypeMap().get(rChannel.getName()))) {
                    //Holen der Verteilergruppen (Pro Verteilergruppe eine Message auf RabbitMQ)
                    List<TblFailureInformationDistributionGroup> distributionGroups =
                            failureInformationDistributionGroupRepository.findByFailureInformationId(existingTblFailureInfo.getId());

                    if (distributionGroups == null || distributionGroups.isEmpty()) {
                        log.warn("no distribution group found for failureInfo: "
                                + existingTblFailureInfo.getUuid().toString());
                        return false;
                    }

                    distributionGroups.forEach( x -> prepareMessage(existingTblFailureInfo, x.getDistributionGroup(), rChannel, processState));

                }
                else {
                    //Veröffentlichung/Sende nur einmal (Bsp. Störungsauskunft.de)
                    RabbitMqMessageDto rabbitMqMessageDto = new RabbitMqMessageDto();
                    FailureInformationDto failureInformationDto = failureInformationService.enrichFailureInfo(failureInformationMapper.toFailureInformationDto(existingTblFailureInfo));
                    failureInformationDto.setPlanned(processHelper.isFailureInfoPlanned(failureInformationDto));
                    rabbitMqMessageDto.setFailureInformationDto(failureInformationDto);
                    sendMessageToRabbitMq(rabbitMqMessageDto, rChannel);
                }

                markChannelAsPublished(targetChannel, existingTblFailureInfo);

            }

        }

        log.info("Finished exporting failureInformation with uuid: " + uuid);
        return countExportedMessages > 0;
    }

    public boolean exportFailureInformationReminderMail(UUID uuid){

        List<String> channelsToPublishList = processHelper.getChannelsToPublishList(uuid, true);
        for (String targetChannel : channelsToPublishList) {
            RabbitMqChannel rChannel = getAvailableRabbitMqChannel(targetChannel);
            if(getAvailableRabbitMqChannel(targetChannel) == null){
                log.warn(Constants.CHANNEL_NOT_EXISTING);
                throw new NotFoundException(Constants.CHANNEL_NOT_EXISTING);
            }

            rabbitMqConfig.checkExchangeAndQueueOnRabbitMq(rChannel.getExportQueue(), rChannel.getExportKey());

            TblFailureInformation existingTblFailureInfo = failureInformationRepository.findByUuid( uuid )
                    .orElseThrow(NotFoundException::new);

            //Holen der Verteilergruppen (Pro Verteilergruppe eine Message auf RabbitMQ)
            Optional<TblDistributionGroup> distributionGroupOptional = distributionGroupRepository.findByName(publisherDistributionGroup);
            if (!distributionGroupOptional.isPresent()) {
                log.error("Distribution group 'Veröffentlicher' does not exist.");
                throw new NotFoundException("publisher.distribution.group.not.existing");
            }

            TblDistributionGroup distributionGroup = distributionGroupOptional.get();

            prepareMessage(existingTblFailureInfo, distributionGroup, rChannel, null);
        }

        return true;
    }

    public void exportFailureInformationsToDMZ(){

        log.trace("start exportFailureInformationsToDMZ");
        // Holen der in die DMZ zu exportierenden FailureInfos
        List<TblFailureInformation> tblFailureInfosVeroeffentlicht = failureInformationRepository
                .findByPublicationStatus(Constants.PUB_STATUS_VEROEFFENTLICHT);

        Set<UUID> currTableViewUuidSet = failureInformationService
                .findFailureInformationsForDisplay(Pageable.unpaged())
                .stream()
                .map(TblFailureInformation::getUuid)
                .collect(Collectors.toSet());

        List<FailureInformationDto> failureInfoDtossVeroeffentlicht = tblFailureInfosVeroeffentlicht.stream()
                .filter( x -> currTableViewUuidSet.contains(x.getUuid()))
                .filter( this::hasSITWebComponentChannel)
                .filter(this::filterByState)
               // .filter( x -> x.getFailureEndPlanned() == null || x.getFailureEndPlanned().compareTo(new Date()) > 0)
                .map( failureInformationMapper::toFailureInformationDto )
                .map( failureInformationService::enrichFailureInfo )
                .collect(Collectors.toList());

        try {

            ExportGridFailures export = new ExportGridFailures();
            export.setFailures(failureInfoDtossVeroeffentlicht);
            export.setSource(source);
            export.setPassword(password);
            log.trace("start postPublicFailureInfos");
            sitCacheApi.postPublicFailureInfos(export);
            log.trace("finish postPublicFailureInfos");
        } catch (Exception e) {
            log.error("error.exporting.published.failure.infos", e);
        }
        log.trace("finish exportFailureInformationsToDMZ");
    }

    private boolean filterByState(TblFailureInformation x) {

        return !x.getRefStatusIntern().getStatus().equals("storniert") && !x.getRefStatusIntern().getStatus().equals("abgeschlossen");
    }

    private boolean hasSITWebComponentChannel(TblFailureInformation x) {
       return publicationChannelRepository.findByTblFailureInformation(x).stream()
                .map(TblFailureInformationPublicationChannel::getPublicationChannel)
                .anyMatch(publicationChannelName -> Constants.CHANNEL_TPYE_PUBLICATION_OWNDMZ.equals(rabbitMqConfig.getChannelNameToTypeMap().get(publicationChannelName)));
    }

    public void exportFeSettingsToDMZ(){
       try {
           ExportSettings exportSettings = new ExportSettings();
           exportSettings.setPassword(password);
           exportSettings.setSettings(settingsService.getFESettings());
           sitCacheApi.postFeSettings(exportSettings);
       } catch (Exception e) {
           log.error("error.exporting.feSettings.to.DMZ", e);
       }
   }

    private boolean isChannelAlreadyPublished(String targetChannel, TblFailureInformation existingTblFailureInfo) {

        List<TblFailureInformationPublicationChannel> publicationChannelList = publicationChannelRepository.findByTblFailureInformation(existingTblFailureInfo);

        if( publicationChannelList == null ) {
            return true;
        }
        Optional<TblFailureInformationPublicationChannel> tf =
                publicationChannelList.stream()
                        .filter( x -> x.getPublicationChannel().equals(targetChannel) )
                        .findFirst();

        if( tf.isPresent() ) {
            boolean published = tf.get().isPublished();
            if (published) {
                log.debug("Channel '"+targetChannel+"' has already been published for FailureInfo '"+
                        existingTblFailureInfo.getUuid()+"' and has been skipped.");
            }
            return published;
        } else {
            // noch nicht veröffentlicht da noch nicht in Tabelle vorhanden
            return false;
        }
    }

    private void markChannelAsPublished(String targetChannel, TblFailureInformation existingTblFailureInfo) {

        List<TblFailureInformationPublicationChannel> publicationChannelList = publicationChannelRepository.findByTblFailureInformation(existingTblFailureInfo);

        Optional<TblFailureInformationPublicationChannel> pubChannelToSave = publicationChannelList.stream()
                .filter( x->x.getPublicationChannel().equals(targetChannel))
                .findFirst();

        if( pubChannelToSave.isPresent() ) {
            pubChannelToSave.get().setPublished(true);
            publicationChannelRepository.save(pubChannelToSave.get());
        } else {
            saveNewPubChannel(targetChannel, existingTblFailureInfo);
        }
    }

    private void saveNewPubChannel(String targetChannel, TblFailureInformation existingTblFailureInfo) {
        TblFailureInformationPublicationChannel newPubChannelToSave = new TblFailureInformationPublicationChannel();
        newPubChannelToSave.setPublished(true);
        newPubChannelToSave.setPublicationChannel(targetChannel);
        newPubChannelToSave.setTblFailureInformation(existingTblFailureInfo);
        publicationChannelRepository.save(newPubChannelToSave);
    }

    protected RabbitMqChannel getAvailableRabbitMqChannel(String targetChannel){

        List<RabbitMqChannel> rabbitMqChannels = rabbitMqProperties.getChannels();
        List<RabbitMqChannel> filteredChannelList= rabbitMqChannels.stream().filter(channel -> channel.getName().contains(targetChannel)).collect(Collectors.toList());

        if(!filteredChannelList.isEmpty()){
            return filteredChannelList.get(0);
        }
        else{
            return null;
        }


    }

    private void prepareMessage(TblFailureInformation existingTblFailureInfo, TblDistributionGroup distributionGroup, RabbitMqChannel rabbitMqChannel, GfiProcessState processState){

        List<String> mailAddresses= new ArrayList<>();
        List<String> mobilAddresses= new ArrayList<>();

        RabbitMqMessageDto rabbitMqMessageDto = new RabbitMqMessageDto();
        rabbitMqMessageDto.setFailureInformationDto(failureInformationMapper.toFailureInformationDto(existingTblFailureInfo));

        //Eintrag Verteilergruppe ins MessageDto
        rabbitMqMessageDto.setDistributionGroup(distributionGroup.getName());

        //  der dem Verteiler zugeordneten Personen
        List<DistributionGroupMemberDto> memberList =  groupMemberPlzFilter.filter(
                distributionGroupMemberService.getMembersByGroupId(distributionGroup.getUuid()),
                existingTblFailureInfo);

        //Holen der Mailadressen der dem Verteiler zugeordneten Personen und Eintrag ins MessageDto
        for (DistributionGroupMemberDto member : memberList) {

            if (member.getEmail() != null) {
                mailAddresses.add(member.getEmail());
            }
            if (member.getMobileNumber() != null && !member.getMobileNumber().isEmpty()) {
                mobilAddresses.add(member.getMobileNumber());
            }
        }

        rabbitMqMessageDto.setMobileNumbers(mobilAddresses);
        rabbitMqMessageDto.setMailAddresses(mailAddresses);
        setMailTextAndSubject(existingTblFailureInfo, distributionGroup, processState, rabbitMqMessageDto, rabbitMqChannel);

        if (Constants.CHANNEL_TPYE_SHORT_MAIL.equals(rabbitMqConfig.getChannelNameToTypeMap().get(rabbitMqChannel.getName()))) {
            if (!mobilAddresses.isEmpty()) {
                sendSMS(rabbitMqMessageDto);
            }
            return;
        }

        if (mailAddresses.isEmpty()) {
            log.warn("no mail addresses found for distribution group: "
                    + distributionGroup.getUuid().toString());
            return;
        }
        sendMessageToRabbitMq(rabbitMqMessageDto, rabbitMqChannel);
    }

    private void sendSMS(RabbitMqMessageDto rabbitMqMessageDto) {

        try {
            SMSDto sms = new SMSDto(smsAPIKey, rabbitMqMessageDto.getMobileNumbers(), rabbitMqMessageDto.getBody());

            String result = restTemplate.postForObject(smsUrl + "/sendSMS", sms, String.class);

            log.trace(result);
        } catch (Exception e) {
            log.error("Error while sending SMS", e);
        }
    }

    private void setMailTextAndSubject(TblFailureInformation existingTblFailureInfo,
                                       TblDistributionGroup distributionGroup, GfiProcessState processState,
                                       RabbitMqMessageDto rabbitMqMessageDto, RabbitMqChannel rabbitMqChannel) {
        //Holen des zum Verteiler zugehörigen Textes
        //Ersatz der gekennzeichneten Stellen im Text mit vorhandenen Infos und Eintrag ins MessageDto
        //Mailtype long (default)
        String distributionText = distributionGroup.getDistributionTextPublish();
        String emailSubject = distributionGroup.getEmailSubjectPublish();

        if (Constants.CHANNEL_TPYE_SHORT_MAIL.equals(rabbitMqConfig.getChannelNameToTypeMap().get(rabbitMqChannel.getName()))) {
            distributionText = distributionGroup.getDistributionTextPublishShort();
            emailSubject = distributionGroup.getEmailSubjectPublishShort();
        }

        if (processState == GfiProcessState.CANCELED) {
            if (existingTblFailureInfo.getRefFailureClassification() != null && existingTblFailureInfo.getRefFailureClassification().getClassification().equals("Störung AVG")) {
                distributionText = "Die $Sparte$ von $Störungsbeginn_gemeldet$ wurde storniert.";
            } else {
                distributionText = "Die Störungsmeldung im $Sparte$-Netz von $Störungsbeginn_gemeldet$ wurde storniert.";
            }
            emailSubject = "Störung wurde storniert";
        } else if (processState == GfiProcessState.COMPLETED) {
            distributionText = distributionGroup.getDistributionTextComplete();
            emailSubject = distributionGroup.getEmailSubjectComplete();
        } else if (processState == GfiProcessState.UPDATED) {
            distributionText = distributionGroup.getDistributionTextUpdate();
            emailSubject = distributionGroup.getEmailSubjectUpdate();
        }

        String finalDistributionText = replacePlaceholders(distributionText, existingTblFailureInfo);
        String finalEmailSubject = replacePlaceholders(emailSubject, existingTblFailureInfo);
        rabbitMqMessageDto.setBody(finalDistributionText);
        rabbitMqMessageDto.setEmailSubject(finalEmailSubject);
    }

    private void sendMessageToRabbitMq(RabbitMqMessageDto rabbitMqMessageDto, RabbitMqChannel rabbitMqChannel){
        try{
            rabbitTemplate.convertAndSend(rabbitMqProperties.getExportExchange(), rabbitMqChannel.getExportKey(), objectMapper.writeValueAsString(rabbitMqMessageDto));
            countExportedMessages++;
            log.info("RabbitMq: Successfully sent msg: Export-Exchange: " +  rabbitMqProperties.getExportExchange() + " Export-Queue: " + rabbitMqChannel.getExportQueue()
                    + " Export-Key: " + rabbitMqChannel.getExportKey());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.rabbitMqMessage");
        }
    }

    protected String replacePlaceholders(String result, TblFailureInformation tblFailureInfo) {
        if (result == null) return "";

        createContentReplaceHashMap(tblFailureInfo);

        for (Map.Entry<String, String> stringStringEntry : contentReplaceMap.entrySet()) {
            if(stringStringEntry.getKey() != null && stringStringEntry.getValue() != null) {
                result = result.replace(stringStringEntry.getKey(), stringStringEntry.getValue());
            }
        }

        return result;
    }

    protected void createContentReplaceHashMap(TblFailureInformation tblFailureInfo){ // NOSONAR: _fd 17.07.2020 no Cognitive complexity warning because this method is very easy

        DistributionTextPlaceholderDto placeholderDto = resourceConfig.placeholderLoader();

        this.contentReplaceMap = new HashMap<>();
        initContentReplaceMap(placeholderDto);

        RefFailureClassification failureClassification = tblFailureInfo.getRefFailureClassification();

        if (failureClassification != null) {
            contentReplaceMap.put(placeholderDto.getFailureClassification(), failureClassification.getClassification());
        }

        String internalRemark = tblFailureInfo.getInternalRemark();
        if (internalRemark != null) {
            contentReplaceMap.put(placeholderDto.getInternalRemark(), internalRemark);
        }

        String responsibility = tblFailureInfo.getResponsibility();
        if (responsibility != null) {
            contentReplaceMap.put(placeholderDto.getResponsibility(), responsibility);
        }

        RefStatus statusIntern = tblFailureInfo.getRefStatusIntern();
        if (statusIntern != null) {
            contentReplaceMap.put(placeholderDto.getStatusIntern(), statusIntern.getStatus());
        }

        String statusExtern = getStatusExtern(tblFailureInfo);
        if (statusExtern != null) {
            contentReplaceMap.put(placeholderDto.getStatusExtern(), statusExtern);
        }

        String publicationStatus = tblFailureInfo.getPublicationStatus();
        if (publicationStatus != null) {
            contentReplaceMap.put(placeholderDto.getPublicationStatus(), publicationStatus);
        }

        String branch = tblFailureInfo.getRefBranch().getDescription();
        if (branch != null) {
            contentReplaceMap.put(placeholderDto.getBranch(), branch);
        }

        String voltageLevel = tblFailureInfo.getVoltageLevel();
        if (voltageLevel != null) {
            contentReplaceMap.put(placeholderDto.getVoltageLevel(), voltageLevel);
        }

        String pressureLevel = tblFailureInfo.getPressureLevel();
        if (pressureLevel != null) {
            contentReplaceMap.put(placeholderDto.getPressureLevel(), pressureLevel);
        }

        Date failureBegin = tblFailureInfo.getFailureBegin();
        if(failureBegin != null) {
            DateFormat dfmt = new SimpleDateFormat(SUBJECT_DATE_PATTERN_APPLIED);
            String formattedDate = dfmt.format(failureBegin);
            contentReplaceMap.put(placeholderDto.getFailureBegin(), formattedDate);
        }

        Date failureEndPlanned = tblFailureInfo.getFailureEndPlanned();
        if(failureEndPlanned != null) {
            DateFormat dfmt = new SimpleDateFormat(SUBJECT_DATE_PATTERN_APPLIED);
            String formattedDate = dfmt.format(failureEndPlanned);
            contentReplaceMap.put(placeholderDto.getFailureEndPlanned(), formattedDate);
        }

        Date failureEndResupplied = tblFailureInfo.getFailureEndResupplied();
        if(failureEndResupplied != null) {
            DateFormat dfmt = new SimpleDateFormat(SUBJECT_DATE_PATTERN_APPLIED);
            String formattedDate = dfmt.format(failureEndResupplied);
            contentReplaceMap.put(placeholderDto.getFailureEndResupplied(), formattedDate);
        }

        RefExpectedReason expectedReason = tblFailureInfo.getRefExpectedReason();
        if (expectedReason != null) {
            contentReplaceMap.put(placeholderDto.getExpectedReason(), expectedReason.getText());
        }

        String description = tblFailureInfo.getDescription();
        if (description != null) {
            contentReplaceMap.put(placeholderDto.getDescription(), description);
        }

        String postcode = tblFailureInfo.getPostcode();
        if (postcode != null) {
            contentReplaceMap.put(placeholderDto.getPostcode(), postcode);
        }

        String city = tblFailureInfo.getCity();
        if (city != null) {
            contentReplaceMap.put(placeholderDto.getCity(), city);
        }

        String district = tblFailureInfo.getDistrict();
        if (district != null) {
            contentReplaceMap.put(placeholderDto.getDistrict(), district);
        }

        String street = tblFailureInfo.getStreet();
        if (street != null) {
            contentReplaceMap.put(placeholderDto.getStreet(), street);
        }

        String housenumber = tblFailureInfo.getHousenumber();
        if (housenumber != null) {
            contentReplaceMap.put(placeholderDto.getHousenumber(), housenumber);
        }

        RefRadius radius = tblFailureInfo.getRefRadius();
        if (radius != null) {
            contentReplaceMap.put(placeholderDto.getRadius(), radius.getRadius().toString() + "m");
        }

        String stationDescription = tblFailureInfo.getStationDescription();
        if (stationDescription != null) {
            contentReplaceMap.put(placeholderDto.getStationDescription(), stationDescription);
            contentReplaceMap.put(placeholderDto.getStationShortDescription(), stationDescription);
        }

        contentReplaceMap.put(placeholderDto.getStationShortDescription(), "");
        List<TblFailinfoStation> stations = failureInformationStationRepository.findByFailureInformationId(tblFailureInfo.getId());
        if (stations != null && !stations.isEmpty()) {

            StringBuilder b = new StringBuilder();

            for (TblFailinfoStation station : stations) {

                String stationName = stationRepository.findByStationId(station.getStationStationId()).orElse(new TblStation()).getStationName();

                if (b.length() != 0) {
                    b.append(", ");
                }
                else {
                    contentReplaceMap.put(placeholderDto.getStationShortDescription(), stationName + ", ...");
                }
                b.append(stationName);
            }

            contentReplaceMap.put(placeholderDto.getStationDescription(), b.toString());
            contentReplaceMap.put(placeholderDto.getRadius(), "");
        }

        createDirectFailureLink(tblFailureInfo, placeholderDto);

    }

    private void createDirectFailureLink(TblFailureInformation tblFailureInfo,
                                         DistributionTextPlaceholderDto placeholderDto) {
        String directFailureLink = authNAuthService.getDirectMeasureLink(tblFailureInfo.getUuid().toString());
        if (isUseHtmlEmailBtnTemplate) {
            String htmlEmailBtnTemplate = resourceConfig.getHtmlEmailBtnTemplate();
            directFailureLink = htmlEmailBtnTemplate.replace(placeholderDto.getDirectFailureLink(), directFailureLink);
        }
        contentReplaceMap.put(placeholderDto.getDirectFailureLink(), directFailureLink);
    }

    private String getStatusExtern(TblFailureInformation tblFailureInfo) {
        FailureInformationDto failureInformationDto = failureInformationMapper.toFailureInformationDto(tblFailureInfo);
        return ExternalStatusCalculator.addExternalStatus(statusRepository, failureInformationDto);
    }

    private void initContentReplaceMap(DistributionTextPlaceholderDto placeholderDto) {
        contentReplaceMap.put(placeholderDto.getFailureClassification(), "");
        contentReplaceMap.put(placeholderDto.getInternalRemark(), "");
        contentReplaceMap.put(placeholderDto.getResponsibility(), "");
        contentReplaceMap.put(placeholderDto.getStatusIntern(), "");
        contentReplaceMap.put(placeholderDto.getStatusExtern(), "");
        contentReplaceMap.put(placeholderDto.getPublicationStatus(), "");
        contentReplaceMap.put(placeholderDto.getBranch(), "");
        contentReplaceMap.put(placeholderDto.getVoltageLevel(), "");
        contentReplaceMap.put(placeholderDto.getPressureLevel(), "");
        contentReplaceMap.put(placeholderDto.getFailureBegin(), "");
        contentReplaceMap.put(placeholderDto.getFailureEndPlanned(), "");
        contentReplaceMap.put(placeholderDto.getFailureEndResupplied(), "");
        contentReplaceMap.put(placeholderDto.getExpectedReason(), "");
        contentReplaceMap.put(placeholderDto.getDescription(), "");
        contentReplaceMap.put(placeholderDto.getPostcode(), "");
        contentReplaceMap.put(placeholderDto.getCity(), "");
        contentReplaceMap.put(placeholderDto.getDistrict(), "");
        contentReplaceMap.put(placeholderDto.getStreet(), "");
        contentReplaceMap.put(placeholderDto.getHousenumber(), "");
        contentReplaceMap.put(placeholderDto.getStationDescription(), "");
        contentReplaceMap.put(placeholderDto.getRadius(), "");
        contentReplaceMap.put(placeholderDto.getDirectFailureLink(), "");
    }

}
