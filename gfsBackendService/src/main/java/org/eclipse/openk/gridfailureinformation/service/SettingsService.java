/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.config.FESettings;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqChannel;
import org.eclipse.openk.gridfailureinformation.mapper.tools.VisibilityConfigurationMapper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FEInitialContentDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@Log4j2
public class SettingsService {

    @Autowired
    private FESettings feSettings;

    @Autowired
    private VisibilityConfigurationMapper visConfigMapper;

    private FESettingsDto feSettingsDto;
    private FEInitialContentDto feInitialContentDto;

    public FESettingsDto getFESettings(){

        if (feSettingsDto != null) return feSettingsDto;

        feSettingsDto = new FESettingsDto();
        List<RabbitMqChannel> rabbitMqChannels = feSettings.getChannels();

        List<String> channelNames = new LinkedList<>();
        for(RabbitMqChannel channel: rabbitMqChannels){
            channelNames.add(channel.getName());
        }

        feSettingsDto.setExportChannels(channelNames);
        feSettingsDto.setDetailMapInitialZoom(feSettings.getDetailMapInitialZoom());
        feSettingsDto.setOverviewMapInitialZoom(feSettings.getOverviewMapInitialZoom());
        feSettingsDto.setOverviewMapInitialLatitude(feSettings.getOverviewMapInitialLatitude());
        feSettingsDto.setOverviewMapInitialLongitude(feSettings.getOverviewMapInitialLongitude());

        feSettingsDto.setMapTileAttribution(feSettings.getMapTileAttribution());
        feSettingsDto.setMapTileLayerUrl(feSettings.getMapTileLayerUrl());
        feSettingsDto.setMapWmsLayer(feSettings.getMapWmsLayer());
        feSettingsDto.setMapWmsLayer(feSettings.getMapWmsLayer());
        feSettingsDto.setIconUrl(feSettings.getIconUrl());

        feSettingsDto.setDataExternInitialVisibility(feSettings.getDataExternInitialVisibility());

        FESettingsDto.VisibilityConfiguration visibilityConfiguration = new FESettingsDto.VisibilityConfiguration();
        feSettingsDto.setVisibilityConfiguration(visibilityConfiguration);
        visibilityConfiguration.setFieldVisibility(
                visConfigMapper.fromRawToDto(feSettings.getVisibilityConfigurationRaw().getFieldVisibilityRaw()));
        visibilityConfiguration.setTableInternColumnVisibility(
                visConfigMapper.fromRawToDto(feSettings.getVisibilityConfigurationRaw().getTableInternColumnVisibility()));
        visibilityConfiguration.setTableExternColumnVisibility(
                visConfigMapper.fromRawToDto(feSettings.getVisibilityConfigurationRaw().getTableExternColumnVisibility()));
        visibilityConfiguration.setMapExternTooltipVisibility(
                visConfigMapper.fromRawToDto(feSettings.getVisibilityConfigurationRaw().getMapExternTooltipVisibility()));

        feSettingsDto.setNominatimURL(feSettings.getNominatimURL());
        feSettingsDto.setPlzLookupEnabled(feSettings.isPlzLookupEnabled());

        return feSettingsDto;
    }

    public FEInitialContentDto getInitialEmailSubjectAndContent(){
        if (feInitialContentDto != null) return feInitialContentDto;

        feInitialContentDto = new FEInitialContentDto();
        feInitialContentDto.setEmailSubjectPublishInit(feSettings.getEmailSubjectPublishInit());
        feInitialContentDto.setEmailContentPublishInit(feSettings.getEmailContentPublishInit());

        feInitialContentDto.setEmailSubjectUpdateInit(feSettings.getEmailSubjectUpdateInit());
        feInitialContentDto.setEmailContentUpdateInit(feSettings.getEmailContentUpdateInit());

        feInitialContentDto.setEmailContentCompleteInit(feSettings.getEmailContentCompleteInit());
        feInitialContentDto.setEmailSubjectCompleteInit(feSettings.getEmailSubjectCompleteInit());

        feInitialContentDto.setEmailSubjectPublishShortInit(feSettings.getEmailSubjectPublishShortInit());
        feInitialContentDto.setEmailContentPublishShortInit(feSettings.getEmailContentPublishShortInit());

        feInitialContentDto.setEmailSubjectUpdateShortInit(feSettings.getEmailSubjectUpdateShortInit());
        feInitialContentDto.setEmailContentUpdateShortInit(feSettings.getEmailContentUpdateShortInit());

        feInitialContentDto.setEmailContentCompleteShortInit(feSettings.getEmailContentCompleteShortInit());
        feInitialContentDto.setEmailSubjectCompleteShortInit(feSettings.getEmailSubjectCompleteShortInit());

        return feInitialContentDto;
    }

}
