/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import lombok.Data;

import java.io.Serializable;

@Data
public class DistributionTextPlaceholderDto implements Serializable {

    private String failureClassification;
    private String failureType;
    private String responsibility;
    private String internExtern;
    private String statusIntern;
    private String statusExtern;
    private String publicationStatus;
    private String branch;
    private String voltageLevel;
    private String pressureLevel;
    private String failureBegin;
    private String failureEndPlanned;
    private String failureEndResupplied;
    private String expectedReason;
    private String description;
    private String internalRemark;

    private String postcode;
    private String city;
    private String district;
    private String street;
    private String housenumber;

    private String stationDescription;
    private String stationShortDescription;
    private String radius;

    private String directFailureLink;

}
