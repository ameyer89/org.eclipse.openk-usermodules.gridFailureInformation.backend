/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.email;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Log4j2
@Configuration
@Data
public class EmailConfig {

    @Value("${email.sender}")
    private String sender;

    @Value("${email.smtpHost}")
    private String smtpHost;

    @Value("${email.port:1025}")
    private String emailPort;

    @Value("${email.isHtmlEmail:true}")
    private boolean isHtmlEmail;

    @Value("${email.isUseHtmlEmailTemplate:true}")
    private boolean isUseHtmlEmailTemplate;

    private static final String EMAIL_TEMPLATE_PATH = "email/emailTemplate.html";

    private String htmlEmailTemplate;

    @PostConstruct
    private void init(){
        htmlEmailTemplate = new ResourceLoaderBase().loadStringFromResource(EMAIL_TEMPLATE_PATH);
    }

}
