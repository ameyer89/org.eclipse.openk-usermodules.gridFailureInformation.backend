/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;

@Log4j2
public abstract class UserInteractionTask<T extends ProcessSubject> extends BaseTask<T> {
    protected UserInteractionTask(String description) {
        super(description);
    }

    protected boolean isStayInThisTask(T model){ // NOSONAR Parameter needed, if overridden
        return false;
    }

    protected void onStayInTask(T model) throws ProcessException {
    }

    @Override
    protected void onRecover(T model) throws ProcessException {
        leaveStep( model );
    }

    @Override
    public void leaveStep(ProcessSubject model) throws ProcessException {
        if( !isStayInThisTask((T)model)) {
            super.leaveStep(model);
        }
        else {
            log.debug("Stay In this task valid! -> staying!");
            onStayInTask((T)model);
        }
    }
}
