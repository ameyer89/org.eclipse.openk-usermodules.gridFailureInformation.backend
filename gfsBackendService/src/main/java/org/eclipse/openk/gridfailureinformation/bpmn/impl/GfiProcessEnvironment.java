/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import lombok.Data;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationPublicationChannelRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Data
public class GfiProcessEnvironment {
    @Autowired
    private FailureClassificationRepository failureClassificationRepository;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private FailureInformationService failureInformationService;

    @Autowired
    private FailureInformationMapper failureInformationMapper;

    @Autowired
    private FailureInformationPublicationChannelRepository channelRepository;

    @Lazy
    @Autowired
    private ExportService exportService;

}
