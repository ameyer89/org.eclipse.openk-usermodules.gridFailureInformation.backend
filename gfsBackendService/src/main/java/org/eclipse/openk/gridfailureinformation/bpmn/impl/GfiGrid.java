/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessGrid;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessTask;
import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoCanceled;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoHasSubordinatedInfos;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoPlanned;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoPublished;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.DecideFailureInfoUpdated;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.PublishFailureToChannelsTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.RePublishFailureToMailChannelsTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ResetPublishedFlagOnChannelsTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.SetEndDateTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ShortcutTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.StoreEditStatusOfParentToChildrenTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.StorePublishStatusTask;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.UIStoreFailureInformationTask;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.springframework.stereotype.Component;

import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.NO;
import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.PORT1;
import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.PORT2;
import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.PORT3;
import static org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask.OutputPort.YES;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.CANCELED;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.CHECKED_FOR_PUBLISH_;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.COMPLETED;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.CREATED;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.NEW;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.PLANNED;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.QUALIFIED;
import static org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState.UPDATED;

@Log4j2
@Component
public class GfiGrid extends ProcessGrid {

    public GfiGrid() throws ProcessException {
        DecisionTask<GfiProcessSubject> decidePlanned = new DecideFailureInfoPlanned();

        ProcessTask storeEditStatusPlanned = new ShortcutTask( PLANNED );

        ProcessTask enterMessage = register( PLANNED,
                new UIStoreFailureInformationTask("State PLANNED UI Task", true));
        register( NEW, enterMessage );

        ProcessTask storeEditStatusCreated = new ShortcutTask( CREATED );
        ProcessTask qualifyMessage = register( CREATED,
                new UIStoreFailureInformationTask( "State CREATED UI Task", true));
        register( UPDATED, qualifyMessage);

        DecisionTask<GfiProcessSubject> decideCanceled = new DecideFailureInfoCanceled();
        ProcessTask storeEditStatusQualified = new ShortcutTask( QUALIFIED );
        ProcessTask storeEditStatusCanceled = new ShortcutTask( CANCELED );

        ProcessTask canceledEndpoint = register( CANCELED,
                new UIStoreFailureInformationTask( "State CANCELED UI Task", true));

        DecisionTask<GfiProcessSubject> decidePublished = new DecideFailureInfoPublished();
        ProcessTask storePubStatusWithdrawnOnCancel = new StorePublishStatusTask(Constants.PUB_STATUS_ZURUECKGEZOGEN);
        ProcessTask publishMessage = register( QUALIFIED,
                new UIStoreFailureInformationTask( "State QUALIFIED UI Task", true));
        ProcessTask rePublishToMailChannelsQualifiedAfterUpdated = new RePublishFailureToMailChannelsTask(UPDATED);

        ProcessTask publishMessageToChannels = register( CHECKED_FOR_PUBLISH_, new PublishFailureToChannelsTask());
        ProcessTask storePublicationStatusPublished = new StorePublishStatusTask(Constants.PUB_STATUS_VEROEFFENTLICHT);
        ProcessTask storePublicationStatusWithdrawn = new StorePublishStatusTask(Constants.PUB_STATUS_ZURUECKGEZOGEN);
        DecisionTask<GfiProcessSubject> decidePublishedOnComplete = new DecideFailureInfoPublished();
        ProcessTask updateMessage = new UIStoreFailureInformationTask( "State Updated UI Task", true);

        DecisionTask<GfiProcessSubject> decideUpdated = new DecideFailureInfoUpdated();
        ProcessTask storeEditStatusUpdated = new ShortcutTask( UPDATED );
        ProcessTask resetPublishedFlagOnChannels = new ResetPublishedFlagOnChannelsTask();

        DecisionTask<GfiProcessSubject> decideHasSubordinatedInfosCompleted = new DecideFailureInfoHasSubordinatedInfos();
        ProcessTask storeEditStatusOfParentForChildrenCompleted = new StoreEditStatusOfParentToChildrenTask();

        DecisionTask<GfiProcessSubject> decideHasSubordinatedInfosCancelled = new DecideFailureInfoHasSubordinatedInfos();
        ProcessTask storeEditStatusOfParentForChildrenCancelled = new StoreEditStatusOfParentToChildrenTask();

        ProcessTask completedEndpoint = register( COMPLETED,
                new UIStoreFailureInformationTask( "State COMPETED UI Task", true));
        ProcessTask rePublishToMailChannelsCompleted = new RePublishFailureToMailChannelsTask(COMPLETED);

        ProcessTask setEndDate = new SetEndDateTask();

        //Connect

        //Geplante Meldung?
        decidePlanned.connectOutputTo( YES, storeEditStatusPlanned );
        decidePlanned.connectOutputTo( NO, storeEditStatusCreated );
        // -> ja
        storeEditStatusPlanned.connectOutputTo(enterMessage);
        enterMessage.connectOutputTo( storeEditStatusCreated );
        // -> nein
        storeEditStatusCreated.connectOutputTo( qualifyMessage );
        qualifyMessage.connectOutputTo(decideCanceled);
        //Meldung storniert?
        decideCanceled.connectOutputTo( YES, storeEditStatusCanceled ); //Status nicht speichern da sonst doppelt, -> nächster Prozessschritt
        decideCanceled.connectOutputTo( NO, storeEditStatusQualified ); //Status nicht speichern da sonst doppelt, -> nächster Prozessschritt
        decideCanceled.connectOutputTo( PORT3, setEndDate );

        //Benachrichtigungsemail bei Qualified nachdem aktualisiert wurde und nur wenn schon veröffentlicht
        storeEditStatusQualified.connectOutputTo(rePublishToMailChannelsQualifiedAfterUpdated);
        rePublishToMailChannelsQualifiedAfterUpdated.connectOutputTo(publishMessage);
        publishMessage.connectOutputTo(decideUpdated);
        decideUpdated.connectOutputTo(PORT1, storeEditStatusUpdated);
        decideUpdated.connectOutputTo(PORT2, setEndDate);
        decideUpdated.connectOutputTo(PORT3, storeEditStatusCanceled);
        setEndDate.connectOutputTo(rePublishToMailChannelsCompleted);

        //Benachrichtigungsemail bei Complete wenn schon einmal veröffentlicht
        rePublishToMailChannelsCompleted.connectOutputTo(decidePublishedOnComplete);
        decidePublishedOnComplete.connectOutputTo(YES, storePublicationStatusWithdrawn);

        decidePublishedOnComplete.connectOutputTo(NO, decideHasSubordinatedInfosCompleted);
        storePublicationStatusWithdrawn.connectOutputTo(decideHasSubordinatedInfosCompleted);
        decideHasSubordinatedInfosCompleted.connectOutputTo(NO, completedEndpoint);
        decideHasSubordinatedInfosCompleted.connectOutputTo(YES, storeEditStatusOfParentForChildrenCompleted);
        storeEditStatusOfParentForChildrenCompleted.connectOutputTo(completedEndpoint);

        storeEditStatusCanceled.connectOutputTo(decidePublished);
        decidePublished.connectOutputTo( YES, storePubStatusWithdrawnOnCancel);
        decidePublished.connectOutputTo( NO, decideHasSubordinatedInfosCancelled );
        storePubStatusWithdrawnOnCancel.connectOutputTo( decideHasSubordinatedInfosCancelled );
        decideHasSubordinatedInfosCancelled.connectOutputTo(YES, storeEditStatusOfParentForChildrenCancelled );
        decideHasSubordinatedInfosCancelled.connectOutputTo(NO, canceledEndpoint );
        storeEditStatusOfParentForChildrenCancelled.connectOutputTo( canceledEndpoint );

        publishMessageToChannels.connectOutputTo(storePublicationStatusPublished);
        storePublicationStatusPublished.connectOutputTo(updateMessage);

        storeEditStatusUpdated.connectOutputTo(resetPublishedFlagOnChannels);
        resetPublishedFlagOnChannels.connectOutputTo(qualifyMessage);

    }

}
