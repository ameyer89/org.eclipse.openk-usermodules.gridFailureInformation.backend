/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.email.EmailService;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.ImportGeoJsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import java.util.UUID;


@Log4j2
@RestController
@RequestMapping("/export")
public class ExportController {

    @Autowired
    private ExportService exportService;

    @Autowired
    ImportGeoJsonService importGeoJsonService;

    @Autowired
    private EmailService emailService;


    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @PutMapping("/{failureInfoUuid}")
    @ApiOperation(value = "Export einer Störungsinformation über die übergebenen Kanäle")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity exportFailureInformation (@PathVariable UUID failureInfoUuid, @RequestBody String[] channels) {

        exportService.exportFailureInformation(failureInfoUuid, channels, null);

        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @GetMapping("/exporttodmz")
    @ApiOperation(value = "Export der veröffentlichten Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity exportFailureInformationsToDMZ () {

        exportService.exportFeSettingsToDMZ();
        exportService.exportFailureInformationsToDMZ();
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @GetMapping("/importgeojson")
    @ApiOperation(value = "Import der GeoJsons für die Stationen")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "GeoJsons erfolgreich importiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity importGeoJsonTask () {
        importGeoJsonService.importGeoJsonFile();
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @ApiOperation(value = "Versende Testmail")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Erfolgreich durchgeführt")})
    @GetMapping("/testmail")
    public ResponseEntity testMail() throws MessagingException {
        emailService.sendTestMail("test@test33.de");
        return ResponseEntity.ok().build();
    }


}