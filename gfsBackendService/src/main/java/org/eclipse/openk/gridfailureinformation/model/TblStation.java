/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
public class TblStation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_station_id_seq")
    @SequenceGenerator(name = "tbl_station_id_seq", sequenceName = "tbl_station_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;
    private BigDecimal sdox1;
    private BigDecimal sdoy1;
    private Long g3efid;
    private String stationId;
    private String stationName;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String geoJson;

}
