/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.service.ExportService;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationLastModDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationPublicationChannelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/grid-failure-informations")
public class FailureInformationController {

    @Value("${gridFailureInformation.maxListSize}")
    int maxListSize;

    @Autowired
    private ExportService exportService;

    @Autowired
    private FailureInformationService failureInformationService;

    @Secured({"ROLE_GRID-FAILURE-ADMIN","ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @GetMapping("/{uuid}")
    @ApiOperation(value = "Anzeigen einer Störungsinformation")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformation wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public FailureInformationDto getFailureInformation(@PathVariable UUID uuid) {
        return failureInformationService.findFailureInformation(uuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN","ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @GetMapping("/last-modification")
    @ApiOperation(value = "Anzeigen des Zeitpunkts der letzten Änderung am sichtbaren Datenbestand")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Es sind keine Störungsinformationen vorhanden")})
    @ResponseStatus(HttpStatus.OK)
    public FailureInformationLastModDto getFailureInformationLastModTimeStamp() {
        return failureInformationService.getFailureInformationLastModTimeStamp();
    }


    @Secured({"ROLE_GRID-FAILURE-ADMIN","ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeigen aller Störungsinformationen")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<FailureInformationDto> findFailureInfos() {
        return failureInformationService.findFailureInformations(PageRequest.of(0, maxListSize)).getContent();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    @PostMapping
    @ApiOperation(value = "Anlegen einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<FailureInformationDto> insertFailureInfo(
            @Validated @RequestBody FailureInformationDto gfDto) {
        FailureInformationDto savedgfDto = failureInformationService.insertFailureInfo(gfDto, GfiProcessState.NEW);
        failureInformationService.setDefaultChannels(savedgfDto);

        exportService.exportFailureInformationsToDMZ();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedgfDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedgfDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @PutMapping("/{failureInfoUuid}")
    @ApiOperation(value = "Ändern einer Störungsinformation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Störungsinformation wurde aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateFailureInfo(
            @PathVariable UUID failureInfoUuid,
            @RequestParam( defaultValue = "false" ) @ApiParam( "Save and publish the failure to the distribution channels") boolean saveForPublish,
            @Validated @RequestBody FailureInformationDto failureInfoDto) {

        if (!failureInfoDto.getUuid().equals(failureInfoUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        if( saveForPublish ) {
            failureInformationService.updateAndPublish(failureInfoDto);
        }
        else {
            failureInformationService.updateFailureInfo(failureInfoDto);
        }
        exportService.exportFailureInformationsToDMZ();
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @PostMapping("/condense")
    @ApiOperation(value = "Verdichten mehrerer Störungsinformationen zu einer")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformationen erfolgreich verdichtet"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<FailureInformationDto> condenseFailureInfos(@ApiParam("Liste UUIDs der zu verdichtenden Störungen")
             @RequestBody List<UUID> listUuids) {
        FailureInformationDto condensedGfDto = failureInformationService.condenseFailureInfos(listUuids, Optional.empty());
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(condensedGfDto.getUuid())
                .toUri();
        exportService.exportFailureInformationsToDMZ();
        return ResponseEntity.created(location).body(condensedGfDto);
    }

    @GetMapping("/condensed/{uuid}")
    @ApiOperation(value = "Anzeigen von Störungsinformationen, die einer Verdichtung zuzuordnen sind")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<FailureInformationDto> getFailureInformations(@PathVariable UUID uuid) {
        return failureInformationService.findFailureInformationsByCondensedUuid(uuid);
    }

    @PutMapping("/{failureInfoUuid}/condense")
    @ApiOperation(value = "Update einer verdichteten Störungsinfo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public FailureInformationDto updateSubordinatedFailureInformations(
            @ApiParam(name="failureInfoUuid", value= "Liste UUIDs der zu verdichtenden Störungen", required = true)
            @PathVariable UUID failureInfoUuid,
            @RequestBody List<UUID> listSubordinatedUuids) {

        return failureInformationService.updateSubordinatedFailureInfos(failureInfoUuid, listSubordinatedUuids);
    }

    @PostMapping("/{failureInfoUuid}/channels")
    @ApiOperation(value = "Insert eines Veröffentlichungs-Kanals für eine Störungsinfo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public FailureInformationPublicationChannelDto insertChannelForFailureInfo(
            @ApiParam(name="failureInfoUuid", value= "UUID der Störungsinfo", required = true)
            @PathVariable UUID failureInfoUuid,
            @RequestParam String publicationChannel) {

        return failureInformationService.insertPublicationChannelForFailureInfo(failureInfoUuid, publicationChannel, false);
    }

    @DeleteMapping("/{failureInfoUuid}/channels")
    @ApiOperation(value = "Löschen eines Veröffentlichungs-Kanals für eine Störungsinfo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public void deleteChannelForFailureInfo(
            @ApiParam(name="failureInfoUuid", value= "UUID der Störungsinfo", required = true)
            @PathVariable UUID failureInfoUuid,
            @RequestParam String publicationChannel) {

        failureInformationService.deletePublicationChannelForFailureInfo(failureInfoUuid, publicationChannel);
    }

    @GetMapping("/{failureInfoUuid}/channels")
    @ApiOperation(value = "Holen Veröffentlichungs-Kanälen für eine Störungsinfo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformationen wurden nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    public List<FailureInformationPublicationChannelDto> getChannelsForFailureInfo(
            @ApiParam(name="failureInfoUuid", value= "UUID der Störungsinfo", required = true)
            @PathVariable UUID failureInfoUuid) {
        return failureInformationService.getPublicationChannelsForFailureInfo(failureInfoUuid);
    }

    @DeleteMapping("/{failureInfoUuid}")
    @ApiOperation(value = "Löschen einer neuen oder geplanten Störungsinformation durch den Erfasser")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Störungsinformation wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-CREATOR"})
    public void deleteFailureInfo(
            @ApiParam(name="failureInfoUuid", value= "UUID der Störungsinfo", required = true)
            @PathVariable UUID failureInfoUuid) {

        failureInformationService.deleteFailureInfo(failureInfoUuid);
    }

}
