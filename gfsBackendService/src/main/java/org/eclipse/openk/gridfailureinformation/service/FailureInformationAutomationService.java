package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureClassificationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.RadiusRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Log4j2
@EnableScheduling
@Component
public class FailureInformationAutomationService {

    @Autowired
    private BranchRepository branchRepo;

    @Autowired
    private FailureClassificationRepository classificationRepository;

    @Autowired
    private RadiusRepository radiusRepository;

    @Autowired
    private FailureInformationRepository failureRepo;

    @Autowired
    private StatusRepository statusRepo;

    @Autowired
    FailureInformationService failureService;

    @Autowired
    StationService stationService;

    @Autowired
    EntityManager em;

    @Value("${failureAutoClose:false}")
    private boolean failureAutoClose;

    @Value("${failureAutoCloseHoursAfter:48}")
    private int failureAutoCloseHoursAfter;

    @Scheduled(cron = "${failureAutoCloseCron:-}")
    @Transactional
    public void autoEndFailures() {

        if (!failureAutoClose) {
            return;
        }

        RefStatus abgeschlossen = statusRepo.findByStatus("abgeschlossen").orElse(new RefStatus());
        RefStatus storniert = statusRepo.findByStatus("storniert").orElse(new RefStatus());

        List<TblFailureInformation> openFailures = failureRepo.findByRefStatusInternNotAndRefStatusInternNot(abgeschlossen, storniert);

        for (TblFailureInformation failure : openFailures) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(failure.getModDate());
            calendar.add(Calendar.HOUR_OF_DAY, failureAutoCloseHoursAfter);

            Date compareDate = calendar.getTime();

            if (failure.getFailureEndPlanned() != null) {
                compareDate = failure.getFailureEndPlanned();
            }
            
            Instant instandLocalTimeZone = OffsetDateTime.ofInstant(Instant.now(), ZoneId.of("Europe/Berlin")).toLocalDateTime().toInstant(ZoneOffset.ofHours(0));
            if (TimeZone.getDefault().getID().equals("Europe/Berlin")) {
                instandLocalTimeZone = Instant.now();
            }

            if (compareDate.toInstant().isAfter(instandLocalTimeZone)) {
                continue;
            }

            log.info("Autoclosing FailureInformation mit UUID " + failure.getId());
            log.trace(failure.getId());
            log.trace(compareDate);
            log.trace(instandLocalTimeZone);
            log.trace(compareDate.toInstant().isAfter(instandLocalTimeZone));

            FailureInformationDto dto = failureService.findFailureInformation(failure.getUuid());
            dto.setStatusInternId(abgeschlossen.getUuid());
            dto.setResponsibility(dto.getResponsibility() != null ? dto.getResponsibility() : "");

            failureService.processGrid(dto, null);

        }
    }

}
