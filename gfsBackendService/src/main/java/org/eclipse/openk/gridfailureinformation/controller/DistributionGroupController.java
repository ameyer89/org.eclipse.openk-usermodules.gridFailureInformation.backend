/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.ConflictException;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupService;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/distribution-groups")
public class DistributionGroupController {

    @Autowired
    private DistributionGroupService distributionGroupService;

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeigen aller Verteilergruppen")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @GetMapping
    public List<DistributionGroupDto> findDistributionGroups() {
        return distributionGroupService.getDistributionGroups();
    }

    @GetMapping("/{uuid}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anzeigen einer bestimmten Verteilergruppe")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Verteilergruppe wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public DistributionGroupDto readDistributionGroup(
            @PathVariable UUID uuid) {
        return distributionGroupService.getDistributionGroupByUuid(uuid);
    }

    @PostMapping
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Anlegen einer neuen Verteilergruppe")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Verteilergruppe erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<DistributionGroupDto> insertDistributionGroup(@Validated @RequestBody DistributionGroupDto distributionGroupDto) {
        DistributionGroupDto savedDistributionGroupDto = distributionGroupService.insertDistributionGroup(distributionGroupDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedDistributionGroupDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedDistributionGroupDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Eine bestimmte Verteilergruppe löschen")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreich gelöscht"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    @DeleteMapping("/{groupUuid}")
    public void deleteDistributionGroup(
            @PathVariable UUID groupUuid) {
        try {
            distributionGroupService.deleteDistributionGroup(groupUuid);
        }
        catch ( Exception e ) {
            log.error("Exception in delete distribution group: ", e);
            throw new ConflictException("distribution.group.still.in.use");
        }
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ApiOperation(value = "Eine bestimmte Verteilergruppe bearbeiten.")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Verteilergruppe nicht gefunden."),
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Verteilergruppe erfolgreich geändert.")})
    @PutMapping("/{groupUuid}")
    public ResponseEntity updateDistributionGroup(
            @PathVariable UUID groupUuid,
            @Validated @RequestBody DistributionGroupDto distributionGroupDto) {
        if (!groupUuid.equals(distributionGroupDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        distributionGroupService.updateGroup(distributionGroupDto);
        return ResponseEntity.ok().build();
    }

}
