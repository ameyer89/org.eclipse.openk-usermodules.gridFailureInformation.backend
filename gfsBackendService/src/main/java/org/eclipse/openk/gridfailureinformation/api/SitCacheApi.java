/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.api;


import org.eclipse.openk.gridfailureinformation.viewmodel.ExportGridFailures;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExportSettings;
import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "${services.sitCache.name}")
public interface SitCacheApi {

    @PostMapping("/internal-sit")
    public void postPublicFailureInfos(
            @RequestBody ExportGridFailures export);

    @PostMapping("/fe-settings")
    public void postFeSettings(
            @RequestBody ExportSettings feSettings);

}
