/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessEnvironment;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.config.rabbitmq.RabbitMqConfig;
import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.model.RefFailureClassification;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationPublicationChannel;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
@Component
@Transactional()
@Data
public class ProcessHelper {

    @Lazy
    @Autowired
    private RabbitMqConfig rabbitMqConfig;

    @Lazy
    @Autowired
    private GfiProcessEnvironment environment;

    @Value("${process.definitions.classification.plannedMeasureDbid}")
    long plannedMeasureDbId;

    public boolean isFailureInfoPlanned(FailureInformationDto dto) {
        RefFailureClassification refPlannedMeasure = environment.getFailureClassificationRepository()
                           .findById(plannedMeasureDbId)
                            .orElseThrow(() -> new InternalServerErrorException("planned.measure.obj.not.found"));
        return refPlannedMeasure.getUuid().equals(dto.getFailureClassificationId());
    }

    @Transactional
    public FailureInformationDto storeFailureFromViewModel( FailureInformationDto dto ) {
        return environment.getFailureInformationService().storeFailureInfo(dto, GfiProcessState.NEW);
    }

    public ProcessState getProcessStateFromStatusUuid( UUID statusUuid ) {
        Optional<RefStatus> optRefStatus = environment.getStatusRepository().findByUuid(statusUuid);
        if( !optRefStatus.isPresent()) {
            log.error("RefStatus <"+statusUuid+"> not found in DB");
            throw  new InternalServerErrorException("status.uuid.not.found");
        }
        return GfiProcessState.fromValue(optRefStatus.get().getId());
    }

    public FailureInformationDto storeEditStatus( FailureInformationDto dto, GfiProcessState newState ) {
        UUID statusUuid = environment.getStatusRepository()
                .findById(newState.getStatusValue())
                .orElseThrow( () -> new NotFoundException( "status.not.found"))
                .getUuid();

        dto.setStatusInternId(statusUuid);
        return environment.getFailureInformationService().storeFailureInfo(dto, GfiProcessState.NEW);
    }

    public void publishFailureInformation(FailureInformationDto dto, boolean onlyMail, GfiProcessState processState) {
        List<String> finalChannelList = getChannelsToPublishList(dto.getUuid(), onlyMail);
        environment.getExportService().exportFailureInformation(dto.getUuid(), finalChannelList.toArray(new String[0]), processState);
    }

    public List<String> getChannelsToPublishList(UUID uuid, boolean onlyMail) {
        TblFailureInformation tblFailureInfo = environment.getFailureInformationRepository().findByUuid(uuid).orElseThrow(()-> new NotFoundException(""));
        List<TblFailureInformationPublicationChannel> channelList = environment.getChannelRepository().findByTblFailureInformation(tblFailureInfo);
        List<String> finalChannelList = channelList.stream().map(TblFailureInformationPublicationChannel::getPublicationChannel).collect(Collectors.toList());

        if (onlyMail){
            //remove all but email && sms channel
            finalChannelList.removeIf(p -> !p.equals(rabbitMqConfig.getChannelTypeToNameMap().get(Constants.CHANNEL_TPYE_SHORT_MAIL))
                    && !p.equals(rabbitMqConfig.getChannelTypeToNameMap().get(Constants.CHANNEL_TPYE_LONG_MAIL)));
        }
        return finalChannelList;
    }

    public void resetPublishedStateForChannels( FailureInformationDto dto ) {
        TblFailureInformation tblFailureInformation = environment.getFailureInformationRepository().findByUuid(dto.getUuid())
                .orElseThrow(() -> new NotFoundException("failure.information.not.found"));

        List<TblFailureInformationPublicationChannel> channelList = environment.getChannelRepository().findByTblFailureInformation(tblFailureInformation);

        channelList.forEach(x -> {
            x.setPublished(false);
            environment.getChannelRepository().save(x);
        });
    }

    public List<FailureInformationDto> getSubordinatedChildren(FailureInformationDto dto ) {
        return environment.getFailureInformationService().findFailureInformationsByCondensedUuid(dto.getUuid());
    }

}
