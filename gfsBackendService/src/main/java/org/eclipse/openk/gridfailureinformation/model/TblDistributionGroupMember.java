/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.util.UUID;

@Data
@Entity
public class TblDistributionGroupMember {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "TBL_DISTR_GROUP_MEMB_ID_SEQ")
    @SequenceGenerator(name = "TBL_DISTR_GROUP_MEMB_ID_SEQ", sequenceName = "TBL_DISTR_GROUP_MEMB_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private UUID uuid;

    @ManyToOne
    @JoinColumn( name = "fk_tbl_distribution_group")
    private TblDistributionGroup tblDistributionGroup;

    private UUID contactId;
    private String postcodes;
}
