/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.viewmodel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class FailureInformationDto implements Serializable {
    @JsonProperty("id")
    private UUID uuid;
    private String title;
    private String description;
    private Long versionNumber;
    private String responsibility;
    private String voltageLevel;
    private String pressureLevel;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureBegin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndPlanned;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date failureEndResupplied;

    private String internalRemark;
    private String postcode;
    private String city;
    private String district;
    private String street;
    private String housenumber;
    private String stationId;
    private String stationDescription;
    private String stationCoords;
    private String freetextPostcode;
    private String freetextCity;
    private String freetextDistrict;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private String objectReferenceExternalSystem;
    private String publicationStatus;
    private String publicationFreetext;
    private Boolean condensed;
    private Long condensedCount;
    private String faultLocationArea;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date createDate;
    private String createUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date modDate;
    private String modUser;

    private UUID failureClassificationId;
    private String failureClassification;

    private boolean isPlanned;

    private UUID failureTypeId;
    private String failureType;

    private UUID statusInternId;
    private String statusIntern;

    private String statusExtern;

    private UUID branchId;
    private String branch;
    private String branchDescription;
    private String branchColorCode;

    private UUID radiusId;
    private Long radius;

    private UUID expectedReasonId;
    private String expectedReasonText;

    private UUID failureInformationCondensedId;

    private List<StationPolygonDto> addressPolygonPoints;
    private List<UUID> stationIds;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof FailureInformationDto)) return false;

        FailureInformationDto that = (FailureInformationDto) o;

        return new EqualsBuilder().append(isPlanned, that.isPlanned).append(uuid, that.uuid).append(title,
                that.title).append(description, that.description).append(versionNumber, that.versionNumber).append(responsibility, that.responsibility).append(voltageLevel, that.voltageLevel).append(pressureLevel, that.pressureLevel).append(failureBegin, that.failureBegin).append(failureEndPlanned, that.failureEndPlanned).append(failureEndResupplied, that.failureEndResupplied).append(internalRemark, that.internalRemark).append(postcode, that.postcode).append(city, that.city).append(district, that.district).append(street, that.street).append(housenumber, that.housenumber).append(stationId, that.stationId).append(stationDescription, that.stationDescription).append(stationCoords, that.stationCoords).append(freetextPostcode, that.freetextPostcode).append(freetextCity, that.freetextCity).append(freetextDistrict, that.freetextDistrict).append(longitude, that.longitude).append(latitude, that.latitude).append(objectReferenceExternalSystem, that.objectReferenceExternalSystem).append(publicationStatus, that.publicationStatus).append(publicationFreetext, that.publicationFreetext).append(condensed, that.condensed).append(condensedCount, that.condensedCount).append(faultLocationArea, that.faultLocationArea).append(createUser, that.createUser).append(modUser, that.modUser).append(failureClassificationId, that.failureClassificationId).append(failureClassification, that.failureClassification).append(failureTypeId, that.failureTypeId).append(failureType, that.failureType).append(statusInternId, that.statusInternId).append(statusIntern, that.statusIntern).append(statusExtern, that.statusExtern).append(branchId, that.branchId).append(branch, that.branch).append(branchDescription, that.branchDescription).append(radiusId, that.radiusId).append(radius, that.radius).append(expectedReasonId, that.expectedReasonId).append(expectedReasonText, that.expectedReasonText).append(failureInformationCondensedId, that.failureInformationCondensedId).append(addressPolygonPoints, that.addressPolygonPoints).append(stationIds, that.stationIds).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(uuid).append(title).append(description).append(versionNumber).append(responsibility).append(voltageLevel).append(pressureLevel).append(failureBegin).append(failureEndPlanned).append(failureEndResupplied).append(internalRemark).append(postcode).append(city).append(district).append(street).append(housenumber).append(stationId).append(stationDescription).append(stationCoords).append(freetextPostcode).append(freetextCity).append(freetextDistrict).append(longitude).append(latitude).append(objectReferenceExternalSystem).append(publicationStatus).append(publicationFreetext).append(condensed).append(condensedCount).append(faultLocationArea).append(createUser).append(modUser).append(failureClassificationId).append(failureClassification).append(isPlanned).append(failureTypeId).append(failureType).append(statusInternId).append(statusIntern).append(statusExtern).append(branchId).append(branch).append(branchDescription).append(radiusId).append(radius).append(expectedReasonId).append(expectedReasonText).append(failureInformationCondensedId).append(addressPolygonPoints).append(stationIds).toHashCode();
    }
}
