/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.email;

import lombok.extern.log4j.Log4j2;

import javax.mail.MessagingException;
import java.util.List;

@Log4j2
public class GfiEmail {

    protected org.eclipse.openk.gridfailureinformation.email.Email email;

    protected String subject;
    protected String emailBody;

    public GfiEmail(EmailConfig emailConfig) throws MessagingException {
        email = new org.eclipse.openk.gridfailureinformation.email.Email(emailConfig.getSmtpHost(), emailConfig.getEmailPort(), emailConfig.isHtmlEmail());
        email.setFrom(emailConfig.getSender());
    }

    public void setEmailContent(String subject, String textBody) {
        log.debug("loading email content");
        this.emailBody = textBody;
        this.subject = subject == null ? "- kein Betreff -" : subject;
        log.debug("finsished loading email content");
    }

    private void prepareMailToSend() throws MessagingException {
        email.addText(emailBody);
        email.setSubject(subject);
    }

    public void setRecipientsTo(List<String> recipientsTo) throws MessagingException {
        email.addRecipients(recipientsTo);
    }

    public void sendEmail() throws MessagingException {

        try {
            prepareMailToSend();
            boolean ret = email.send();
            if (ret) {
                log.info("Email with subject: " + email.getMessage().getSubject() + " has been sent successfully sent to smtpserver");
            } else {
                log.error("Some error occured while sending email to smtpserver with subject: " + email.getMessage().getSubject() + " . Email wasn't sent.");
                throw new MessagingException();
            }
        } catch (MessagingException e) {
            log.error("Error occured in sendEmail", e);
            throw e;
        }
    }

}
