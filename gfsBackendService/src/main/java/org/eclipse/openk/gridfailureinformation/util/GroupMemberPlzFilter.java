/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.util;

import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GroupMemberPlzFilter {
    @Autowired
    private FailureInformationStationRepository fiStationRepository;

    @Autowired
    private AddressRepository addressRepository;


    public List<DistributionGroupMemberDto> filter(List<DistributionGroupMemberDto> memberList, TblFailureInformation existingTblFailureInfo) {
        Set<String> fiPostcodes = calculatePostcodes( existingTblFailureInfo );
        return memberList.stream()
                .filter( x -> noPlzOrContainedInSet( x, fiPostcodes ))
                .collect( Collectors.toList());
    }

    private Set<String> calculatePostcodes( TblFailureInformation tblFi ) {
        Set<String> postcodesInFi = new HashSet<>();
        if( tblFi.getPostcode() != null ) {
            postcodesInFi.add(tblFi.getPostcode());
        }
        List<String> stationsIds = fiStationRepository.findByFailureInformationId(tblFi.getId()).stream()
                .map(TblFailinfoStation::getStationStationId)
                .collect(Collectors.toList());

        stationsIds.forEach( x -> postcodesInFi.addAll(evaluatePostcodesForStation( x )));

        return postcodesInFi;
    }

    private List<String> evaluatePostcodesForStation(String stationId ) {
        return addressRepository.findByStationId(stationId).stream()
                .filter( x -> x.getPostcode() != null )
                .map(TblAddress::getPostcode)
                .collect(Collectors.toList());
     }

    private boolean noPlzOrContainedInSet( DistributionGroupMemberDto dtoToCheck, Set<String> plzs) {
        // no plz defined: -> mailaddress valid
        if( dtoToCheck.getPostcodeList() == null || dtoToCheck.getPostcodeList().isEmpty() ) {
            return true;
        }

        // look if at least one plz of dtoToCheck is contained in the given plzs-Set
        return dtoToCheck.getPostcodeList().stream()
                .anyMatch( plzs::contains );
    }
}
