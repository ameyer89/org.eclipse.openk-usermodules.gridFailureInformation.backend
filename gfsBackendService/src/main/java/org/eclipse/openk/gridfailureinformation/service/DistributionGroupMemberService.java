/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.api.ContactApi;
import org.eclipse.openk.gridfailureinformation.api.dto.CommunicationDto;
import org.eclipse.openk.gridfailureinformation.api.dto.VwDetailedContact;
import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMemberMapper;
import org.eclipse.openk.gridfailureinformation.model.JwtToken;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupMemberRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class DistributionGroupMemberService {

    @Autowired
    private DistributionGroupMemberRepository distributionGroupMemberRepository;

    @Autowired
    private DistributionGroupMemberMapper distributionGroupMemberMapper;

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    private DistributionGroupMapper distributionGroupMapper;

    @Autowired
    private ContactApi contactApi;

    @Autowired
    private AuthNAuthService authNAuthService;

    @Value("${services.contacts.communicationType.mobile}")
    private String communicationTypeMobile;

    private static final String DISTRIBUTION_GROUP_NOT_EXISTING = "distribution.group.uuid.not.existing";

    public List<DistributionGroupMemberDto> getDistributionGroupMembers() {

        return distributionGroupMemberRepository.findAll().stream()
                .map( distributionGroupMemberMapper::toDistributionGroupMemberDto )
                .collect(Collectors.toList());
    }

    public DistributionGroupMemberDto getMemberByUuid(UUID groupUuid, UUID memberUuuid ) {
        TblDistributionGroupMember tblDistributionGroupMember = distributionGroupMemberRepository.findByUuid(memberUuuid)
                .orElseThrow(NotFoundException::new);
        if(!tblDistributionGroupMember.getTblDistributionGroup().getUuid().equals(groupUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        DistributionGroupMemberDto memberDto = distributionGroupMemberMapper.toDistributionGroupMemberDto(tblDistributionGroupMember);

        DistributionGroupMemberDto distributionGroupMemberDto;
        JwtToken technicalJWT = authNAuthService.getTechnicalJWT();
        try {
            distributionGroupMemberDto = completeMemberDto(memberDto, technicalJWT);
        } finally {
            authNAuthService.logout(technicalJWT.getAccessToken());
        }

        return distributionGroupMemberDto;
    }

    public List<DistributionGroupMemberDto> getMembersByGroupId(UUID groupUuid) {
        List<DistributionGroupMemberDto> memberDtoList = distributionGroupMemberRepository.findByTblDistributionGroupUuid(groupUuid).stream()
                .map( distributionGroupMemberMapper::toDistributionGroupMemberDto )
                .collect(Collectors.toList());

        JwtToken technicalJWT = authNAuthService.getTechnicalJWT();
        try {
            for (DistributionGroupMemberDto memberDto : memberDtoList) {
                completeMemberDto(memberDto, technicalJWT);
            }
        } finally {
            authNAuthService.logout(technicalJWT.getAccessToken());
        }

        return memberDtoList;
    }

    @Transactional
    public DistributionGroupMemberDto insertDistributionGroupMember(UUID groupUuid, DistributionGroupMemberDto distributionGroupMemberDto) {

        TblDistributionGroup tblDistributionGroup = distributionGroupRepository
                .findByUuid(groupUuid)
                .orElseThrow(() -> new NotFoundException(DISTRIBUTION_GROUP_NOT_EXISTING));

        TblDistributionGroupMember distributionGroupMemberToSave = distributionGroupMemberMapper.toTblDistributionGroupMember(distributionGroupMemberDto);
        distributionGroupMemberToSave.setUuid(UUID.randomUUID());
        distributionGroupMemberToSave.setTblDistributionGroup(tblDistributionGroup);

        if ( !checkUniqueContactForGroupMemberForInsert(tblDistributionGroup.getId(), distributionGroupMemberToSave.getContactId())) {
            throw new OperationDeniedException(OperationType.INSERT, "distribution.group.member.already.existing.for.group");
        }

        TblDistributionGroupMember savedDistributionGroupMember = distributionGroupMemberRepository.save(distributionGroupMemberToSave);
        return distributionGroupMemberMapper.toDistributionGroupMemberDto(savedDistributionGroupMember);
    }


    @Transactional
    public ResponseEntity<Resource> handleLoadFile(UUID groupUuid) {
        String distGroupName = distributionGroupRepository.findByUuid(groupUuid)
                .orElseThrow( () -> new NotFoundException("distribution.group.not.found"))
                .getName();
        List<DistributionGroupMemberDto> dtoList = getMembersByGroupId(groupUuid);
        String csv = getBuildCsvFileAsString(dtoList);

        byte[] binret = csv.getBytes(StandardCharsets.UTF_8);
        final ResponseEntity.BodyBuilder responseBuilder = ResponseEntity
                .status(HttpStatus.OK)
                .contentLength(binret.length)
                .contentType(MediaType.valueOf("text/csv"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + distGroupName.replaceAll("[\\\\/:*?\"<>|]", "") + ".txt\"");
        return responseBuilder.body(new ByteArrayResource(binret));
    }

    private String getBuildCsvFileAsString(List<DistributionGroupMemberDto> dtoList) {
        StringBuilder builder = new StringBuilder();
        builder.append("name;email;mobilenumber\r\n");
        dtoList.forEach( x-> buildCsvLine(builder, x));
        return builder.toString();
    }

    private void buildCsvLine(StringBuilder builder, DistributionGroupMemberDto x) {
        builder.append(Optional.ofNullable(x.getName()).orElse("")+";");
        builder.append(Optional.ofNullable(x.getEmail()).orElse("")+";");
        builder.append(Optional.ofNullable(x.getMobileNumber()).orElse("")+"\r\n");
    }

    @Transactional
    public void deleteDistributionGroupMember(UUID groupUuid, UUID memberUuid) {
        TblDistributionGroupMember existingMember = distributionGroupMemberRepository.findByUuid(memberUuid)
                .orElseThrow( () -> new BadRequestException("distribution.group.member.uuid.not.existing"));

        distributionGroupMemberRepository.delete(existingMember);
    }

    @Transactional
    public DistributionGroupMemberDto updateGroupMember(UUID groupUuid, DistributionGroupMemberDto distributionGroupMemberDto) {

        TblDistributionGroup group = distributionGroupRepository.findByUuid(groupUuid)
                .orElseThrow(() -> new NotFoundException(DISTRIBUTION_GROUP_NOT_EXISTING));

        TblDistributionGroupMember member = distributionGroupMemberRepository.findByUuid(distributionGroupMemberDto.getUuid())
                .orElseThrow(() -> new NotFoundException("distribution.group.member.uuid.not.existing"));

        TblDistributionGroupMember memberToSave = distributionGroupMemberMapper.toTblDistributionGroupMember(distributionGroupMemberDto);
        memberToSave.setTblDistributionGroup(group);
        memberToSave.setId(member.getId());

        if ( !checkUniqueContactForGroupMemberForUpdate(group.getId(), memberToSave.getContactId(), memberToSave.getUuid())) {
            throw new OperationDeniedException(OperationType.UPDATE, "distribution.group.member.already.existing.for.group");
        }

        TblDistributionGroupMember savedMember = distributionGroupMemberRepository.save(memberToSave);
        return distributionGroupMemberMapper.toDistributionGroupMemberDto(savedMember);
    }

    private boolean checkUniqueContactForGroupMemberForInsert( Long distributionGroupId, UUID contactId){
       Long result = distributionGroupMemberRepository.countByDistributionGroupIdAndContactId(distributionGroupId, contactId);
       return result==0;
    }

    private boolean checkUniqueContactForGroupMemberForUpdate( Long distributionGroupId, UUID contactId, UUID memberUuid){
        Long result = distributionGroupMemberRepository.countByDistributionGroupIdAndContactIdAndIsNotSame(distributionGroupId, contactId, memberUuid) ;
        return result==0;
    }

    private DistributionGroupMemberDto completeMemberDto (DistributionGroupMemberDto groupMemberDto, JwtToken technicalJWT) {

        VwDetailedContact contact = null;
        try {
            contact = contactApi.findContact(groupMemberDto.getContactId(), technicalJWT.getAccessToken());
        } catch (Exception e) {
            log.error("Error while calling contactApi.findContact with groupMemberDto.getContactId: " + groupMemberDto.getContactId(), e);
        }

        if( contact == null ) {
            return groupMemberDto;
        }
        groupMemberDto.setName(contact.getName());
        groupMemberDto.setEmail(contact.getEmail());
        groupMemberDto.setMainAddress(contact.getMainAddress());
        groupMemberDto.setSalutationType(contact.getSalutationType());
        groupMemberDto.setPersonType(contact.getPersonType());
        groupMemberDto.setDepartment(contact.getDepartment());

        List<CommunicationDto> communicationDtoList = contactApi.getContactCommunications(groupMemberDto.getContactId(), technicalJWT.getAccessToken());
        if( communicationDtoList != null ) {
            groupMemberDto.setMobileNumber(
                    communicationDtoList
                            .stream()
                            .filter(f -> f.getCommunicationTypeType()
                                    .equals(communicationTypeMobile))
                            .findFirst()
                            .orElseGet(CommunicationDto::new)
                            .getCommunicationData());
        }
        return groupMemberDto;
    }

}
