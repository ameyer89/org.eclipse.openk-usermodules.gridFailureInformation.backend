/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessState;

@Log4j2
public enum GfiProcessState implements ProcessState {
    NEW                     (1),
    PLANNED                 (2),
    CREATED                 (3),
    CANCELED                (4),
    QUALIFIED               (5),
    UPDATED                 (6),
    COMPLETED               (7),
    CHECKED_FOR_PUBLISH_    ( 20),// NOSONAR virtual state not for storing in db
    UNDEFINED_              (-1); // NOSONAR

    private final long statusValue;

    GfiProcessState(long statusValue ) {
        this.statusValue = statusValue;
    }

    public long getStatusValue() {
        return statusValue;
    }

    public static ProcessState fromValue( long statusValue ) { // NOSONAR complexity high but simple
        switch( (int)statusValue ) {
            case 1:
                return NEW;
            case 2:
                return PLANNED;
            case 3:
                return CREATED;
            case 4:
                return CANCELED;
            case 5:
                return QUALIFIED;
            case 6:
                return UPDATED;
            case 7:
                return COMPLETED;
            case 20:
                return CHECKED_FOR_PUBLISH_;
            default:
                log.error("Invalid statusValue:"+statusValue);
                return UNDEFINED_;
        }
    }
}
