package org.eclipse.openk.gridfailureinformation.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.util.ResourceLoaderBase;
import org.eclipse.openk.gridfailureinformation.viewmodel.geojson.StationGeoJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ImportGeoJsonService {

    private static final Logger log = LoggerFactory.getLogger("geojsonlogger");
    private File importedFolder;
    private File invalidFolder;


    @Value("${geoJsonImport.filepattern:^geoj_(\\d+)_(.*).(json)$}")
    private String filePattern;

    @Value("${geoJsonImport.folder:}")
    private String folderToScan;

    private Pattern pattern;

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Bean
    public void initImportGeoJsonService(){
        log.info("init initImportGeoJsonService");
        pattern = Pattern.compile(filePattern);
        importedFolder = new File(folderToScan+ "/imported");
        invalidFolder = new File(folderToScan+ "/invalid");
        invalidFolder.mkdir();
        importedFolder.mkdir();
    }

    public boolean isValid(final String stringToTest) {
        Matcher matcher = pattern.matcher(stringToTest);
        return matcher.matches();
    }
    @Transactional
    public void importGeoJsonFile() {
        ResourceLoaderBase resourceLoader = new ResourceLoaderBase();
        Collection<File> fileList = getFilesForFolder();
        for (File file : fileList) {
            String fileName = file.getName();
            log.info("Filename: {}", fileName);
            if (isValid(fileName)) {
                log.debug("Filename valid");
                String stationId = fileName.split("_")[1];
                log.debug("StationId: {}", stationId);
                String fileContent = readFileContentAndValidate(objectMapper, resourceLoader, file);

                boolean isSuccessfullySaved = saveGeoJsonContent(fileContent, stationId);
                if (isSuccessfullySaved) {
                    moveFile(importedFolder, file);
                } else {
                    moveFile(invalidFolder, file);
                }
            } else {
                log.warn("Invalid filename for geoJsonImport: {}", file.getAbsolutePath());
                moveFile(invalidFolder, file);
            }
        }
    }

    public Collection<File> getFilesForFolder() {
        return FileUtils.listFiles(new File(folderToScan), null, false);
    }

    private void moveFile(File targetFolder, File file) {
        File targetFile = new File(targetFolder, file.getName());
        try {
            Files.move(file.toPath(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            log.info("File successfully moved to: {}", targetFile.getAbsolutePath());
        } catch (IOException e) {
            log.error("Error while moving file: " + file.toPath(), e);
        }
    }

    private String readFileContentAndValidate(ObjectMapper objectMapper, ResourceLoaderBase resourceLoader, File file) {
        String filecontent = resourceLoader.loadFromPath(file.getPath());

        StationGeoJson stationGeoJson;
        try {
            stationGeoJson = objectMapper.readValue(filecontent, StationGeoJson.class);
            log.debug("Stationname: {}", stationGeoJson.getName());
            return filecontent;
        } catch (JsonProcessingException e) {
            log.error("Error parsing GeoJson for file: " + file.getAbsolutePath(), e);
        }
        return null;
    }

    public boolean saveGeoJsonContent(String content, String stationId) {
        if (content == null) {
            return false;
        }
        log.debug("saveGeoJsonContent start");
        try {
            Optional<TblStation> tblStationOptional = stationRepository.findByStationId(stationId);
            if (tblStationOptional.isPresent()) {
                TblStation tblStation = tblStationOptional.get();
                tblStation.setGeoJson(content);
                stationRepository.save(tblStation);
                log.debug("saveGeoJsonContent finished");
                return true;
            } else {
                log.error("Station with stationId {} doesn't exist", stationId);
            }
        } catch (Exception e) {
            log.error("Error while saving GeoJsonContent", e);
        }
        return false;
    }
}
