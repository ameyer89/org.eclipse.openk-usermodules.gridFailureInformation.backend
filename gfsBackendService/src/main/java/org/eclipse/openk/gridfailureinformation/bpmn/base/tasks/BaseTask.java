/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base.tasks;


import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessException;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessSubject;
import org.eclipse.openk.gridfailureinformation.bpmn.base.ProcessTask;

@Log4j2
public abstract class BaseTask<T extends ProcessSubject> implements ProcessTask {
    private ProcessTask outputStep;
    private String description;

    protected BaseTask(String description ) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void leaveStep(ProcessSubject model) throws ProcessException {
        log.debug("On Leave: \""+description+"\"");
        onLeaveStep( (T)model );
        if(outputStep==null) {
            log.debug(description+": firing unconnected Output!");
        }
        else {
            log.debug("Left step: \""+description+"\"");
            outputStep.enterStep(model);
        }
    }

    @Override
    public void enterStep(ProcessSubject model ) throws ProcessException {
        log.debug("Enter: \""+description+"\"");
        onEnterStep((T)model);
    }

    @Override
    public void recover(ProcessSubject model) throws ProcessException {
        log.debug("Recover: \""+description+"\"");
        onRecover((T)model);
    }


    protected abstract void onLeaveStep(T model) throws ProcessException;
    protected abstract void onEnterStep(T model) throws ProcessException;
    protected abstract void onRecover(T model) throws ProcessException;

    @Override
    public void connectOutputTo(ProcessTask step) throws ProcessException {
        this.outputStep = step;
    }
}
