/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationReminderMailSentMapper;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformationReminderMailSent;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationReminderMailSentRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FailureInformationReminderMailSentService {

    @Autowired
    private FailureInformationService failureInformationService;

    @Autowired
    private FailureInformationReminderMailSentRepository failureInformationReminderMailSentRepository;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private FailureInformationReminderMailSentMapper failureInformationReminderMailSentMapper;

    @Autowired
    private FailureInformationMapper failureInformationMapper;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private ExportService exportService;

    @Value("${reminder.status-change.minutes-before}")
    long minutesBefore;

    @Value("${reminder.status-change.enabled}")
    boolean enabled;

    public Boolean displayStatusChangeReminderAndSendMails() {

        if (!enabled) {
           return false;
        }

        Page<FailureInformationDto> failureInfoPage = failureInformationService
            .findFailureInformationsForDisplay(PageRequest.of(0, 9999999))
            .map(failureInformationMapper::toFailureInformationDto);

        LocalDateTime reminderDate = LocalDateTime.now().plusMinutes(minutesBefore);

        List<FailureInformationDto> failureInformationDtoList =  failureInfoPage.getContent()
                .stream()
                .filter(f -> f.getStatusInternId() != statusRepository.findById(GfiProcessState.COMPLETED.getStatusValue()).get().getUuid())
                .filter(f -> f.getStatusInternId() != statusRepository.findById(GfiProcessState.CANCELED.getStatusValue()).get().getUuid())
                .filter(f -> f.getFailureEndPlanned() != null )
                .filter(f -> f.getFailureEndPlanned().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().isBefore(reminderDate) )
                .collect(Collectors.toList());

        if (failureInformationDtoList.isEmpty()) {
            return false;
        } else {
            sendReminderMails(failureInformationDtoList);
            return true;
        }

    }

    public void sendReminderMails(List<FailureInformationDto> failureInformationDtoList) {

        for (FailureInformationDto dto: failureInformationDtoList) {
            boolean sendMail = true;
            TblFailureInformation tblFailureInfo = failureInformationRepository.findByUuid(dto.getUuid())
                    .orElseThrow(() -> new NotFoundException("failure.information.not.found"));

            Optional<TblFailureInformationReminderMailSent> failureInformationReminderMailSentOptional =
                    failureInformationReminderMailSentRepository.findByTblFailureInformation(tblFailureInfo);

            if (failureInformationReminderMailSentOptional.isPresent()) {
                TblFailureInformationReminderMailSent failureInformationReminderMailSent = failureInformationReminderMailSentOptional.get();
                if (failureInformationReminderMailSent.getDateMailSent().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().isBefore(LocalDateTime.now()) ) {
                    sendMail = false;
                }
            }
            if (sendMail && exportService.exportFailureInformationReminderMail(dto.getUuid())) {
                    TblFailureInformationReminderMailSent tblToSave = new TblFailureInformationReminderMailSent();
                    tblToSave.setTblFailureInformation(tblFailureInfo);
                    tblToSave.setMailSent(true);
                    tblToSave.setDateMailSent(new Date());

                    failureInformationReminderMailSentRepository.save(tblToSave);
            }
        }

    }

}