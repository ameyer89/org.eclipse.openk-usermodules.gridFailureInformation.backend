/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.enums.OperationType;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.exceptions.OperationDeniedException;
import org.eclipse.openk.gridfailureinformation.mapper.FailureInformationStationMapper;
import org.eclipse.openk.gridfailureinformation.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblFailinfoStation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationStationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class FailureInformationStationService {

    @Autowired
    private StationRepository stationRepository;

    @Autowired
    private StationMapper stationMapper;

    @Autowired
    private FailureInformationRepository failureInformationRepository;

    @Autowired
    private FailureInformationStationRepository failureInformationStationRepository;

    @Autowired
    private FailureInformationStationMapper failureInformationStationMapper;

    public List<StationDto> findStationsByFailureInfo(UUID failureInfoUuid ) {

        List<UUID> tblStationList = failureInformationStationRepository
                .findUuidByFkTblFailureInformation(failureInfoUuid);
        List<TblStation> retStationList = new LinkedList<>();

        tblStationList.forEach(x -> addStationIfPresent(retStationList, x));

        return retStationList.stream()
                .map( stationMapper::toStationDto )
                .collect(Collectors.toList());
    }

    private void addStationIfPresent(List<TblStation> retStationList, UUID x) {
        Optional<TblStation> curr = stationRepository.findByUuid(x);
        if (curr.isPresent()) {
            retStationList.add(curr.get());
        }
    }


    @Transactional
    public FailureInformationStationDto insertFailureInfoStation(UUID failureInfoUuid, StationDto groupDto) {

        TblFailureInformation failureInformation = failureInformationRepository.findByUuid(failureInfoUuid)
                .orElseThrow(() -> new NotFoundException("failure.info.uuid.not.existing"));

        TblStation station = stationRepository.findByUuid(groupDto.getUuid())
                .orElseThrow(() -> new NotFoundException("station.uuid.not.existing"));

        Optional<TblFailinfoStation> fiStation = failureInformationStationRepository.findByFailureInformationIdAndStationStationId(failureInformation.getId(), station.getStationId());

        if (fiStation.isPresent()) {
            throw new OperationDeniedException(OperationType.INSERT, "double.assignment.of.stations");
        }

        TblFailinfoStation assignmentToSave = new TblFailinfoStation();
        assignmentToSave.setFailureInformation(failureInformation);
        assignmentToSave.setStationStationId(station.getStationId());

        TblFailinfoStation savedFailureInfoGroup = failureInformationStationRepository.save(assignmentToSave);

        return failureInformationStationMapper.toFailureInfoStationDto(savedFailureInfoGroup);

    }

    @Transactional
    public void deleteFailureInfoStation(UUID failureInfoUuid, UUID groupUuid) {
        TblFailureInformation failureInformation = failureInformationRepository.findByUuid(failureInfoUuid)
                .orElseThrow( () -> new BadRequestException("failure.info.uuid.not.existing"));

        TblStation station = stationRepository.findByUuid(groupUuid)
                .orElseThrow( () -> new BadRequestException("station.uuid.not.existing"));

        TblFailinfoStation tblFailureInformationStation
                = failureInformationStationRepository.findByFailureInformationIdAndStationStationId(failureInformation.getId(), station.getStationId())
                    .orElseThrow(() -> new BadRequestException("failure.info.station.not.existing"));

        failureInformationStationRepository.delete(tblFailureInformationStation);
    }


}
