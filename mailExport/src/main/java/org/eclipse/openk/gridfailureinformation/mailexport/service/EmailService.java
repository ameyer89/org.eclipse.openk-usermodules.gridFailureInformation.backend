/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.service;


import org.eclipse.openk.gridfailureinformation.mailexport.config.EmailConfig;
import org.eclipse.openk.gridfailureinformation.mailexport.dtos.MailMessageDto;
import org.eclipse.openk.gridfailureinformation.mailexport.email.GfiEmail;
import org.eclipse.openk.gridfailureinformation.mailexport.util.ResourceLoaderBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Service
public class EmailService {

    public static final String DATE_PATTERN = "dd.MM.yyyy, HH:mm:ss";

    public static final String TEMPLATE_EMAIL_CONTENT_PLACEHOLDER = "##EMAIL_CONTENT_PLACEHOLDER###";
    public static final String EMAIL_CREATION_SENT_DATE_PLACEHOLDER = "##CREATION_SENT_DATE###";

    @Autowired
    EmailConfig emailConfig;

    public void sendMail(MailMessageDto mailMessageDto) throws MessagingException {
        GfiEmail gfiEmail = new GfiEmail(emailConfig);
        gfiEmail.setEmailContent(mailMessageDto.getEmailSubject(), useHtmlEmailTemplate(mailMessageDto.getBody()));
        gfiEmail.setRecipientsTo(mailMessageDto.getMailAddresses());
        gfiEmail.sendEmail();
    }

    public void sendTestMail(String recipient) throws MessagingException {
        GfiEmail gfiEmail = new GfiEmail(emailConfig);
        List<String> mailAddressList = new ArrayList<>();
        mailAddressList.add(recipient);
        gfiEmail.setEmailContent("Test Betreff", useHtmlEmailTemplate("Email Text Inhalt Test"));
        gfiEmail.setRecipientsTo(mailAddressList);
        gfiEmail.sendEmail();
    }

    private String useHtmlEmailTemplate(String emailText) {
        if (!emailConfig.isHtmlEmail() || !emailConfig.isUseHtmlEmailTemplate()) {
            return emailText;
        }

        String htmlEmailTemplate = emailConfig.getHtmlEmailTemplate();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        String nowDateString = LocalDateTime.now().format(dateTimeFormatter);
        htmlEmailTemplate = htmlEmailTemplate.replace(EMAIL_CREATION_SENT_DATE_PLACEHOLDER, nowDateString);
        return htmlEmailTemplate.replace(TEMPLATE_EMAIL_CONTENT_PLACEHOLDER, emailText);
    }



}
