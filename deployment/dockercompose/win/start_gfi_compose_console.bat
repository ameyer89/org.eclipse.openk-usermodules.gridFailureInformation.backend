@echo off

rem Setzen Sie das Root-Verzeichnis, in dem sich die Docker-Compose-Projekte befinden
set "ROOT_DIR=%~dp0.."

rem Start Projekt 1 in einer neuen Konsole
start "gfi" cmd /c "cd /d %ROOT_DIR%/gfi && docker-compose up"

rem Start Projekt 2 in einer neuen Konsole
start "gfi-dmz" cmd /c "cd /d %ROOT_DIR%/gfi-dmz && docker-compose up"