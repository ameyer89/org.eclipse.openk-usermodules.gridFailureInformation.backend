@echo off

rem Setzen Sie das Root-Verzeichnis, in dem sich die Docker-Compose-Projekte befinden
set "ROOT_DIR=%~dp0.."

rem Start Projekt 1 in einer neuen Konsole
start "postgresql" cmd /c "cd /d %ROOT_DIR%/infrastructure/db/postgresql && docker-compose up"

timeout /t 1

:wait_for_network
REM Warten, bis das Netzwerk "openknet" verfügbar ist
docker network inspect openknet >nul 2>&1
if %errorlevel% neq 0 (
    echo "waiting for network"
    timeout /t 1
    goto wait_for_network
)

rem Start Projekt 2 in einer neuen Konsole
start "traefik" cmd /c "cd /d %ROOT_DIR%/infrastructure/traefik && docker-compose up"

rem Start Projekt 3 in einer neuen Konsole
start "portal" cmd /c "cd /d %ROOT_DIR%/portal && docker-compose up"

start "cbd" cmd /c "cd /d %ROOT_DIR%/cbd && docker-compose up"

rem Start Projekt 1 in einer neuen Konsole
start "gfi" cmd /c "cd /d %ROOT_DIR%/gfi && docker-compose up"

rem Start Projekt 2 in einer neuen Konsole
start "gfi-dmz" cmd /c "cd /d %ROOT_DIR%/gfi-dmz && docker-compose up"
