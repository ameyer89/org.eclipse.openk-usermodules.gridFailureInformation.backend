/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.mapper;

import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ViewGeplanteVU;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SARISMapper {

    @Mappings({
            @Mapping(target = "description", source = "bemerkung"),
            @Mapping(target = "failureBegin", source = "beginn", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", qualifiedByName = "convertToUTC3"),
            @Mapping(target = "failureEndPlanned", source = "ende", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",  qualifiedByName = "convertToUTC3"),
            @Mapping(target = "postcode", source = "plz"),
            @Mapping(target = "city", source = "ort"),
            @Mapping(target = "district", source = "ortsteil"),
            @Mapping(target = "street", source = "strasse"),
            @Mapping(target = "housenumber", source = "hausnummern")
    })
    ForeignFailureDataDto toForeignFailureDataDto(ViewGeplanteVU srcEntity);


    @AfterMapping
    default void afterMappingProcess(ViewGeplanteVU srcEntity, @MappingTarget ForeignFailureDataDto targetEntity){
        if (targetEntity.getDistrict() != null) {
            targetEntity.setDistrict(targetEntity.getDistrict().trim());
        }
        targetEntity.setRadiusInMeters(100L);

        targetEntity.setBranch(Constants.BRANCH_OTHER);
        if (srcEntity.getSparteID() == 2899) {
            targetEntity.setBranch(Constants.BRANCH_ELECTRICITY);
            targetEntity.setVoltageLevel(Constants.VOLTAGE_LVL_LOW);
        }
        if (srcEntity.getSparteID() == 2900) {
            targetEntity.setBranch(Constants.BRANCH_WATER);
        }
        if (srcEntity.getSparteID() == 2898) {
            targetEntity.setBranch(Constants.BRANCH_GAS);
        }

        if(srcEntity.getXKoordinate() != 0) {
            targetEntity.setLongitude(BigDecimal.valueOf(srcEntity.getXKoordinate()).setScale(6, RoundingMode.HALF_UP));
        }

        if(srcEntity.getYKoordinate() != 0) {
            targetEntity.setLatitude(BigDecimal.valueOf(srcEntity.getYKoordinate()).setScale(6, RoundingMode.HALF_UP));
        }
    }

    @Named("convertToUTC")
    default Date convertToUTC(XMLGregorianCalendar xmlCalendar) {
        if (xmlCalendar == null){
            return null;
        }
        TimeZone berlinTimeZone = TimeZone.getTimeZone("Europe/Berlin");
        GregorianCalendar localCalendar = xmlCalendar.toGregorianCalendar(berlinTimeZone, null, null);
        localCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return localCalendar.getTime();
    }

}