/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.sarisinterface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.sarisinterface.mapper.SARISMapper;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ArrayOfViewGeplanteVU;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ViewGeplanteVU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "rabbitmq")
public class ImportService {

    @Autowired
    private MessageChannel failureImportChannel;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    SARISMapper sarisMapper;

    @Autowired
    private SarisWebservice sarisWebservice;

    @Value("${gridFailureInformation.autopublish:false}")
    private boolean autopublish;
    @Value("${gridFailureInformation.onceOnlyImport:false}")
    private boolean onceOnlyImport;
    @Value("${gridFailureInformation.excludeEquals:true}")
    private boolean excludeEquals;
    @Value("${gridFailureInformation.excludeAlreadyEdited:true}")
    private boolean excludeAlreadyEdited;

    public void importForeignFailures(int sarisSparteId, boolean test, boolean mocked) {

        GetAktuelleGVUsInfoAllgemeinResponse sarisResponse;
        if (mocked) {
            sarisResponse = getMockedSarisResponse("sarisRealMockResponse.xml");
        } else {
            sarisResponse = sarisWebservice.getAktuelleGVU(sarisSparteId, test);
        }

        ArrayOfViewGeplanteVU getAktuelleGVUsInfoAllgemeinResult =
                sarisResponse.getGetAktuelleGVUsInfoAllgemeinResult();
        if (getAktuelleGVUsInfoAllgemeinResult != null) {
            List<ViewGeplanteVU> viewGeplanteVU =
                    sarisResponse.getGetAktuelleGVUsInfoAllgemeinResult().getViewGeplanteVU();
            List<ForeignFailureMessageDto> messageDtoList = new ArrayList<>();

            for (ViewGeplanteVU geplanteVU : viewGeplanteVU) {
                pushForeignFailure(createForeignFailureMessageDto(geplanteVU));
                ForeignFailureMessageDto foreignFailureMessageDto = createForeignFailureMessageDto(geplanteVU);
                messageDtoList.add(foreignFailureMessageDto);
                log.trace("foreignFailureMessageDto" + foreignFailureMessageDto);
            }
            log.debug(messageDtoList);
        }
    }

    private GetAktuelleGVUsInfoAllgemeinResponse getMockedSarisResponse(String xmlResponseFile) {
        GetAktuelleGVUsInfoAllgemeinResponse response = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(GetAktuelleGVUsInfoAllgemeinResponse.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            File file = new File(xmlResponseFile);
            response = (GetAktuelleGVUsInfoAllgemeinResponse) jaxbUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return response;
    }

    private ForeignFailureMessageDto createForeignFailureMessageDto(ViewGeplanteVU viewGeplanteVU) {
        ForeignFailureMessageDto foreignFailureMessageDto = new ForeignFailureMessageDto();
        foreignFailureMessageDto.setMetaId(viewGeplanteVU.getVersorgungsunterbrechungID()+"");
        foreignFailureMessageDto.setSource(Constants.SRC_SARIS);
        ForeignFailureDataDto foreignFailureDataDto = createForeignFailureData(viewGeplanteVU);
        foreignFailureMessageDto.setPayload(foreignFailureDataDto);
        return foreignFailureMessageDto;
    }

    private ForeignFailureDataDto createForeignFailureData(ViewGeplanteVU viewGeplanteVU) {
        ForeignFailureDataDto foreignFailureDataDto = sarisMapper.toForeignFailureDataDto(viewGeplanteVU);
        foreignFailureDataDto.setPlanned(true);
        foreignFailureDataDto.setAutopublish(autopublish);
        foreignFailureDataDto.setOnceOnlyImport(onceOnlyImport);
        foreignFailureDataDto.setExcludeEquals(excludeEquals);
        foreignFailureDataDto.setExcludeAlreadyEdited(excludeAlreadyEdited);
        return foreignFailureDataDto;
    }

    public void pushForeignFailure(ForeignFailureMessageDto foreignFailureMessageDto) {

        try {
            failureImportChannel.send(
                    MessageBuilder.withPayload(
                            objectMapper.writeValueAsString(foreignFailureMessageDto.getPayload()))
                            .setHeader("metaId", foreignFailureMessageDto.getMetaId())
                            .setHeader("description", foreignFailureMessageDto.getDescription())
                            .setHeader("source", foreignFailureMessageDto.getSource())
                            .build());
            log.info("Succesfully sent message from SARIS-Interface to main-app with metaId: " + foreignFailureMessageDto.getMetaId());
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.message");
        }

    }

}
