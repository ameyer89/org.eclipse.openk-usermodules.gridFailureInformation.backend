package org.eclipse.openk.gridfailureinformation.sarisinterface.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;

@Log4j2
@Configuration
@EnableSwagger2
@Profile({"!test"})
public class SwaggerConfig {

    @Value("${swagger.baseUrl:}")
    public String baseUrl;
    @Value("${swagger.proxyUrl:}")
    public String proxyUrl;

    @Bean
    public Docket api(ServletContext servletContext) {
        return new Docket(DocumentationType.SWAGGER_2).pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return baseUrl + super.getApplicationBasePath();
            }
        }).host(proxyUrl);
    }
}
