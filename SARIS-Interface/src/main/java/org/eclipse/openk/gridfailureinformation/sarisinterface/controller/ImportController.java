/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.ImportService;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.SarisWebservice;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ArrayOfViewGeplanteVU;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.ViewGeplanteVU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/saris")
public class ImportController {

    @Autowired
    private ImportService importService;

    @Autowired
    private SarisWebservice sarisWebservice;

    @GetMapping("/live-import-test")
    @ApiOperation(value = "Import einer externen Störungsinformation von SARIS zu SIT")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void liveImportUserNotification() {
      importService.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, false, false);
      importService.importForeignFailures(Constants.SARIS_GAS_BRANCH_ID, false, false);
      importService.importForeignFailures(Constants.SARIS_WATER_BRANCH_ID, false, false);
    }

    @GetMapping("/import-test")
    @ApiOperation(value = "Import einer externen Störungsinformation von Störungsauskunft.de zu SIT")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void importUserNotification(@RequestParam(defaultValue = "true") boolean mocked,
        @RequestParam(defaultValue = "true") boolean test ) {
        importService.importForeignFailures(Constants.SARIS_ELECTRICITY_BRANCH_ID, test, mocked);
    }

    @GetMapping("/response-test")
    @ApiOperation(value = "Response-Test für SARIS")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Response von Saris erfolgreich"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void responsetest() {
        log.info("response-test");
        GetAktuelleGVUsInfoAllgemeinResponse response = sarisWebservice.getAktuelleGVU(Constants.SARIS_ELECTRICITY_BRANCH_ID,true);
        log.info("RESPONSE received import-test: " + response);
        ArrayOfViewGeplanteVU getAktuelleGVUsInfoAllgemeinResult = response.getGetAktuelleGVUsInfoAllgemeinResult();
        if (getAktuelleGVUsInfoAllgemeinResult != null) {
            List<ViewGeplanteVU> viewGeplanteVU = response.getGetAktuelleGVUsInfoAllgemeinResult().getViewGeplanteVU();
            for (ViewGeplanteVU geplanteVU : viewGeplanteVU) {
                log.info("VersorgungsunterbrechungID: " + geplanteVU.getVersorgungsunterbrechungID());
            }
        }
    }

}
