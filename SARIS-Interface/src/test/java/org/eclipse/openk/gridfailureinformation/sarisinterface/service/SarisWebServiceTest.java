/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.service;

import org.eclipse.openk.gridfailureinformation.sarisinterface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.sarisinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.sarisinterface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.sarisinterface.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.ws.client.core.WebServiceTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.ws.test.client.RequestMatchers.payload;
import static org.springframework.ws.test.client.ResponseCreators.withPayload;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class SarisWebServiceTest {

    @MockBean
    WebServiceTemplate webServiceTemplate;

    @Autowired
    private SarisWebservice sarisWebservice;

    @Test
    public void shouldGetAktuelleGVU() throws IOException {

        SarisWebservice sarisWebservice = spy(this.sarisWebservice);

        GetAktuelleGVUsInfoAllgemeinResponse mockedSarisResponse = MockDataHelper.getMockedSarisResponse(
                "sarisRealMockResponse.xml");

        when(webServiceTemplate.marshalSendAndReceive(anyString(), any(), any())).thenReturn(mockedSarisResponse);
        when(sarisWebservice.getWebServiceTemplate()).thenReturn(webServiceTemplate);

        GetAktuelleGVUsInfoAllgemeinResponse aktuelleGVU =
                sarisWebservice.getAktuelleGVU(Constants.SARIS_ELECTRICITY_BRANCH_ID, false);

        assertFalse(aktuelleGVU.getGetAktuelleGVUsInfoAllgemeinResult().getViewGeplanteVU().isEmpty());
        assertEquals(mockedSarisResponse, aktuelleGVU);

    }


}
