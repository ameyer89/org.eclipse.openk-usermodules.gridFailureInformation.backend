/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.sarisinterface.controller;

import org.eclipse.openk.gridfailureinformation.sarisinterface.SarisInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.ImportService;
import org.eclipse.openk.gridfailureinformation.sarisinterface.service.SarisWebservice;
import org.eclipse.openk.gridfailureinformation.sarisinterface.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.sarisinterface.wsdl.GetAktuelleGVUsInfoAllgemeinResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SarisInterfaceApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ImportControllerTest {

    @MockBean
    private SarisWebservice sarisWebservice;

    @MockBean
    private ImportService importService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallImport() throws Exception {

        mockMvc.perform(get("/saris/import-test"))
                .andExpect(status().is2xxSuccessful());

        verify(importService, times(1)).importForeignFailures(anyInt(), anyBoolean(), anyBoolean());
    }

    @Test
    public void shouldCallImportAndReturnUnauthorized() throws Exception {

        mockMvc.perform(get("/saris/import-test"))
                .andExpect(status().isUnauthorized());

        verify(importService, times(0)).importForeignFailures(anyInt(), anyBoolean(), anyBoolean());
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallResponseTest() throws Exception {

        GetAktuelleGVUsInfoAllgemeinResponse mockedSarisResponse = MockDataHelper.getMockedSarisResponse(
                "sarisRealMockResponse.xml");

        when(sarisWebservice.getAktuelleGVU(anyInt(), anyBoolean())).thenReturn(mockedSarisResponse);

        mockMvc.perform(get("/saris/response-test"))
                .andExpect(status().is2xxSuccessful());

        verify(sarisWebservice, times(1)).getAktuelleGVU( anyInt(), anyBoolean());
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallResponseTest2() throws Exception {

        when(sarisWebservice.getAktuelleGVU(anyInt(), anyBoolean())).thenReturn(new GetAktuelleGVUsInfoAllgemeinResponse());

        mockMvc.perform(get("/saris/response-test"))
                .andExpect(status().is2xxSuccessful());

        verify(sarisWebservice, times(1)).getAktuelleGVU( anyInt(), anyBoolean());
    }
}