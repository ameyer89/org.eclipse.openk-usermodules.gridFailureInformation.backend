/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.config;


import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.TestImportGridFailuresApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@EntityScan(basePackageClasses = TestImportGridFailuresApplication.class)
@ContextConfiguration( initializers = {ConfigFileApplicationContextInitializer.class})
@TestPropertySource("spring.config.location=classpath:application.yml")
public class TestConfiguration {


    @Bean
    public EventProducerConfig myEventProducerConfig() {return new EventProducerConfig();}
}
