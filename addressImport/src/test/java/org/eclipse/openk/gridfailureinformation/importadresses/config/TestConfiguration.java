/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.config;

import org.eclipse.openk.gridfailureinformation.importadresses.AddressImportApplication;
import org.eclipse.openk.gridfailureinformation.importadresses.jobs.JobManager;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.AddressMapperImpl;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapper;
import org.eclipse.openk.gridfailureinformation.importadresses.mapper.StationMapperImpl;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressImportService;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressService;
import org.eclipse.openk.gridfailureinformation.importadresses.service.StationService;
import org.eclipse.openk.gridfailureinformation.importadresses.util.UtmConverter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@EnableJpaRepositories(basePackages = "org.eclipse.openk.gridfailureinformation")
@EntityScan(basePackageClasses = AddressImportApplication.class)
@ContextConfiguration( initializers = {ConfigFileApplicationContextInitializer.class})
@ActiveProfiles("test")
public class TestConfiguration {
    @Bean
    AddressMapper addressMapper() { return new AddressMapperImpl(); }

    @Bean
    StationMapper stationMapper() { return new StationMapperImpl(); }

    @Bean
    UtmConverter utmConverter() { return new UtmConverter(); }
    @Bean
    public AddressService myAddressService() {
        return new AddressService();
    }

    @Bean
    public AddressImportService myAddressImportService() {
        return new AddressImportService();
    }

    @Bean
    public StationService myStationService() { return new StationService(); }

    @Bean
    public JobManager jobManager() { return  new JobManager(); }
}
